<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/inc/header.inc';

echo '
<body>

<div class="container">

	<div class="row">

		<div class="col-lg-8 col-lg-offset-2">

		<h1>Contact formulier</h1>


		<form id="contactform" method="post" action="#" role="form">

		<div class="messages"></div>
		    <div class="controls">
		        <div class="row">
		            <div class="col-md-6">
		                <div class="form-group">
		                    <label for="voornaam">Voornaam of initialen *</label>
		                    <input id="voornaam" type="text" name="voornaam" class="form-control" placeholder="Vul uw voornaam alstublieft in *" required="required" data-error="Vul uw voornaam alstublieft in.">
		                    <div class="help-block with-errors"></div>
		                </div>
		            </div>
		            <div class="col-md-6">
		                <div class="form-group">
		                    <label for="anaam">Achternaam *</label>
		                    <input id="anaam" type="text" name="anaam" class="form-control" placeholder="Vul uw achternaam alstublieft in *" required="required" data-error="Vul uw achternaam alstublieft in.">
		                    <div class="help-block with-errors"></div>
		                </div>
		            </div>
		        </div>

		        <div class="row">
		            <div class="col-md-6">
		                <div class="form-group">
		                    <label for="email">Email *</label>
		                    <input id="email" type="email" name="email" class="form-control" placeholder="Uw email adres *" required="required" data-error="Vul uw mailadres alstublieft in.">
		                    <div class="help-block with-errors"></div>
		                </div>
		            </div>
		            <div class="col-md-6">
		                <div class="form-group">
		                    <label for="telnr">Telefoon</label>
		                    <input id="telnr" type="tel" name="telnr" class="form-control" placeholder="Uw telefoonnummer eventueel">
		                    <div class="help-block with-errors"></div>
		                </div>
		            </div>
		        </div>

		        <div class="row">
		            <div class="col-md-12">
		                <div class="form-group">
		                    <label for="form_message">Bericht *</label>
		                    <textarea id="form_message" name="message" class="form-control" placeholder="Uw bericht aan ICTmethode *" rows="4" required="required" data-error="Voer alstublieft een bericht in voor ons!."></textarea>
		                    <div class="help-block with-errors"></div>
		                </div>
		            </div>
		            <div class="col-md-12">
		                <input type="submit" class="btn btn-success btn-send" value="Versturen">
		            </div>
		        </div>

		        <div class="row">
		            <div class="col-md-12">
		                <p class="text-muted"><strong>*</strong>Deze info is echt nodig.</p>
		            </div>
		        </div>
		    </div>

		</form> 
		 </div>
		</div>
		</div>

</body>
';


if (!empty( $_POST["email"] ) && !empty( $_POST["message"]) ){

	// a email address that will be in the From field of the email.
	$from = $_POST["email"];

	// an email address that will receive the email with the output of the form
	$sendTo = 'someone at example.com';

	// subject of the email
	$subject = 'Bericht via contactformulier';

	// form field names and their translations.
	// array variable name => Text to appear in the email
	$fields = array('name' => 'Name', 'surname' => 'Surname', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message'); 

	// message that will be displayed when everything is OK :)
	$okMessage = '<div class="correct">Uw bericht aan ICTmethode is verstuurd</div>';

	// If something goes wrong, we will display this message.
	$errorMessage = '<div class="wrong">Er is iets misgegaan. Probeer het later nog eens.</div>';

	/*	
	 *  LET'S DO THE SENDING
	 */


	try
	{

	    if(count($_POST) == 0) throw new \Exception('Form is empty');
            
	    	$emailText = $_POST["message"]."\n";

    		foreach ($_POST as $key => $value) {
        		// If the field exists in the $fields array, include it in the email 
        		if (isset($fields[$key])) {
            			$emailText .= "$fields[$key]: $value\n";
        		}
    		}

    		// All the neccessary headers for the email.
    		$headers = array('Content-Type: text/plain; charset="UTF-8";',
        	'From: ' . $from,
        	'Reply-To: ' . $from,
        	'Return-Path: ' . $from,
    		);
    
    		// Send email
		$output = shell_exec('sendEmail -s smtp.sendgrid.net:587 -xp "" -xu ""  -t '.$sendTo.' -f '.$from.' -u '.$subject.' -m '.$emailText.''  );
    		$responseArray = array('type' => 'success', 'message' => $okMessage);
		}
			catch (\Exception $e)
		{
			    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
		}


		// if requested by AJAX request return JSON response
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$encoded = json_encode($responseArray);

			header('Content-Type: application/json');

    			echo $encoded;
		}else {
		    echo $responseArray['message'];
		}
}

?>
