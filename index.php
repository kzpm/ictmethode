<?php

session_start();

require_once 'inc/error_reporting.inc';
require_once 'inc/header.inc';
?>
<style>

html,body{
    position: relative;
    height: 100%;
    background: #999;

}

.login-container{
    width: 300px;
    margin: 80px auto;
    padding: 20px 40px 40px;
    text-align: center;
    background: #eee;
    border: 1px solid #ccc;
    //position:relative;z-index:-1;

}

#output{
    position: absolute;
    width: 300px;
    top: -75px;
    left: 0;
    color: #fff;
 
}

#output.alert-success{
    background: rgb(25, 204, 25);
}

#output.alert-danger{
    background: rgb(228, 105, 105);
}


.login-container::before,.login-container::after{
    content: "";
    position: absolute;
    width: 100%;height: 100%;
    top: 3.5px;left: 0;
    background: #fff;
    z-index: -1;
    -webkit-transform: rotateZ(4deg);
    -moz-transform: rotateZ(4deg);
    -ms-transform: rotateZ(4deg);
    border: 1px solid #ccc;

}

.login-container::after{
    top: 5px;
    z-index: -2;
    -webkit-transform: rotateZ(-2deg);
     -moz-transform: rotateZ(-2deg);
      -ms-transform: rotateZ(-2deg);

}

.avatar{
    width: 100px;height: 100px;
    margin: 10px auto 30px;
    border-radius: 100%;
    border: 2px solid #aaa;
    background-size: cover;

}

.form-box input{
    width: 100%;
    padding: 10px;
    text-align: center;
    height:40px;
    border: 1px solid #ccc;;
    background: #fafafa;
    transition:0.2s ease-in-out;
    z-index:1;position:relative;
}

.form-box input:focus{
    outline: 0;
    background: #eee;

}

.form-box input[type="text"]{
    border-radius: 5px 5px 0 0;
    /*text-transform: lowercase;*/
    color:#000;

}

.form-box input[type="password"]{
    border-radius: 0 0 5px 5px;
    border-top: 0;
}

.form-box button.login{
    margin-top:15px;
    padding: 10px 20px;
}

.animated {
  -webkit-animation-duration: 1s;
  animation-duration: 1s;
  -webkit-animation-fill-mode: both;
  animation-fill-mode: both;
}

@-webkit-keyframes fadeInUp {
  0% {
    opacity: 0;
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
}

@keyframes fadeInUp {
  0% {
    opacity: 0;
    -webkit-transform: translateY(20px);
    -ms-transform: translateY(20px);
    transform: translateY(20px);
  }

  100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    -ms-transform: translateY(0);
    transform: translateY(0);
  }
}

.fadeInUp {
  -webkit-animation-name: fadeInUp;
  animation-name: fadeInUp;
}
p{
	color:#000;
}
</style>

<?php

echo '
<div class="container">
	<div class="login-container">
            <div id="output"></div>
            <div class=	"avatar"><img src="https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fst2.depositphotos.com%2F1010146%2F5808%2Fv%2F170%2Fdepositphotos_58083779-Mobile-phone-click-icon.jpg&f=1"></div>
            <div class="form-box">
		<form action ='.$_SERVER["SCRIPT_NAME"].' method="POST">
                    <input name="userCode" autocomplete="off" type="text" placeholder=" Vul je code in">
                    <button class="btn btn-info btn-block login" name="Ok" type="submit">OK</button>
		</form>
		<form action ="register.php" method="POST">
                    <button class="btn btn-success btn-block login" type="submit" name="code">Nog geen code?</button>
                </form>
            </div>
        </div>
</div>
';

if ( isset( $_SESSION["mycode"] ) ){


	echo '
        <div class="container">
                <div class="jumbotron">
		<p class="text-center">Je code is:</p>

                        <h1 class="text-center">'.$_SESSION["mycode"].'</h1>
                </div>
        </div>';	


}//end if

if ( isset( $_POST[ "userCode" ] ) ){


	require_once 'src/clsCodeCheck.class.php';
	$myCodeCheck = new clsCodeCheck();
	$res = $myCodeCheck->fCheckCode( $_POST[ "userCode" ] );
	if ( $res[0] !== '0' ){
		
		
		$_SESSION["mycode"] = trim($_POST["userCode"]);
		$_SESSION["leerjaar"] = $res[4];
		
		require 'src/clsUserConnect.class.php';
		$myUserConnect = new clsUserConnect();
		$myUserConnect->fUserConnect( $_SESSION["leerjaar"], $_SESSION["mycode"] );
		echo '<script>location.replace( "studyoverview.php" );</script>';



	}else{
		echo '<div class="alert alert-danger text-center" role="alert">Deze code ken ik niet. Probeer het nog een keer!</div>';

	
	}//end if


}//end if


?>
