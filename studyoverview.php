<?php

session_start();
$rdir= $_SERVER["DOCUMENT_ROOT"];
require_once $rdir.'/src/clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace("./logout.php" )</script>';
	

}//end if

require_once $rdir.'/inc/error_reporting.inc';
require_once $rdir.'/inc/header.inc';


require_once $rdir.'/src/clsGetCurrentCourse.class.php';
$myCurrentCourse = new clsGetCurrentCourse();

require_once $rdir.'/src/clsGetStudentNr.class.php';
$myStNr = new clsGetStudentNr();
$nr = $myStNr->fGetStudentNr( $_SESSION["mycode"] );
$_SESSION["studentnr"]=$nr[0];



$_SESSION['tableName']= 'user'.$_SESSION['leerjaar'].$_SESSION['studentnr'];


require_once $rdir.'/src/clsHasRecords.class.php';
$myHasRecords = new clsHasRecords();
if ( $myHasRecords->fHasRecords( $_SESSION["tableName"] ) ){

	require_once $rdir.'/src/clsInsertCourses.class.php';
	$myInsertCourses= new clsInsertCourses( );
	if ( $myInsertCourses->fInsertCourses_OS( $_SESSION["tableName"], $_SESSION["leerjaar"], $_SESSION["studentnr"] ) ){

	}//end if

	if( $myInsertCourses->fInsertCourses_AT( $_SESSION["tableName"], $_SESSION["leerjaar"], $_SESSION["studentnr"] ) ){


	}//end if
	if( $myInsertCourses->fInsertCourses_SW( $_SESSION["tableName"], $_SESSION["leerjaar"], $_SESSION["studentnr"] ) ){


	}//end if
	if( $myInsertCourses->fInsertCourses_PR( $_SESSION["tableName"], $_SESSION["leerjaar"], $_SESSION["studentnr"] ) ){


	}//end if
	if( $myInsertCourses->fInsertCourses_SC( $_SESSION["tableName"], $_SESSION["leerjaar"], $_SESSION["studentnr"] ) ){


	}//end if
	if( $myInsertCourses->fInsertCourses_GS2( $_SESSION["tableName"], $_SESSION["leerjaar"], $_SESSION["studentnr"] ) ){


	}//end if
	if( $myInsertCourses->fInsertCourses_GS1( $_SESSION["tableName"], $_SESSION["leerjaar"], $_SESSION["studentnr"] ) ){


	}//end if


}//end if


//Move finished modules to archive
require_once $rdir.'/src/clsMoveToArchive.class.php';
$myMove = new clsMoveToArchive();
if ( $myMove->fMoveToArchive( $_SESSION['tableName'] ) ){ 


}//

// Show current courses
$courses = $myCurrentCourse->fGetCurrentCourse( $_SESSION["tableName"], $_SESSION["leerjaar"],$_SESSION["studentnr"] );

echo '<div class="container">
<a href="./logout.php" class="btn btn-info" role="button">Afmelden</a>';

echo '
<div class="page page-header">
	<h4>Leuk dat je hier bent '.ucwords( $nr[1] ).', van harte welkom!
	Kijk in de lijst wat je nog moet doen en wat je al klaar hebt.</h4>
</div>
<div class="jumbotron">
	<strong>Nog doen:</strong>
	<table class ="table table-striped" width ="100%"><head><tr><td><u>Cursus code</u> </td><td><u>Cursus naam</u></td><td><u>Lezen</u></td><td><u>Toets</u></td><td><u>Certificaat</u></td></tr>';
		for ( $i=0;$i < count( $courses ); $i++ ){
	
			echo strtolower( $courses[$i] );

		}//end for

	echo "</table>";

// Show current courses
$done = $myCurrentCourse->fGetCurrentCourse( 'arch_'.$_SESSION["tableName"], $_SESSION["leerjaar"],$_SESSION["studentnr"] );

	echo '<br><b>Klaar:</b>


	<table class ="table table-striped" width ="100%"><head><tr><td><u>Cursus code</u> </td><td><u>Cursus naam</u></td><td><u>Lezen</u></td><td><u>Toets</u></td><td><u>Certificaat</u></td></tr>';
		for ( $i=0;$i < count( $done ); $i++ ){
	
			echo strtolower( $done[$i] );

		}//end for

echo' 
</table>
</div>
</div>';

