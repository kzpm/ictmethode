<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os09<hr>
Vraag 5<h4> Wat gebeurt er wanneer je een bestand selecteert en op CTRL+ENTER drukt?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os091462794035' value='1'>Je computer sluit af<label></div>
<div class='radio'><label><input type='radio' name='os091462794035' value='2'>Je opent het bestand<label></div>
<div class='radio'><label><input type='radio' name='os091462794035' value='3'>Je voegt een nieuwe bladzijde toe<label></div>
<div class='radio'><label><input type='radio' name='os091462794035' value='4'>Je krijgt de eigenschappen van het bestand in beeld<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462794035' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos091462794035' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os091462794035'] ) &&  $_POST['os091462794035'] == "4" && isset( $_POST['btnos091462794035'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['os091462794035'] ) && $_POST['os091462794035'] !== "4" && isset( $_POST['btnos091462794035'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462794035'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
