<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os09<hr>
Vraag 9<h4> Wat doet de sneltoets 'CTRL+W'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os091462794616' value='1'>Opent een nieuw tabblad in je browser<label></div>
<div class='radio'><label><input type='radio' name='os091462794616' value='2'>Sluit alle tabbladen in de browser<label></div>
<div class='radio'><label><input type='radio' name='os091462794616' value='3'>Sluit het actieve tabblad<label></div>
<div class='radio'><label><input type='radio' name='os091462794616' value='4'>Sluit de browser af<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462794616' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos091462794616' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os091462794616'] ) &&  $_POST['os091462794616'] == "3" && isset( $_POST['btnos091462794616'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['os091462794616'] ) && $_POST['os091462794616'] !== "3" && isset( $_POST['btnos091462794616'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462794616'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
