<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os09<hr>
Vraag 1<h4> Wat zijn 'sneltoetsen'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os091462793656' value='1'>Snelle muisbewegingen<label></div>
<div class='radio'><label><input type='radio' name='os091462793656' value='2'>Een korte reken- of taaltoets<label></div>
<div class='radio'><label><input type='radio' name='os091462793656' value='3'>Snel typen<label></div>
<div class='radio'><label><input type='radio' name='os091462793656' value='4'>Combinaties van toetsen, waarmee je een opdracht uit kunt voeren<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462793656' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos091462793656' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os091462793656'] ) &&  $_POST['os091462793656'] == "4" && isset( $_POST['btnos091462793656'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['os091462793656'] ) && $_POST['os091462793656'] !== "4" && isset( $_POST['btnos091462793656'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462793656'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
