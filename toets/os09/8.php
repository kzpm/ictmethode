<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os09<hr>
Vraag 8<h4> Welke toetsencombinatie ken je voor 'Ongedaan maken'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os091462794479' value='1'>CTRL+O<label></div>
<div class='radio'><label><input type='radio' name='os091462794479' value='2'>CTRL+Z<label></div>
<div class='radio'><label><input type='radio' name='os091462794479' value='3'>XTRL+C<label></div>
<div class='radio'><label><input type='radio' name='os091462794479' value='4'>CTRL+A<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462794479' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos091462794479' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os091462794479'] ) &&  $_POST['os091462794479'] == "2" && isset( $_POST['btnos091462794479'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['os091462794479'] ) && $_POST['os091462794479'] !== "2" && isset( $_POST['btnos091462794479'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462794479'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
