<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at07<hr>
Vraag 8<h4> Wat kun je het beste doen als je een gehackte account wil herstellen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at071477302629' value='1'>Een nieuwe account maken met een heel sterk wachtwoord<label></div>
<div class='radio'><label><input type='radio' name='at071477302629' value='2'>Contact opnemen met de helpdesk van de dienst, waar je gebruik van maakte<label></div>
<div class='radio'><label><input type='radio' name='at071477302629' value='3'>Proberen de hackers te achterhalen<label></div>
<div class='radio'><label><input type='radio' name='at071477302629' value='4'>Een paar weken geen gebruik maken van internet. Je account wordt dan vanzelf 
hersteld<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477302629' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat071477302629' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at071477302629'] ) &&  $_POST['at071477302629'] == "2" && isset( $_POST['btnat071477302629'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['at071477302629'] ) && $_POST['at071477302629'] !== "2" && isset( $_POST['btnat071477302629'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477302629'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
