<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at07<hr>
Vraag 4<h4> Wat is handig om even op te letten wanneer je software installeert op Windows computers of tablets?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at071477301611' value='1'>Het besturingssysteem op mijn computer weet zelf wat schadelijke software is, dus hoef ik niks te doen<label></div>
<div class='radio'><label><input type='radio' name='at071477301611' value='2'>Mijn virusscanner zorgt ervoor dat er geen programma's worden geinstalleerd, die ik niet wil hebben<label></div>
<div class='radio'><label><input type='radio' name='at071477301611' value='3'>Het is handig om de extra software, waar ik niet om heb gevraagd toch te installeren. Het is gratis en je weet nooit waar het nuttig voor is!<label></div>
<div class='radio'><label><input type='radio' name='at071477301611' value='4'>Kijk of er ook extra software mee geinstalleerd wordt en kijk of je dit ook echt wil installeren<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477301611' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat071477301611' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at071477301611'] ) &&  $_POST['at071477301611'] == "4" && isset( $_POST['btnat071477301611'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['at071477301611'] ) && $_POST['at071477301611'] !== "4" && isset( $_POST['btnat071477301611'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477301611'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
