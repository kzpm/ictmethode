<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at07<hr>
Vraag 3<h4> Waarom kun je beter geen gebruik maken van de optie 'Wachtwoord onthouden'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at071477298645' value='1'>Je wachtwoord moet je regelmatig veranderen, dus is wachtwoord onthouden niet echt handig<label></div>
<div class='radio'><label><input type='radio' name='at071477298645' value='2'>Meestal kun je je wachtwoord zelf wel onthouden<label></div>
<div class='radio'><label><input type='radio' name='at071477298645' value='3'>Wanneer je hiervoor kiest, kunnen anderen, die gebruik maken van je computer, in al je accounts komen<label></div>
<div class='radio'><label><input type='radio' name='at071477298645' value='4'>Mensen kunnen dan mijn wachtwoord achterhalen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477298645' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat071477298645' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at071477298645'] ) &&  $_POST['at071477298645'] == "3" && isset( $_POST['btnat071477298645'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['at071477298645'] ) && $_POST['at071477298645'] !== "3" && isset( $_POST['btnat071477298645'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477298645'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
