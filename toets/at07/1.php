<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at07<hr>
Vraag 1<h4> Wat beteken 'hacken'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at071477296129' value='1'>Iemand probeert een foto te gebruiken zonder toetsemming te vragen<label></div>
<div class='radio'><label><input type='radio' name='at071477296129' value='2'>Iemand heeft zijn virusscanner uitgeschakeld<label></div>
<div class='radio'><label><input type='radio' name='at071477296129' value='3'>Iemand probeert ongeoorloofd toegang te krijgen tot een computer van iemand anders<label></div>
<div class='radio'><label><input type='radio' name='at071477296129' value='4'>Wanneer je inloggegevens niet meer werken, ben je gehackt<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477296129' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat071477296129' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at071477296129'] ) &&  $_POST['at071477296129'] == "3" && isset( $_POST['btnat071477296129'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['at071477296129'] ) && $_POST['at071477296129'] !== "3" && isset( $_POST['btnat071477296129'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477296129'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
