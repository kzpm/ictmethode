<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at07<hr>
Vraag 5<h4> Welke zoekresultaten krijg je wanneer je zoekt op 'Ask toolbar'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at071477301729' value='1'>Allemaal resultaten met hoe je de zoekbalk moet verwijderen<label></div>
<div class='radio'><label><input type='radio' name='at071477301729' value='2'>Ik krijg geen zoekresultaten<label></div>
<div class='radio'><label><input type='radio' name='at071477301729' value='3'>Hoe je geld kunt verdienen met deze toolbar<label></div>
<div class='radio'><label><input type='radio' name='at071477301729' value='4'>Ik krijg alleen plaatjes te zien<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477301729' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat071477301729' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at071477301729'] ) &&  $_POST['at071477301729'] == "1" && isset( $_POST['btnat071477301729'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['at071477301729'] ) && $_POST['at071477301729'] !== "1" && isset( $_POST['btnat071477301729'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477301729'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
