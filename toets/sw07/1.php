<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw07<hr>
Vraag 1<h4> Wat wordt met de 'cloud' bedoeld?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw071478014814' value='1'>Een wolk<label></div>
<div class='radio'><label><input type='radio' name='sw071478014814' value='2'>De programma's die je gebruikt staan niet meer op je computer maar op internet<label></div>
<div class='radio'><label><input type='radio' name='sw071478014814' value='3'>De computer die je gebruikt staat niet bij je thuis<label></div>
<div class='radio'><label><input type='radio' name='sw071478014814' value='4'>De programma's die je gebruikt kun je alleen in een vliegtuig gebruiken<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478014814' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw071478014814' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw071478014814'] ) &&  $_POST['sw071478014814'] == "2" && isset( $_POST['btnsw071478014814'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['sw071478014814'] ) && $_POST['sw071478014814'] !== "2" && isset( $_POST['btnsw071478014814'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478014814'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
