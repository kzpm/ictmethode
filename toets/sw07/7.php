<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw07<hr>
Vraag 7<h4> Hoe kun je meer te weten komen over een 'clouddienst'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw071478019501' value='1'>Je kunt contact opnemen met de clouddienst en vragen stellen<label></div>
<div class='radio'><label><input type='radio' name='sw071478019501' value='2'>Je kunt kijken hoeveel leden ze hebben. Hoe meer leden, hoe betere de dienst<label></div>
<div class='radio'><label><input type='radio' name='sw071478019501' value='3'>Je kunt kijken op gebruikersfora<label></div>
<div class='radio'><label><input type='radio' name='sw071478019501' value='4'>Je kunt reclameboodschappen van de dienst bestuderen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478019501' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw071478019501' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw071478019501'] ) &&  $_POST['sw071478019501'] == "3" && isset( $_POST['btnsw071478019501'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['sw071478019501'] ) && $_POST['sw071478019501'] !== "3" && isset( $_POST['btnsw071478019501'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478019501'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
