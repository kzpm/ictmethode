<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw07<hr>
Vraag 9<h4> Wat betekent `opschorten` van je account?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw071478019847' value='1'>Dat je het lidmaatschap van een dienst verlengd<label></div>
<div class='radio'><label><input type='radio' name='sw071478019847' value='2'>Dat je je account tijdelijk uit de lucht haalt<label></div>
<div class='radio'><label><input type='radio' name='sw071478019847' value='3'>Dat je je account permanent verwijdert<label></div>
<div class='radio'><label><input type='radio' name='sw071478019847' value='4'>Dat je gebruiker wordt van een 'clouddienst'<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478019847' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw071478019847' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw071478019847'] ) &&  $_POST['sw071478019847'] == "2" && isset( $_POST['btnsw071478019847'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['sw071478019847'] ) && $_POST['sw071478019847'] !== "2" && isset( $_POST['btnsw071478019847'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478019847'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
