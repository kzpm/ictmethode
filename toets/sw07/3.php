<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw07<hr>
Vraag 3<h4> In welk land kunnen de `cloudprogramma's` staan die je gebruikt?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw071478015183' value='1'>Ze staan altijd in het land waar je op dat moment bent<label></div>
<div class='radio'><label><input type='radio' name='sw071478015183' value='2'>Ze kunnen in elk land staan<label></div>
<div class='radio'><label><input type='radio' name='sw071478015183' value='3'>Ze kunnen overal staan, maar niet in Noord-Korea<label></div>
<div class='radio'><label><input type='radio' name='sw071478015183' value='4'>Alleen in Nederland<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478015183' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw071478015183' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw071478015183'] ) &&  $_POST['sw071478015183'] == "2" && isset( $_POST['btnsw071478015183'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['sw071478015183'] ) && $_POST['sw071478015183'] !== "2" && isset( $_POST['btnsw071478015183'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478015183'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
