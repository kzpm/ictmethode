<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw07<hr>
Vraag 4<h4> welk programma heb je nodig om 'cloud' programma's te kunnen draaien?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw071478015272' value='1'>Een internetbrowser<label></div>
<div class='radio'><label><input type='radio' name='sw071478015272' value='2'>Een tekstverwerker<label></div>
<div class='radio'><label><input type='radio' name='sw071478015272' value='3'>Een mailprogramma<label></div>
<div class='radio'><label><input type='radio' name='sw071478015272' value='4'>Een chatprogramma<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478015272' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw071478015272' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw071478015272'] ) &&  $_POST['sw071478015272'] == "1" && isset( $_POST['btnsw071478015272'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['sw071478015272'] ) && $_POST['sw071478015272'] !== "1" && isset( $_POST['btnsw071478015272'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478015272'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
