<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os01<hr>
Vraag 9<h4> Wat is een programmeertaal?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST' autocomplete=off>
<div class='radio'><label><input type='radio' name='os011455313471' value='1'>Een taal die je moet spreken, wanneer je het over computers wilt hebben<label></div>
<div class='radio'><label><input type='radio' name='os011455313471' value='2'>Een taal, die door computers begrepen wordt<label></div>
<div class='radio'><label><input type='radio' name='os011455313471' value='3'>Een tekstdocument<label></div>
<div class='radio'><label><input type='radio' name='os011455313471' value='4'>Japans, Duits en Engels<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1455313471' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos011455313471' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os011455313471'] ) &&  $_POST['os011455313471'] == "2" && isset( $_POST['btnos011455313471'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['os011455313471'] ) && $_POST['os011455313471'] !== "2" && isset( $_POST['btnos011455313471'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1455313471'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
