<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os01<hr>
Vraag 1<h4> Welke bewering is waar?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST' autocomplete='off'>
<div class='radio'><label><input type='radio' name='os011455312050' value='1'>Een besturingssysteem zorgt er voor dat opdrachten van de gebruiker 'begrepen' worden<label></div>
<div class='radio'><label><input type='radio' name='os011455312050' value='2'>Een besturingssysteem past alleen op een fiets<label></div>
<div class='radio'><label><input type='radio' name='os011455312050' value='3'>Een besturingssysteem moet zo snel mogelijk verwijderd worden, omdat het een virus kan zijn<label></div>
<div class='radio'><label><input type='radio' name='os011455312050' value='4'>Het is niet mogelijk om zelf een besturingssysteem te installeren<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1455312050' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos011455312050' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os011455312050'] ) &&  $_POST['os011455312050'] == "1" && isset( $_POST['btnos011455312050'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['os011455312050'] ) && $_POST['os011455312050'] !== "1" && isset( $_POST['btnos011455312050'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1455312050'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
