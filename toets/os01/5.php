<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os01<hr>
Vraag 5<h4> Aan welk besturingssysteem kun je actief mee ontwikkelen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST' autocomplete=off>
<div class='radio'><label><input type='radio' name='os011455312866' value='1'>Microsoft Windows<label></div>
<div class='radio'><label><input type='radio' name='os011455312866' value='2'>Google Android<label></div>
<div class='radio'><label><input type='radio' name='os011455312866' value='3'>Apple iOS<label></div>
<div class='radio'><label><input type='radio' name='os011455312866' value='4'>Linux<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1455312866' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos011455312866' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os011455312866'] ) &&  $_POST['os011455312866'] == "4" && isset( $_POST['btnos011455312866'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['os011455312866'] ) && $_POST['os011455312866'] !== "4" && isset( $_POST['btnos011455312866'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1455312866'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
