<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os06<hr>
Vraag 8<h4> Wat is een 'Trojan'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os061462394883' value='1'>Het is een programmaatje, wat toegang tot een computer probeert te krijgen<label></div>
<div class='radio'><label><input type='radio' name='os061462394883' value='2'>Het is een paard<label></div>
<div class='radio'><label><input type='radio' name='os061462394883' value='3'>Een trojan probeert je vaak berichten te sturen<label></div>
<div class='radio'><label><input type='radio' name='os061462394883' value='4'>Alle antwoorden kloppen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462394883' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos061462394883' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os061462394883'] ) &&  $_POST['os061462394883'] == "1" && isset( $_POST['btnos061462394883'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['os061462394883'] ) && $_POST['os061462394883'] !== "1" && isset( $_POST['btnos061462394883'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462394883'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
