<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw05<hr>
Vraag 9<h4> Welke handelingen moet je doen om in Windows het 'configuratiescherm' te openen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw051478004223' value='1'>Druk op `start` en klik daarna op 'configuratiescherm'<label></div>
<div class='radio'><label><input type='radio' name='sw051478004223' value='2'>Druk op `start` en kies daarna `Uitvoeren...`<label></div>
<div class='radio'><label><input type='radio' name='sw051478004223' value='3'>Druk op `start` en kies daarna `Alle programma's`<label></div>
<div class='radio'><label><input type='radio' name='sw051478004223' value='4'>Geen van de antwoorden is juist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478004223' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw051478004223' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw051478004223'] ) &&  $_POST['sw051478004223'] == "1" && isset( $_POST['btnsw051478004223'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['sw051478004223'] ) && $_POST['sw051478004223'] !== "1" && isset( $_POST['btnsw051478004223'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478004223'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
