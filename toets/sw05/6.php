<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw05<hr>
Vraag 6<h4> Op welke extensie eindigen configuratiebestanden in Linux en Apple?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw051478003747' value='1'>Op 'dll'<label></div>
<div class='radio'><label><input type='radio' name='sw051478003747' value='2'>Op 'exe'<label></div>
<div class='radio'><label><input type='radio' name='sw051478003747' value='3'>Op 'doc' of 'docx'<label></div>
<div class='radio'><label><input type='radio' name='sw051478003747' value='4'>Op 'cfg' of 'conf'<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478003747' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw051478003747' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw051478003747'] ) &&  $_POST['sw051478003747'] == "4" && isset( $_POST['btnsw051478003747'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['sw051478003747'] ) && $_POST['sw051478003747'] !== "4" && isset( $_POST['btnsw051478003747'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478003747'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
