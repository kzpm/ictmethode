<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw05<hr>
Vraag 3<h4> Hoe noem je de optie in een programma waarmee je instellingen kunt wijzigen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw051478003419' value='1'>Beeld<label></div>
<div class='radio'><label><input type='radio' name='sw051478003419' value='2'>Help<label></div>
<div class='radio'><label><input type='radio' name='sw051478003419' value='3'>Instellingen<label></div>
<div class='radio'><label><input type='radio' name='sw051478003419' value='4'>Bestand<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478003419' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw051478003419' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw051478003419'] ) &&  $_POST['sw051478003419'] == "3" && isset( $_POST['btnsw051478003419'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['sw051478003419'] ) && $_POST['sw051478003419'] !== "3" && isset( $_POST['btnsw051478003419'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478003419'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
