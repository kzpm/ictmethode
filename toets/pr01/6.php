<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 6<h4> Wat betekent: `6 * 3 == 2 * 9`?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021479895269' value='1'>Dit is geen goede som<label></div>
<div class='radio'><label><input type='radio' name='pr021479895269' value='2'>De uitkomst van 6 * 3 is groter dan de uitkomst van 2 * 9<label></div>
<div class='radio'><label><input type='radio' name='pr021479895269' value='3'>De uitkomst van 6 * 3 is kleiner dan de uitkomst van 2 * 9<label></div>
<div class='radio'><label><input type='radio' name='pr021479895269' value='4'>De uitkomst van 6 * 3 is gelijk aan de uitkomst van 2 * 9<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479895269' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021479895269' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021479895269'] ) &&  $_POST['pr021479895269'] == "4" && isset( $_POST['btnpr021479895269'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['pr021479895269'] ) && $_POST['pr021479895269'] !== "4" && isset( $_POST['btnpr021479895269'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479895269'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
