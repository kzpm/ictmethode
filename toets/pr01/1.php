<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 1<h4> Welke bewerking zie je bij de som  `7 + 8`?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021479894770' value='1'>Delen<label></div>
<div class='radio'><label><input type='radio' name='pr021479894770' value='2'>Aftrekken<label></div>
<div class='radio'><label><input type='radio' name='pr021479894770' value='3'>Vermenigvuldigen<label></div>
<div class='radio'><label><input type='radio' name='pr021479894770' value='4'>Optellen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479894770' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021479894770' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021479894770'] ) &&  $_POST['pr021479894770'] == "4" && isset( $_POST['btnpr021479894770'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['pr021479894770'] ) && $_POST['pr021479894770'] !== "4" && isset( $_POST['btnpr021479894770'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479894770'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
