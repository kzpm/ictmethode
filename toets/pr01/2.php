<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 2<h4> Welke bewerking zie je in de som `140 - 75`?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021479894850' value='1'>Aftrekken<label></div>
<div class='radio'><label><input type='radio' name='pr021479894850' value='2'>Optellen<label></div>
<div class='radio'><label><input type='radio' name='pr021479894850' value='3'>Delen<label></div>
<div class='radio'><label><input type='radio' name='pr021479894850' value='4'>alle antwoorden zijn onjuist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479894850' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021479894850' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021479894850'] ) &&  $_POST['pr021479894850'] == "1" && isset( $_POST['btnpr021479894850'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['pr021479894850'] ) && $_POST['pr021479894850'] !== "1" && isset( $_POST['btnpr021479894850'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479894850'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
