<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 7<h4> Welke som hoort bij `De uitkomst van 3 : 1 is gelijk aan 1 * 3`?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021479895414' value='1'>3 : 1 < 1 * 3<label></div>
<div class='radio'><label><input type='radio' name='pr021479895414' value='2'>3 : 1 > 1 * 3<label></div>
<div class='radio'><label><input type='radio' name='pr021479895414' value='3'>3 : 1 == 3 * 1<label></div>
<div class='radio'><label><input type='radio' name='pr021479895414' value='4'>Geen van antwoorden is juist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479895414' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021479895414' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021479895414'] ) &&  $_POST['pr021479895414'] == "3" && isset( $_POST['btnpr021479895414'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['pr021479895414'] ) && $_POST['pr021479895414'] !== "3" && isset( $_POST['btnpr021479895414'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479895414'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
