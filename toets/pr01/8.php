<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 8<h4> Welke som hoort bij `De variabele x is kleiner dan de variabele y`</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021479895501' value='1'>x < y<label></div>
<div class='radio'><label><input type='radio' name='pr021479895501' value='2'>x > y<label></div>
<div class='radio'><label><input type='radio' name='pr021479895501' value='3'>y < x<label></div>
<div class='radio'><label><input type='radio' name='pr021479895501' value='4'>x == y<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479895501' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021479895501' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021479895501'] ) &&  $_POST['pr021479895501'] == "1" && isset( $_POST['btnpr021479895501'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['pr021479895501'] ) && $_POST['pr021479895501'] !== "1" && isset( $_POST['btnpr021479895501'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479895501'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
