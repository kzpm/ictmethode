<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 3<h4> Welke bewerking zie je in de som `47 * 128`?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021479894910' value='1'>Optellen<label></div>
<div class='radio'><label><input type='radio' name='pr021479894910' value='2'>Vermenigvuldigen<label></div>
<div class='radio'><label><input type='radio' name='pr021479894910' value='3'>Vergelijken<label></div>
<div class='radio'><label><input type='radio' name='pr021479894910' value='4'>Aftrekken<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479894910' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021479894910' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021479894910'] ) &&  $_POST['pr021479894910'] == "2" && isset( $_POST['btnpr021479894910'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['pr021479894910'] ) && $_POST['pr021479894910'] !== "2" && isset( $_POST['btnpr021479894910'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479894910'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
