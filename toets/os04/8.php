<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os04<hr>
Vraag 8<h4> Je wilt het woord 'Appel' vervangen door het woord 'Peer'. Welke optie moet je nu inschakelen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os041462364026' value='1'>Van onder naar boven zoeken en vervangen<label></div>
<div class='radio'><label><input type='radio' name='os041462364026' value='2'>Zoeken naar spaties<label></div>
<div class='radio'><label><input type='radio' name='os041462364026' value='3'>Woorden langer dan twee letters<label></div>
<div class='radio'><label><input type='radio' name='os041462364026' value='4'>Hoofdletters<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462364026' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos041462364026' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os041462364026'] ) &&  $_POST['os041462364026'] == "4" && isset( $_POST['btnos041462364026'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['os041462364026'] ) && $_POST['os041462364026'] !== "4" && isset( $_POST['btnos041462364026'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462364026'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
