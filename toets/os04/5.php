<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os04<hr>
Vraag 5<h4> Ik wil graag het woord 'buiten' vervangen door 'binnen'. Wat gebeurt er met het woord 'binnenstebuiten'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os041462361673' value='1'>Het wordt 'buitenstebinnen'<label></div>
<div class='radio'><label><input type='radio' name='os041462361673' value='2'>Het wordt 'buitenstebuiten'<label></div>
<div class='radio'><label><input type='radio' name='os041462361673' value='3'>Het wordt 'binnenstebinnen'<label></div>
<div class='radio'><label><input type='radio' name='os041462361673' value='4'>De vervang actie lukt niet, omdat het woord 'binnenstebuiten' niet bestaat<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462361673' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos041462361673' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os041462361673'] ) &&  $_POST['os041462361673'] == "3" && isset( $_POST['btnos041462361673'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['os041462361673'] ) && $_POST['os041462361673'] !== "3" && isset( $_POST['btnos041462361673'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462361673'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
