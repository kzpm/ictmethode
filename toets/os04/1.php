<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"]; 
$incdir=$rdir."/inc/";
$srcdir=$rdir."/src/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace("../../logout.php" )</script>';

}//end if 

$st_code =$_SESSION['studentnr'];
$cursuscode ="os04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';


echo "<div class='container'>os04<hr>
Vraag 1<h4> In de map `Mijn documenten` staan 3 bestanden met de namen 'lezen.docx', 'verslag_schoolreis.docx' en 'spreekbeurt-fiets.docx'. Welke bestanden worden gevonden met het zoekargument '*.docx'?</h4><br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os041453746262' value='1'>Alle drie bestanden<label></div>
<div class='radio'><label><input type='radio' name='os041453746262' value='2'>Alleen maar het bestand 'lezen.docx'<label></div>
<div class='radio'><label><input type='radio' name='os041453746262' value='3'>Alleen maar het bestand 'verslag_schoolreis.docx'<label></div>
<div class='radio'><label><input type='radio' name='os041453746262' value='4'>Geen enkel bestand<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1453746262' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos041453746262' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os041453746262'] ) && $_POST['os041453746262']== "1"  ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
$nextpage=2;
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['btnos041453746262'] ) && $_POST['os041453746262'] !== "1" ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
$nextpage=2;
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1453746262'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if

?>
