<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];         
$incdir=$rdir."inc/";
$srcdir=$rdir."src/";

require_once $srcdir.'clsChkSession.class.php';

$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace("../../logout.php" )</script>';

}//end if 
$st_code =$_SESSION['studentnr'];
$cursuscode ="os04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os04<hr>
Vraag 2<h4> Met een zoekactie vind ik 'verslag.txt', 'ik_zoek.docx' en 'schoolreis_2016.docx'. Welke zoekactie heb ik uitgevoerd?</h4><br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os041453717967' value='1'>*.txt<label></div>
<div class='radio'><label><input type='radio' name='os041453717967' value='2'>*docx<label></div>
<div class='radio'><label><input type='radio' name='os041453717967' value='3'>*x*<label></div>
<div class='radio'><label><input type='radio' name='os041453717967' value='4'>verslag.*<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1453717967' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos041453717967' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['btnos041453717967'] ) && $_POST['os041453717967'] == "3" ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
$nextpage=3;
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['btnos041453717967'] ) && $_POST['os041453717967'] !== "3" ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
$nextpage=3;
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1453717967'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
