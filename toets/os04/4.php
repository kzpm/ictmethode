<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];         
$incdir=$rdir."inc/";
$srcdir=$rdir."src/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace("../../logout.php" )</script>';

}//end if 
require_once $incdir.'header.inc';
require_once $srcdir.'clsLevenshtein.class.php';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="os04";

$myLevenshtein = new clsLevenshtein();
echo "<div class='container'>os04<hr>";
echo "<b>Vraag 4</b> 'Vervangen' betekent in het Engels:<br><br>
<form name='qopen' action ='' method= 'POST' autocomplete='off'>
<label for='os041453751866'>Antwoord</label><input type='text' class='form-control' id='os041453751866' name='os041453751866' >
<input type='submit' class='btn btn-warning black-text' name='btnCancel1453751866' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn31453751866' value='Ok'>
</form>
<hr></div>";

$array1=explode(" ","replace replase");
if (  !empty( $_POST['os041453751866'] ) && $myLevenshtein->fLevenshtein( $array1, $_POST["os041453751866"] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
$nextpage=5;
echo '<script>location.replace("5.php");</script>';

}//end if
if ( !empty( $_POST['os041453751866'] ) && !$myLevenshtein->fLevenshtein( $array1, $_POST["os041453751866"] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
$nextpage=5;
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1453751866'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if

?>

