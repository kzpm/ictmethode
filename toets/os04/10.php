<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";
   
                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( ."/logout.php" )</script>';

                }//end if

require_once $incdir.'header.inc';
require_once $srcdir.'clsLevenshtein.class.php';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="os04";

$myLevenshtein = new clsLevenshtein();
echo "<div class='container'>os04<hr>";
echo "Vraag 10<h4> Je wilt in een heel groot verslag de zin `Ik ben twee pennen nodig` vervangen door `Ik heb twee pennen nodig`. In het zoekvak typ je `ben` en in het vervangvak 
type je `heb`. Wat gebeurt er met de zin 'Ik ben bijna gevallen'? Schrijf de zin helemaal op hieronder.</h4>Je mag een zoekmachine gebruiken!<br><br>
<form name='qopen' action ='' method= 'POST' autocomplete='off'>
<label for='os041462364502'>Antwoord</label><input type='text' class='form-control' id='os041462364502' name='os041462364502' >
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462364502' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn31462364502' value='Ok'>
</form>
<hr></div>";

$array1=explode(",","Ik heb bijna gevallen");
if (  isset( $_POST["os041462364502"]) && $myLevenshtein->fLevenshtein( $array1, $_POST["os041462364502"] ) && !empty( $_POST['os041462364502'] ) && isset( $_POST['os041462364502'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,1 );
echo '<script>location.replace("11.php");</script>';

}//end if
if (  isset( $_POST["os041462364502"]) && !$myLevenshtein->fLevenshtein( $array1, $_POST["os041462364502"] ) && !empty( $_POST['os041462364502'] ) && isset( $_POST['os041462364502'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,0 );
echo '<script>location.replace("11.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462364502'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

