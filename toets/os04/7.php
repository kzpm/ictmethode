<?php
session_start();
$rdir=$_SERVER["DOCUMENT_ROOT"];         
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( $rdir."logout.php" )</script>';

                }//end if


require_once $incdir.'header.inc';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="os04";

echo "<div class='container'>os04<hr>
Vraag 7<h4> Waarom is het slim om een backup te maken voordat je tekst gaat vervangen in een bestand?</h4>(Er zijn meerdere antwoorden mogelijk!)<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qcheck' action ='' method= 'POST'>
<div class='checkbox'><label><input type='checkbox' name='os0414623638171' value='2'>Omdat een backup maken bijna geen tijd kost<label></div>
<div class='checkbox'><label><input type='checkbox' name='os0414623638172' value='3'>Soms lukt het niet meer om een verkeerde vervangactie ongedaan te maken<label></div>
<div class='checkbox'><label><input type='checkbox' name='os0414623638173' value='4'>Een backup neemt geen schijfruimte in beslag<label></div>
<div class='checkbox'><label><input type='checkbox' name='os0414623638174' value='5'>Wanneer er iets misgaat met het vervangen, kun je de backup terugzetten<label></div>
<!--<button type='submit' name='btn21462363817' value='1'>Ok</button>-->
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462363817' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn21462363817' value='Ok'>
</form>
<hr></div>";
$array1=array();
  foreach( $_POST as $name => $value) {
        		if ( $value !='Ok' )
        			$array1[]= $value;
   	 	 }//end foreach

$array2=explode( ",",'3,5');
//$res1= array_product( $array1 );
//$res2= array_product( $array2 );
//echo "Array1 ".array_product( $array1 ).'<br>';
//echo "Array2 ".array_product( $array2 ).'<br>';
//echo "Product totaal ".$res1*$res2;
if ( array_product( $array1 )== array_product( $array2) && isset( $_POST["btn21462363817"] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if ( array_product( $array1 )!== array_product( $array2) && isset( $_POST["btn21462363817"] ) ){
        		
$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if
if ( isset( $_POST['btnCancel1462363817'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

