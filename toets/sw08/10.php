<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw08<hr>
Vraag 10<h4> Waarom is het handig om apps te verwijderen, die je niet meer gebruikt?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw081478082669' value='1'>Omdat apps, die je niet veel gebruikt, foto's van je gaan maken<label></div>
<div class='radio'><label><input type='radio' name='sw081478082669' value='2'>Het scheelt ruimte en er kunnen via die app dan geen gegevens meer verzameld worden<label></div>
<div class='radio'><label><input type='radio' name='sw081478082669' value='3'>Je telefoon of tablet ziet er dan mooier uit<label></div>
<div class='radio'><label><input type='radio' name='sw081478082669' value='4'>Alle antwoorden zijn juist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478082669' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw081478082669' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw081478082669'] ) &&  $_POST['sw081478082669'] == "2" && isset( $_POST['btnsw081478082669'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,1 );
echo '<script>location.replace("11.php");</script>';

}//end if

if (  isset( $_POST['sw081478082669'] ) && $_POST['sw081478082669'] !== "2" && isset( $_POST['btnsw081478082669'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,0 );
echo '<script>location.replace("11.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478082669'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
