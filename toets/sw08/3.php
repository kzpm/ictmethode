<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw08<hr>
Vraag 3<h4> Waarom krijg je bij het installeren van apps vaak de 
melding dat de apps gebruik 
willen maken van je netwerkverbinding?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw081478080987' value='1'>Dit is nodig omdat de fabrikant anders geen contact met je op kan nemen<label></div>
<div class='radio'><label><input type='radio' name='sw081478080987' value='2'>Dit is nodig omdat de app een internetverbinding nodig heeft<label></div>
<div class='radio'><label><input type='radio' name='sw081478080987' value='3'>Deze melding krijg je alleen op smartphones en tablets van Apple<label></div>
<div class='radio'><label><input type='radio' name='sw081478080987' value='4'>Wanneer je deze melding krijgt, weet je dat je de app alleen moet gebruiken wanneer er een Wifi toegangspunt aanwezig is<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478080987' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw081478080987' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw081478080987'] ) &&  $_POST['sw081478080987'] == "2" && isset( $_POST['btnsw081478080987'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['sw081478080987'] ) && $_POST['sw081478080987'] !== "2" && isset( $_POST['btnsw081478080987'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478080987'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
