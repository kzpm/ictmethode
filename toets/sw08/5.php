<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw08<hr>
Vraag 5<h4> Wat heb je nodig wanneer je een app uit de Appstore wilt gebruiken?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw081478081500' value='1'>Je hebt alleen een computer nodig<label></div>
<div class='radio'><label><input type='radio' name='sw081478081500' value='2'>Je moet betalen wanneer je iets uit de AppStore wil gebruiken, dus heb je een PayPal account nodig<label></div>
<div class='radio'><label><input type='radio' name='sw081478081500' value='3'>Je hebt een iPhone en een AppleId nodig.<label></div>
<div class='radio'><label><input type='radio' name='sw081478081500' value='4'>Het besturingssysteem Android moet op je iPhone geinstalleerd worden<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478081500' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw081478081500' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw081478081500'] ) &&  $_POST['sw081478081500'] == "3" && isset( $_POST['btnsw081478081500'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['sw081478081500'] ) && $_POST['sw081478081500'] !== "3" && isset( $_POST['btnsw081478081500'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478081500'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
