<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw08<hr>
Vraag 4<h4> Wat is de beste omschrijving voor een app?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw081478081274' value='1'>Een app is een computerprogramma wat alleen op een pc werkt<label></div>
<div class='radio'><label><input type='radio' name='sw081478081274' value='2'>Een app is een computerprogramma, die meestal alleen op smartphones en tablets werkt<label></div>
<div class='radio'><label><input type='radio' name='sw081478081274' value='3'>Een app is een 'gegevensverzameling'<label></div>
<div class='radio'><label><input type='radio' name='sw081478081274' value='4'>Een app is een computerprogramma, waarbij je gegevens niet bekeken kunnen worden<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478081274' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw081478081274' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw081478081274'] ) &&  $_POST['sw081478081274'] == "2" && isset( $_POST['btnsw081478081274'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['sw081478081274'] ) && $_POST['sw081478081274'] !== "2" && isset( $_POST['btnsw081478081274'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478081274'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
