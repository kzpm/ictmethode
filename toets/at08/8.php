<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at08<hr>
Vraag 8<h4> Lees <a href='https://www.mijnonlineidentiteit.nl/privacy-instellingen-facebook-aanpassen/#Zoekmachines' target='_blank'>deze</a> informatie. Hoe kun je 
ervoor zorgen dat je Facebook profiel niet meer in de resultaten van de zoekmachine gevonden worden?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine 
gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at081477400711' value='1'>Dit kan alleen maar wanneer je gebruik maakt van de zoekmachine van Google<label></div>
<div class='radio'><label><input type='radio' name='at081477400711' value='2'>Dit kan alleen maar wanneer je gebruik maakt van de zoekmachine 'DuckDuckGo'<label></div>
<div class='radio'><label><input type='radio' name='at081477400711' value='3'>Klik op je profielfoto, daarna op 'Bewerken'. Druk dan op de knop 'Opslaan'.<label></div>
<div class='radio'><label><input type='radio' name='at081477400711' value='4'>Klik op ‘Bewerken’ aan de rechterkant. zet daarna het vinkje uit in het selectievakje<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477400711' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat081477400711' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at081477400711'] ) &&  $_POST['at081477400711'] == "4" && isset( $_POST['btnat081477400711'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['at081477400711'] ) && $_POST['at081477400711'] !== "4" && isset( $_POST['btnat081477400711'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477400711'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
