<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at08<hr>
Vraag 7<h4> Waarom is het niet erg verstandig om je adresgegevens of je bankrekeningnummer door te geven via internet?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at081477330415' value='1'>Iemand die je banknummer heeft kan dan geld op je rekening storten<label></div>
<div class='radio'><label><input type='radio' name='at081477330415' value='2'>Iemand met je adresgegevens, je naam en je sofinummer, kan doen alsof hij jou is. Zo kan hij/zij bestellingen op jouw naam doen bijvoorbeeld.<label></div>
<div class='radio'><label><input type='radio' name='at081477330415' value='3'>Je kunt van wildvreemde mensen brieven en kaarten in je brievenbus krijgen<label></div>
<div class='radio'><label><input type='radio' name='at081477330415' value='4'>alle antwoorden zijn goed<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477330415' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat081477330415' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at081477330415'] ) &&  $_POST['at081477330415'] == "2" && isset( $_POST['btnat081477330415'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['at081477330415'] ) && $_POST['at081477330415'] !== "2" && isset( $_POST['btnat081477330415'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477330415'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
