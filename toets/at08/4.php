<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at08<hr>
Vraag 4<h4> Wat is de 'host' van een website?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at081477309869' value='1'>Dat is een computer, waar een website op te bereiken is<label></div>
<div class='radio'><label><input type='radio' name='at081477309869' value='2'>Het is een soort 'cookie'<label></div>
<div class='radio'><label><input type='radio' name='at081477309869' value='3'>Dat is een computer<label></div>
<div class='radio'><label><input type='radio' name='at081477309869' value='4'>Een website waar hotel overnachtingen worden aangeboden<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477309869' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat081477309869' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at081477309869'] ) &&  $_POST['at081477309869'] == "1" && isset( $_POST['btnat081477309869'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['at081477309869'] ) && $_POST['at081477309869'] !== "1" && isset( $_POST['btnat081477309869'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477309869'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
