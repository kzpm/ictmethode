<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at08<hr>
Vraag 9<h4> Welk besturingssysteem verzamelt in de laatste versie meer data dan in de vorige versie?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine 
gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at081477400817' value='1'>Windows 10<label></div>
<div class='radio'><label><input type='radio' name='at081477400817' value='2'>Windows 7<label></div>
<div class='radio'><label><input type='radio' name='at081477400817' value='3'>Apple OSx<label></div>
<div class='radio'><label><input type='radio' name='at081477400817' value='4'>Linux<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477400817' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat081477400817' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at081477400817'] ) &&  $_POST['at081477400817'] == "1" && isset( $_POST['btnat081477400817'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['at081477400817'] ) && $_POST['at081477400817'] !== "1" && isset( $_POST['btnat081477400817'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477400817'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
