<?php
session_start();
$rdir=$_SERVER["DOCUMENT_ROOT"];         
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( $rdir."logout.php" )</script>';

                }//end if


require_once $incdir.'header.inc';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";

echo "<div class='container'>at08<hr>
Vraag 3<h4> Wat kun je doen om er voor te zorgen dat een kwetsende foto of video niet verder wordt gedeeld?</h4>(Er zijn meerdere antwoorden mogelijk!)<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qcheck' action ='' method= 'POST'>
<div class='checkbox'><label><input type='checkbox' name='at0814773096041' value='2'>Je kunt er niet zo veel aan doen<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0814773096042' value='3'>Heel soms moet je naar de rechter toe om te vragen of hij het strafbaar kan stellen, wanneer iemand je foto of filmpje blijft delen.<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0814773096043' value='4'>Je kunt de persoon, die het filmpje of de foto heeft geplaatst, vragen of hij/zij het wil verwijderen<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0814773096044' value='5'>Alle antwoorden zijn goed<label></div>
<!--<button type='submit' name='btn21477309604' value='1'>Ok</button>-->
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477309604' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn21477309604' value='Ok'>
</form>
<hr></div>";
$array1=array();
  foreach( $_POST as $name => $value) {
        		if ( $value !='Ok' )
        			$array1[]= $value;
   	 	 }//end foreach

$array2=explode( ",",'5');
//$res1= array_product( $array1 );
//$res2= array_product( $array2 );
//echo "Array1 ".array_product( $array1 ).'<br>';
//echo "Array2 ".array_product( $array2 ).'<br>';
//echo "Product totaal ".$res1*$res2;
if ( array_product( $array1 )== array_product( $array2) && isset( $_POST["btn21477309604"] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if ( array_product( $array1 )!== array_product( $array2) && isset( $_POST["btn21477309604"] ) ){
        		
$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if
if ( isset( $_POST['btnCancel1477309604'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

