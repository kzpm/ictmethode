<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at08<hr>
Vraag 5<h4> Is het mogelijk om zoekresultaten over jezelf weg te halen in de zoekmachine?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at081477310067' value='1'>Nee dat is niet mogelijk<label></div>
<div class='radio'><label><input type='radio' name='at081477310067' value='2'>Ja je moet gewoon twee keer op de 'Delete'toets drukken<label></div>
<div class='radio'><label><input type='radio' name='at081477310067' value='3'>Het is wel mogelijk, maar het kan alleen bij foto's<label></div>
<div class='radio'><label><input type='radio' name='at081477310067' value='4'>Het is wel mogelijk, maar erg moeilijk.<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477310067' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat081477310067' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at081477310067'] ) &&  $_POST['at081477310067'] == "4" && isset( $_POST['btnat081477310067'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['at081477310067'] ) && $_POST['at081477310067'] !== "4" && isset( $_POST['btnat081477310067'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477310067'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
