<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at08<hr>
Vraag 10<h4> Kun je er voor zorgen dat jouw Facebook account alleen vriendschapsverzoeken ontvangt van vrienden van jouw vrienden?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at081477401074' value='1'>Nee dat kan niet. iedereen op de hele wereld kan je op Facebook een vriendschapsverzoek sturen<label></div>
<div class='radio'><label><input type='radio' name='at081477401074' value='2'>Ja dat kun je instellen. Het staat standaard niet aan.<label></div>
<div class='radio'><label><input type='radio' name='at081477401074' value='3'>Het is alleen mogelijk wanneer je account ouder is dan twee jaar<label></div>
<div class='radio'><label><input type='radio' name='at081477401074' value='4'>Je kunt het alleen maar aan of uitzetten. Dus of iedereen kan je een verzoek sturen of niemand<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477401074' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat081477401074' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at081477401074'] ) &&  $_POST['at081477401074'] == "2" && isset( $_POST['btnat081477401074'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,1 );
echo '<script>location.replace("11.php");</script>';

}//end if

if (  isset( $_POST['at081477401074'] ) && $_POST['at081477401074'] !== "2" && isset( $_POST['btnat081477401074'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,0 );
echo '<script>location.replace("11.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477401074'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
