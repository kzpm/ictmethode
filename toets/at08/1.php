<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at08<hr>
Vraag 1<h4> Welke informatie over jezelf kun je laten verwijderen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at081477309056' value='1'>Informatie over jou, die met jouw toestemming geplaatst is<label></div>
<div class='radio'><label><input type='radio' name='at081477309056' value='2'>Informatie over jou, die kwetsend of beledigend is<label></div>
<div class='radio'><label><input type='radio' name='at081477309056' value='3'>Je kunt alleen foto's van jezelf laten verwijderen, die zonder toestemming van jou zijn geplaatst<label></div>
<div class='radio'><label><input type='radio' name='at081477309056' value='4'>Het is niet mogelijk om informatie over je zelf te laten verwijderen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477309056' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat081477309056' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at081477309056'] ) &&  $_POST['at081477309056'] == "2" && isset( $_POST['btnat081477309056'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['at081477309056'] ) && $_POST['at081477309056'] !== "2" && isset( $_POST['btnat081477309056'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477309056'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
