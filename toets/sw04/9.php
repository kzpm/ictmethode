<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw04<hr>
Vraag 9<h4> Zoek het op. Welke software kun je beter niet verwijderen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw041477915224' value='1'>Software, die nodig is om je surfgedrag op internet door te geven<label></div>
<div class='radio'><label><input type='radio' name='sw041477915224' value='2'>Software, die nodig is om je computer te laten werken<label></div>
<div class='radio'><label><input type='radio' name='sw041477915224' value='3'>Software, die met een installatie-wizard is geinstalleerd<label></div>
<div class='radio'><label><input type='radio' name='sw041477915224' value='4'>Een programma waar je heel veel voor hebt betaald<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477915224' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw041477915224' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw041477915224'] ) &&  $_POST['sw041477915224'] == "2" && isset( $_POST['btnsw041477915224'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['sw041477915224'] ) && $_POST['sw041477915224'] !== "2" && isset( $_POST['btnsw041477915224'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477915224'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
