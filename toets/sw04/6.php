<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw04<hr>
Vraag 6<h4> Kun je er zeker van zijn dat een programma helemaal verdwenen is wanneer je het zelf moet doen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw041477914269' value='1'>Ja dat werkt prima<label></div>
<div class='radio'><label><input type='radio' name='sw041477914269' value='2'>Ja, je moet er om denken om ook in het register te zoeken<label></div>
<div class='radio'><label><input type='radio' name='sw041477914269' value='3'>Nee, je kunt er niet helemaal zeker van zijn. Niet alle bestanden hebben na installatie dezelfde naam als het programma<label></div>
<div class='radio'><label><input type='radio' name='sw041477914269' value='4'>Na het zelf verwijderen van software, moet je de virusscanner ook nog aan zetten om te kijken of er nog stukjes software overgebleven zijn.<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477914269' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw041477914269' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw041477914269'] ) &&  $_POST['sw041477914269'] == "3" && isset( $_POST['btnsw041477914269'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['sw041477914269'] ) && $_POST['sw041477914269'] !== "3" && isset( $_POST['btnsw041477914269'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477914269'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
