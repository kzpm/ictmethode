<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw04<hr>
Vraag 1<h4> Wat betekent 'de-installeren'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw041477912765' value='1'>Het omgekeerde van 'installeren'<label></div>
<div class='radio'><label><input type='radio' name='sw041477912765' value='2'>Software installeren<label></div>
<div class='radio'><label><input type='radio' name='sw041477912765' value='3'>Software gedeeltelijk van je computer verwijderen<label></div>
<div class='radio'><label><input type='radio' name='sw041477912765' value='4'>Alleen op tablets software verwijderen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477912765' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw041477912765' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw041477912765'] ) &&  $_POST['sw041477912765'] == "1" && isset( $_POST['btnsw041477912765'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['sw041477912765'] ) && $_POST['sw041477912765'] !== "1" && isset( $_POST['btnsw041477912765'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477912765'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
