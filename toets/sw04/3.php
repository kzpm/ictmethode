<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw04<hr>
Vraag 3<h4> Hoe verwijder je in Windows bij voorkeur software?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw041477913167' value='1'>Door het zelf bestandje voor bestandje te verwijderen<label></div>
<div class='radio'><label><input type='radio' name='sw041477913167' value='2'>Door het programma met de virusscanner te laten verwijderen<label></div>
<div class='radio'><label><input type='radio' name='sw041477913167' value='3'>Via 'Configuratiescherm | Netwerk en Internet |'. Daarna uit de programmalijst de programma's kiezen, die je wilt verwijderen<label></div>
<div class='radio'><label><input type='radio' name='sw041477913167' value='4'>Via 'Configuratiescherm | Programma's |'. Daarna uit de programmalijst de programma's kiezen, die je wilt verwijderen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477913167' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw041477913167' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw041477913167'] ) &&  $_POST['sw041477913167'] == "4" && isset( $_POST['btnsw041477913167'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['sw041477913167'] ) && $_POST['sw041477913167'] !== "4" && isset( $_POST['btnsw041477913167'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477913167'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
