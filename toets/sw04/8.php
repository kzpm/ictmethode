<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw04<hr>
Vraag 8<h4> Hoe kun je in Android een app verwijderen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw041477915050' value='1'>Dat kan niet, de apps verdwijnen vanzelf<label></div>
<div class='radio'><label><input type='radio' name='sw041477915050' value='2'>Druk op het icoontje en kies daarna voor 'verwijderen'<label></div>
<div class='radio'><label><input type='radio' name='sw041477915050' value='3'>Ga naar 'Instellingen' en kies daarna 'Wifi instellingen'<label></div>
<div class='radio'><label><input type='radio' name='sw041477915050' value='4'>Ga naar 'Instellingen' en kies daarna 'Apps'. Kies vervolgens een app uit de lijst die je wilt verwijderen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477915050' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw041477915050' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw041477915050'] ) &&  $_POST['sw041477915050'] == "4" && isset( $_POST['btnsw041477915050'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['sw041477915050'] ) && $_POST['sw041477915050'] !== "4" && isset( $_POST['btnsw041477915050'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477915050'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
