<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";
   
                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( ."/logout.php" )</script>';

                }//end if

require_once $incdir.'header.inc';
require_once $srcdir.'clsLevenshtein.class.php';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="sw06";

$myLevenshtein = new clsLevenshtein();
echo "<div class='container'>sw06<hr>";
echo "Vraag 4<h4>Wat betekent `Task manager` in het Nederlands?</h4>Je mag een zoekmachine gebruiken!<br><br>
<form name='qopen' action ='' method= 'POST' autocomplete='off'>
<label for='sw061478005622'>Antwoord</label><input type='text' class='form-control' id='sw061478005622' name='sw061478005622' >
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478005622' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn31478005622' value='Ok'>
</form>
<hr></div>";

$array1=explode(" ","Taakbeheer");
if (  isset( $_POST["sw061478005622"]) && $myLevenshtein->fLevenshtein( $array1, $_POST["sw061478005622"] ) && !empty( $_POST['sw061478005622'] ) && isset( $_POST['sw061478005622'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if
if (  isset( $_POST["sw061478005622"]) && !$myLevenshtein->fLevenshtein( $array1, $_POST["sw061478005622"] ) && !empty( $_POST['sw061478005622'] ) && isset( $_POST['sw061478005622'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478005622'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

