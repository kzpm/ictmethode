<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw06<hr>
Vraag 8<h4> In de dienstenlijst van Windows zie je vaak een service staan, die steeds checkt of de AdobeFlashPlayer geupdate moet worden. Kun je deze service 
uitschakelen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw071478006352' value='1'>Nee die service kun je niet uitschakelen<label></div>
<div class='radio'><label><input type='radio' name='sw071478006352' value='2'>Je kunt deze service zonder al te veel problemen uitschakelen. Je moet dan wel zelf in de gaten houden of er al een nieuwe update van Flash is<label></div>
<div class='radio'><label><input type='radio' name='sw071478006352' value='3'>Je computer zal waarschijnlijk helemaal vastlopen wanneer je deze service uitschakelt<label></div>
<div class='radio'><label><input type='radio' name='sw071478006352' value='4'>Alle antwoorden kloppen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478006352' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw071478006352' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw071478006352'] ) &&  $_POST['sw071478006352'] == "2" && isset( $_POST['btnsw071478006352'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['sw071478006352'] ) && $_POST['sw071478006352'] !== "2" && isset( $_POST['btnsw071478006352'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478006352'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
