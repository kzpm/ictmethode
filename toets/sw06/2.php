<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw06<hr>
Vraag 2<h4> Is het nodig om voor Windows extra programma's te downloaden om te kijken wat je computer allemaal doet?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw061478005361' value='1'>Nee dat is niet nodig. Je kunt met 'Processen' in 'Taakbeheer' goed zien welke programma's allemaal actief zijn<label></div>
<div class='radio'><label><input type='radio' name='sw061478005361' value='2'>Nee dat is niet nodig. Via de Verkenner kun je precies zien welke programma's actief zijn<label></div>
<div class='radio'><label><input type='radio' name='sw061478005361' value='3'>Ja je moet vaak extra software downloaden en installeren in Windows om te kijken wat er allemaal actief is<label></div>
<div class='radio'><label><input type='radio' name='sw061478005361' value='4'>Ja<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478005361' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw061478005361' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw061478005361'] ) &&  $_POST['sw061478005361'] == "1" && isset( $_POST['btnsw061478005361'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['sw061478005361'] ) && $_POST['sw061478005361'] !== "1" && isset( $_POST['btnsw061478005361'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478005361'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
