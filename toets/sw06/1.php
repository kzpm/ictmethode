<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw06<hr>
Vraag 1<h4> Waarom is het handig om te weten waar je computer mee bezig is?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw061478005138' value='1'>Het is gewoon interessant.<label></div>
<div class='radio'><label><input type='radio' name='sw061478005138' value='2'>Je kunt zo goed zien of je computer gehackt wordt<label></div>
<div class='radio'><label><input type='radio' name='sw061478005138' value='3'>Wanneer je computer niet goed presteert is het handig om te weten of je mail nog openstaat<label></div>
<div class='radio'><label><input type='radio' name='sw061478005138' value='4'>Wanneer je computer niet goed presteert is het handig om te weten, welke programma's allemaal actief zijn<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478005138' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw061478005138' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw061478005138'] ) &&  $_POST['sw061478005138'] == "4" && isset( $_POST['btnsw061478005138'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['sw061478005138'] ) && $_POST['sw061478005138'] !== "4" && isset( $_POST['btnsw061478005138'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478005138'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
