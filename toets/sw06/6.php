<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw06<hr>
Vraag 6<h4> Welke opdracht kunje in Windows geven om 'systeemservices' in - en uit te schakelen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw061478005771' value='1'>Het commando `services`<label></div>
<div class='radio'><label><input type='radio' name='sw061478005771' value='2'>Het commando `spconfig`<label></div>
<div class='radio'><label><input type='radio' name='sw061478005771' value='3'>Het commando `msconfig`<label></div>
<div class='radio'><label><input type='radio' name='sw061478005771' value='4'>Het commando `exit`<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478005771' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw061478005771' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw061478005771'] ) &&  $_POST['sw061478005771'] == "3" && isset( $_POST['btnsw061478005771'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['sw061478005771'] ) && $_POST['sw061478005771'] !== "3" && isset( $_POST['btnsw061478005771'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478005771'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
