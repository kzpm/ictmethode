<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw06<hr>
Vraag 3<h4> Wat doet de 'systeemmonitor'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw061478005532' value='1'>Dit programma geeft aan of er een virus op je pc staat<label></div>
<div class='radio'><label><input type='radio' name='sw061478005532' value='2'>Met dit programma kun je zien hoeveel geheugen bepaalde programma's in beslag nemen<label></div>
<div class='radio'><label><input type='radio' name='sw061478005532' value='3'>Met dit programma kun je in zien wie er allemaal aangemeld zijn op je computer<label></div>
<div class='radio'><label><input type='radio' name='sw061478005532' value='4'>Met dit programma kun je je monitor instellen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478005532' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw061478005532' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw061478005532'] ) &&  $_POST['sw061478005532'] == "2" && isset( $_POST['btnsw061478005532'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['sw061478005532'] ) && $_POST['sw061478005532'] !== "2" && isset( $_POST['btnsw061478005532'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478005532'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
