<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw06<hr>
Vraag 7<h4> Is het mogelijk om alle processen die openstaan in 'Taakbeheer' uit te schakelen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw061478006157' value='1'>Nee dat is niet mogelijk<label></div>
<div class='radio'><label><input type='radio' name='sw061478006157' value='2'>Het is mogelijk, maar dan moet je wel 'beheerder' zijn<label></div>
<div class='radio'><label><input type='radio' name='sw061478006157' value='3'>Het is wel mogelijk wanneer je de virusscanner uitschakelt<label></div>
<div class='radio'><label><input type='radio' name='sw061478006157' value='4'>Het is alleen mogelijk wanneer de computer uitstaat<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478006157' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw061478006157' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw061478006157'] ) &&  $_POST['sw061478006157'] == "1" && isset( $_POST['btnsw061478006157'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['sw061478006157'] ) && $_POST['sw061478006157'] !== "1" && isset( $_POST['btnsw061478006157'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478006157'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
