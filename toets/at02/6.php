<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at02<hr>
Vraag 6<h4> Waarom blijven gegevens zolang op internet 'rondhangen'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at021476879587' value='1'>Omdat gebruikers, die de gegevens hebben gepost, dat graag willen<label></div>
<div class='radio'><label><input type='radio' name='at021476879587' value='2'>Hoe langer gegevens bewaard blijven, hoe 
completer je profiel wordt<label></div>
<div class='radio'><label><input type='radio' name='at021476879587' value='3'>Omdat de gegevens onjuist zijn<label></div>
<div class='radio'><label><input type='radio' name='at021476879587' 
value='4'>Geen van de antwoorden klopt<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1476879587' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat021476879587' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at021476879587'] ) &&  $_POST['at021476879587'] == "2" && isset( $_POST['btnat021476879587'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['at021476879587'] ) && $_POST['at021476879587'] !== "2" && isset( $_POST['btnat021476879587'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1476879587'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
