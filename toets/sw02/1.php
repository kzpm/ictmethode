<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw02<hr>
Vraag 1<h4> Wat klopt er over 'Freeware'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw021477567516' value='1'>Het is gratis, maar de kwaliteit van de software kan niet gecontroleerd worden<label></div>
<div class='radio'><label><input type='radio' name='sw021477567516' value='2'>Na een periode moet je toch voor Freeware betalen<label></div>
<div class='radio'><label><input type='radio' name='sw021477567516' value='3'>Je kunt de broncode van Freeware controleren<label></div>
<div class='radio'><label><input type='radio' name='sw021477567516' value='4'>Geen van de antwoorden is juist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477567516' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw021477567516' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw021477567516'] ) &&  $_POST['sw021477567516'] == "1" && isset( $_POST['btnsw021477567516'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['sw021477567516'] ) && $_POST['sw021477567516'] !== "1" && isset( $_POST['btnsw021477567516'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477567516'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
