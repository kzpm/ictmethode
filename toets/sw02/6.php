<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw02<hr>
Vraag 6<h4> Wat is het verschil tussen open-source en vrije software?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw021477568050' value='1'>Voor open-source software moet je betalen, voor vrije software niet<label></div>
<div class='radio'><label><input type='radio' name='sw021477568050' value='2'>Open-source software aanhangers vinden dat de software controleerbaar moet ziijn, aanhangers van vrije software vinden dat de software vrij te gebruiken moet zijn<label></div>
<div class='radio'><label><input type='radio' name='sw021477568050' value='3'>Er zijn eigenlijk geen verschillen<label></div>
<div class='radio'><label><input type='radio' name='sw021477568050' value='4'>Geen van de antwoorden is juist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477568050' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw021477568050' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw021477568050'] ) &&  $_POST['sw021477568050'] == "2" && isset( $_POST['btnsw021477568050'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['sw021477568050'] ) && $_POST['sw021477568050'] !== "2" && isset( $_POST['btnsw021477568050'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477568050'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
