<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";
   
                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( ."/logout.php" )</script>';

                }//end if

require_once $incdir.'header.inc';
require_once $srcdir.'clsLevenshtein.class.php';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode = "os00";

$myLevenshtein = new clsLevenshtein();
echo "<div class='container'>os00<hr>";
echo "Vraag 5<h4>In welk geheugen van de computer worden gegevens tijdelijk opgeslagen? (*intern* of *extern*)</h4>Je mag een zoekmachine gebruiken!<br><br>
<form name='qopen' action ='' method= 'POST' autocomplete=off>
<label for='os001461682357'>Antwoord</label><input type='text' class='form-control' id='os001461682357' name='os001461682357' >
<input type='submit' class='btn btn-warning black-text' name='btnCancel1461682357' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn31461682357' value='Ok'>
</form>
<hr></div>";

$array1=explode(" ","intern");
if (  isset( $_POST["os001461682357"]) && $myLevenshtein->fLevenshtein( $array1, $_POST["os001461682357"] ) && !empty( $_POST['os001461682357'] ) && isset( $_POST['os001461682357'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if
if (  isset( $_POST["os001461682357"]) && !$myLevenshtein->fLevenshtein( $array1, $_POST["os001461682357"] ) && !empty( $_POST['os001461682357'] ) && isset( $_POST['os001461682357'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1461682357'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

