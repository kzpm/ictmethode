<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw01<hr>
Vraag 6<h4> Heeft 'doellijn technologie' ook met software te maken?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw011477558445' value='1'>Nee, hierbij worden alleen maar camera's gebruikt<label></div>
<div class='radio'><label><input type='radio' name='sw011477558445' value='2'>Nee, er is geen software nodig voor doellijn technologie<label></div>
<div class='radio'><label><input type='radio' name='sw011477558445' value='3'>Ja, want sporters moeten zich kunnen registreren.<label></div>
<div class='radio'><label><input type='radio' name='sw011477558445' value='4'>Ja er is software nodig om doellijn-technologie te laten werken<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477558445' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw011477558445' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw011477558445'] ) &&  $_POST['sw011477558445'] == "4" && isset( $_POST['btnsw011477558445'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['sw011477558445'] ) && $_POST['sw011477558445'] !== "4" && isset( $_POST['btnsw011477558445'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477558445'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
