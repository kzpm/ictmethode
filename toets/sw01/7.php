<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw01<hr>
Vraag 7<h4> Welke gegevens kan een Formule 1 coureur goed gebruiken om een betere race te rijden?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw011477560082' value='1'>Windsnelheid, temperatuur, brandstofpeil, toerental van de motor<label></div>
<div class='radio'><label><input type='radio' name='sw011477560082' value='2'>Windsnelheid, locatie van fietsenstalling, toerental, oliepeil<label></div>
<div class='radio'><label><input type='radio' name='sw011477560082' value='3'>Brandstofpeil, rondetijd, toerental, locatie van claxon<label></div>
<div class='radio'><label><input type='radio' name='sw011477560082' value='4'>Snelheid, Afstand, aantal rondes nog te rijden, wifi signaal<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477560082' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw011477560082' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw011477560082'] ) &&  $_POST['sw011477560082'] == "1" && isset( $_POST['btnsw011477560082'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['sw011477560082'] ) && $_POST['sw011477560082'] !== "1" && isset( $_POST['btnsw011477560082'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477560082'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
