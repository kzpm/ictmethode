<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw01<hr>
Vraag 2<h4> Welke overheidsdienst maakt veel gebruik van inkomensgegevens van burgers?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw011477557704' value='1'>De RDW<label></div>
<div class='radio'><label><input type='radio' name='sw011477557704' value='2'>Het Ministerie van Defensie<label></div>
<div class='radio'><label><input type='radio' name='sw011477557704' value='3'>Het ministerie van Landbouw en Visserij<label></div>
<div class='radio'><label><input type='radio' name='sw011477557704' value='4'>De belastingdienst<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477557704' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw011477557704' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw011477557704'] ) &&  $_POST['sw011477557704'] == "4" && isset( $_POST['btnsw011477557704'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['sw011477557704'] ) && $_POST['sw011477557704'] !== "4" && isset( $_POST['btnsw011477557704'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477557704'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
