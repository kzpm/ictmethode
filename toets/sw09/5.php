<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw09<hr>
Vraag 5<h4> Zoek op wat `BSOD` betekent</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw091478179438' value='1'>Het betekent: 'Bad System Online Detection'. Hiermee wil je computer waarschuwen dat je nog geen updates geinstalleerd hebt.<label></div>
<div class='radio'><label><input type='radio' name='sw091478179438' value='2'>Het betekent: 'Blue Screen Of Death'. Meestal krijg je in Windows dan een blauw scherm in beeld met allemaal foutmeldingen in witte lettertjes. Er is waarschijnlijk een fout in de hardware.<label></div>
<div class='radio'><label><input type='radio' name='sw091478179438' value='3'>Het betekent, dat je er ergens op je systeem software staat, die niet juist is geinstalleerd<label></div>
<div class='radio'><label><input type='radio' name='sw091478179438' value='4'>Alle antwoorden zijn goed<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478179438' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw091478179438' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw091478179438'] ) &&  $_POST['sw091478179438'] == "2" && isset( $_POST['btnsw091478179438'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['sw091478179438'] ) && $_POST['sw091478179438'] !== "2" && isset( $_POST['btnsw091478179438'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478179438'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
