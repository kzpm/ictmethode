<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw09<hr>
Vraag 7<h4> Hoe zorg je ervoor dat je een taak in Windows kunt beeindigen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw091478180182' value='1'>Je kunt het beste de computer uitzetten<label></div>
<div class='radio'><label><input type='radio' name='sw091478180182' value='2'>Je moet de computer herstarten<label></div>
<div class='radio'><label><input type='radio' name='sw091478180182' value='3'>Druk op CTRL+ALT+INS, kies het programma uit en klik op de knop 'Proces beeindigen'<label></div>
<div class='radio'><label><input type='radio' name='sw091478180182' value='4'>Druk op CTRL+ALT+DEL, kies het programma uit in Taakbeheer en klik op de knop 'Proces 
beeindigen'<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478180182' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw091478180182' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw091478180182'] ) &&  $_POST['sw091478180182'] == "4" && isset( $_POST['btnsw091478180182'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['sw091478180182'] ) && $_POST['sw091478180182'] !== "4" && isset( $_POST['btnsw091478180182'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478180182'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
