<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw09<hr>
Vraag 3<h4> Wat kan er aan de hand zijn wanneer een programma traag is?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw091478178592' value='1'>Het programma of een ander programma, vraagt te veel geheugenruimte<label></div>
<div class='radio'><label><input type='radio' name='sw091478178592' value='2'>Wanneer je een programma weinig gebruikt, wordt het traag<label></div>
<div class='radio'><label><input type='radio' name='sw091478178592' value='3'>Dit kan alleen maar, wanneer alle andere programma's zijn afgesloten<label></div>
<div class='radio'><label><input type='radio' name='sw091478178592' value='4'>Hoe sneller je computer, hoe trager de programma's worden<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478178592' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw091478178592' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw091478178592'] ) &&  $_POST['sw091478178592'] == "1" && isset( $_POST['btnsw091478178592'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['sw091478178592'] ) && $_POST['sw091478178592'] !== "1" && isset( $_POST['btnsw091478178592'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478178592'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
