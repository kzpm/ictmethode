<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw09<hr>
Vraag 4<h4> Wat is er aan de hand wanneer je de melding `Dit programma voldoet niet aan de Windows Logo test` in beeld krijgt?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw091478179098' value='1'>De software is niet in de `Logo` taal geschreven<label></div>
<div class='radio'><label><input type='radio' name='sw091478179098' value='2'>De makers van Windows vinden de software niet goed<label></div>
<div class='radio'><label><input type='radio' name='sw091478179098' value='3'>De makers van Windows hebben de software niet gecontroleerd en waarschuwen je<label></div>
<div class='radio'><label><input type='radio' name='sw091478179098' value='4'>Het logo van Windows ontbreekt<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478179098' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw091478179098' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw091478179098'] ) &&  $_POST['sw091478179098'] == "3" && isset( $_POST['btnsw091478179098'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['sw091478179098'] ) && $_POST['sw091478179098'] !== "3" && isset( $_POST['btnsw091478179098'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478179098'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
