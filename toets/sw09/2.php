<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw09<hr>
Vraag 2<h4> Is het ook mogelijk dat je computer door een netwerkstoring kan vastlopen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw091478178374' value='1'>Nee, de computer blijft gewoon doorwerken<label></div>
<div class='radio'><label><input type='radio' name='sw091478178374' value='2'>Ja het is mogelijk<label></div>
<div class='radio'><label><input type='radio' name='sw091478178374' value='3'>Ja het kan wanneer je op een tablet werkt<label></div>
<div class='radio'><label><input type='radio' name='sw091478178374' value='4'>Nee het is niet mogelijk<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478178374' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw091478178374' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw091478178374'] ) &&  $_POST['sw091478178374'] == "2" && isset( $_POST['btnsw091478178374'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['sw091478178374'] ) && $_POST['sw091478178374'] !== "2" && isset( $_POST['btnsw091478178374'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478178374'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
