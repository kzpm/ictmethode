<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw09";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw09<hr>
Vraag 8<h4> Wanneer wordt er een wisselbestand op de harde schijf aangemaakt?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw091478180344' value='1'>Wanneer je muziek afspeelt<label></div>
<div class='radio'><label><input type='radio' name='sw091478180344' value='2'>Wanneer er onvoldende ruimte in het geheugen is<label></div>
<div class='radio'><label><input type='radio' name='sw091478180344' value='3'>Wanneer je alle programma's hebt afgesloten<label></div>
<div class='radio'><label><input type='radio' name='sw091478180344' value='4'>Wanneer je alle processen via 'Taakbeheer' hebt uitgeschakeld<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478180344' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw091478180344' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw091478180344'] ) &&  $_POST['sw091478180344'] == "2" && isset( $_POST['btnsw091478180344'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['sw091478180344'] ) && $_POST['sw091478180344'] !== "2" && isset( $_POST['btnsw091478180344'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478180344'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
