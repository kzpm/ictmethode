<?php
session_start();
$rdir=$_SERVER["DOCUMENT_ROOT"];         
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( $rdir."logout.php" )</script>';

                }//end if


require_once $incdir.'header.inc';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="os02";

echo "<div class='container'>$cursuscode<hr>
Vraag 10<h4> Hoe kun je een bestand terugvinden, wat je in een netwerk per ongeluk hebt verwijderd?</h4>(Er zijn meerdere antwoorden mogelijk!)<br>Je mag een zoekmachine gebruiken!<br>
<form name='qcheck' action ='' method= 'POST' autocomplete=off>
<div class='checkbox'><label><input type='checkbox' name='os0114550305851' value='2'>Bestanden, die van een netwerk worden verwijderd, kun je niet meer terughalen<label></div>
<div class='checkbox'><label><input type='checkbox' name='os0114550305852' value='3'>Je kunt ze alleen weer terughalen wanneer je het binnen 10 minuten doet<label></div>
<div class='checkbox'><label><input type='checkbox' name='os0114550305853' value='4'>Je kunt ze terughalen door de versiegeschiedenis van de map waarin ze stonden, te bekijken<label></div>
<div class='checkbox'><label><input type='checkbox' name='os0114550305854' value='5'>Wanneer je een back-up van het bestand hebt, kun je dat bestand weer terugzetten<label></div>
<!--<button type='submit' name='btn21455030585' value='1'>Ok</button>-->
<input type='submit' class='btn btn-warning black-text' name='btnCancel1455030585' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn21455030585' value='Ok'>
</form>
<hr></div>";
$array1=array();
  foreach( $_POST as $name => $value) {
        		if ( $value !='Ok' )
        			$array1[]= $value;
   	 	 }//end foreach

$array2=explode( ",",'4,5');
//$res1= array_product( $array1 );
//$res2= array_product( $array2 );
//echo "Array1 ".array_product( $array1 ).'<br>';
//echo "Array2 ".array_product( $array2 ).'<br>';
//echo "Product totaal ".$res1*$res2;
if ( array_product( $array1 )== array_product( $array2) && isset( $_POST["btn21455030585"] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,1 );
$nextpage=11;
echo '<script>location.replace("11.php");</script>';

}//end if

if ( array_product( $array1 )!== array_product( $array2) && isset( $_POST["btn21455030585"] ) ){
        		
$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,0 );
$nextpage=10+1;
echo '<script>location.replace("11.php");</script>';


}//end if
if ( isset( $_POST['btnCancel1455030585'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

