<?php
session_start();

$rdir = $_SERVER["DOCUMENT_ROOT"].'';
$srcdir = $rdir.'/src/';
$incdir = $rdir.'/inc/';

require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsChkSession.class.php';


$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace( $rdir."logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os02";
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );

echo "<div class='container'>$cursuscode<hr>
Vraag 1<h4> Wat is waar over 'kopieren'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST' autocomplete=off>
<div class='radio'><label><input type='radio' name='os011455009428' value='1'>Wanneer je een bestand kopieert heb je twee gelijke bestanden.<label></div>
<div class='radio'><label><input type='radio' name='os011455009428' value='2'>Wanneer je een bestand kopieert, kopieer je alleen de naam van het bestand<label></div>
<div class='radio'><label><input type='radio' name='os011455009428' value='3'>Wanneer je een bestand kopieert, verdwijnt het origineel.<label></div>
<div class='radio'><label><input type='radio' name='os011455009428' value='4'>Geen van de antwoorden is waar.<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1455009428' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos011455009428' value='Ok'>
</form>
 <hr></div>";


if ( isset( $_POST['os011455009428'] ) &&  $_POST['os011455009428'] == "1" && isset( $_POST['btnos011455009428'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
$nextpage=2;
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['os011455009428'] ) && $_POST['os011455009428'] !== "1" && isset( $_POST['btnos011455009428'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
$nextpage=2;
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1455009428'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if
?>
