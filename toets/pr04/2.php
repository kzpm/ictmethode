<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr04<hr>
Vraag 2<h4> Als t >= 0 en je weet dat t gelijk is aan 3, klopt de vergelijking dan?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr041479981802' value='1'>Het klopt alleen als t ook gelijk is aan 0<label></div>
<div class='radio'><label><input type='radio' name='pr041479981802' value='2'>Nee, want 3 is NIET kleiner dan of gelijk aan 0<label></div>
<div class='radio'><label><input type='radio' name='pr041479981802' value='3'>Nee, 0 is groter dan 3<label></div>
<div class='radio'><label><input type='radio' name='pr041479981802' value='4'>Ja, want 3 is groter dan of gelijk aan 0<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479981802' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr041479981802' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr041479981802'] ) &&  $_POST['pr041479981802'] == "2" && isset( $_POST['btnpr041479981802'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['pr041479981802'] ) && $_POST['pr041479981802'] !== "2" && isset( $_POST['btnpr041479981802'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479981802'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
