<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr04<hr>
Vraag 8<h4> Welke bewerking zie je in de som `16/2`?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr041479982426' value='1'>Vermenigvuldigen<label></div>
<div class='radio'><label><input type='radio' name='pr041479982426' value='2'>Delen<label></div>
<div class='radio'><label><input type='radio' name='pr041479982426' value='3'>Optellen<label></div>
<div class='radio'><label><input type='radio' name='pr041479982426' value='4'>Aftrekken<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1479982426' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr041479982426' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr041479982426'] ) &&  $_POST['pr041479982426'] == "2" && isset( $_POST['btnpr041479982426'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['pr041479982426'] ) && $_POST['pr041479982426'] !== "2" && isset( $_POST['btnpr041479982426'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1479982426'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
