<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at03<hr>
Vraag 8<h4> Waar moet een 'veilig' wachtwoord aan voldoen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at031477043194' value='1'>Het moet maximaal uit drie letters bestaan, maar je moet geen cijfers en geen symbolen gebruiken<label></div>
<div class='radio'><label><input type='radio' name='at031477043194' value='2'>Het moet uit minimaal 11 tekens bestaan, waarin hoofdletters, kleine letters, symbolen en cijfers voorkomen<label></div>
<div class='radio'><label><input type='radio' name='at031477043194' value='3'>Het moet uit minimaal 30 letters bestaan en het woord wat je bedenkt moet een bestaand woord zijn.<label></div>
<div class='radio'><label><input type='radio' name='at031477043194' value='4'>Het beste wachtwoord is helemaal niks, dan hoef je ook niet zoveel te onthouden<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477043194' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat031477043194' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at031477043194'] ) &&  $_POST['at031477043194'] == "2" && isset( $_POST['btnat031477043194'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['at031477043194'] ) && $_POST['at031477043194'] !== "2" && isset( $_POST['btnat031477043194'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477043194'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
