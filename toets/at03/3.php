<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at03<hr>
Vraag 3<h4> Wat betekent 'toegangscontrole'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at031477041539' value='1'>De bewaking bij een vliegveld<label></div>
<div class='radio'><label><input type='radio' name='at031477041539' value='2'>Een camera die checkt of jij echt bent wie je zegt te zijn<label></div>
<div class='radio'><label><input type='radio' name='at031477041539' value='3'>Controle van je gegevens om ergens binnen te mogen<label></div>
<div class='radio'><label><input type='radio' name='at031477041539' value='4'>Alleen de gebruikersnaam is voldoende bij een toegangscontrole<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477041539' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat031477041539' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at031477041539'] ) &&  $_POST['at031477041539'] == "3" && isset( $_POST['btnat031477041539'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['at031477041539'] ) && $_POST['at031477041539'] !== "3" && isset( $_POST['btnat031477041539'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477041539'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
