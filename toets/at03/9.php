<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at03<hr>
Vraag 9<h4> Wat zijn 'Rainbow tables'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at031477043706' value='1'>Dit is een tabel die eruitziet als een regenboog<label></div>
<div class='radio'><label><input type='radio' name='at031477043706' value='2'>Een tafel waarin geheime wachtwoorden worden gekrast<label></div>
<div class='radio'><label><input type='radio' name='at031477043706' value='3'>Een tabel waarmee je goede wachtwoorden kunt maken<label></div>
<div class='radio'><label><input type='radio' name='at031477043706' value='4'>Een tabel waarin alle mogelijke wachtwoorden worden opgeslagen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477043706' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat031477043706' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at031477043706'] ) &&  $_POST['at031477043706'] == "4" && isset( $_POST['btnat031477043706'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['at031477043706'] ) && $_POST['at031477043706'] !== "4" && isset( $_POST['btnat031477043706'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477043706'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
