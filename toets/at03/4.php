<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at03<hr>
Vraag 4<h4> Waarom is een wachtwoord van 4 tekens makkelijk te kraken?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at031477041895' value='1'>Een gewone computer doet over het kraken van zo'n wachtwoord minder dan een minuut<label></div>
<div class='radio'><label><input type='radio' name='at031477041895' value='2'>Bijna iedereen gebruikt zijn eigen naam als wachtwoord<label></div>
<div class='radio'><label><input type='radio' name='at031477041895' value='3'>Omdat er maar weinig woorden zijn met 4 tekens<label></div>
<div class='radio'><label><input type='radio' name='at031477041895' value='4'>Je kunt beter minder dan 4 tekens gebruiken, dat is nog makkelijker<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477041895' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat031477041895' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at031477041895'] ) &&  $_POST['at031477041895'] == "1" && isset( $_POST['btnat031477041895'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['at031477041895'] ) && $_POST['at031477041895'] !== "1" && isset( $_POST['btnat031477041895'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477041895'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
