<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at03<hr>
Vraag 6<h4> Er zijn veel diensten waarmee je met je accountgegevens van Google of Facebook aan kunt melden. Wat is de engelse uitdrukking hiervoor? (Zoek op de afkorting 'SSO')</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at031477042502' value='1'>SSO=Short System Operator<label></div>
<div class='radio'><label><input type='radio' name='at031477042502' value='2'>SSO=Single Show Out<label></div>
<div class='radio'><label><input type='radio' name='at031477042502' value='3'>SSO=Super Sign On<label></div>
<div class='radio'><label><input type='radio' name='at031477042502' value='4'>SSO=Single Sign On<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477042502' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat031477042502' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at031477042502'] ) &&  $_POST['at031477042502'] == "4" && isset( $_POST['btnat031477042502'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['at031477042502'] ) && $_POST['at031477042502'] !== "4" && isset( $_POST['btnat031477042502'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477042502'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
