<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os05<hr>
Vraag 5<h4> Wat is een 'systeemservice'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='o051462368551' value='1'>Een ernstige computercrash<label></div>
<div class='radio'><label><input type='radio' name='o051462368551' value='2'>Iemand die je computer repareert<label></div>
<div class='radio'><label><input type='radio' name='o051462368551' value='3'>Een programma wat op de 'achtergrond' steeds actief blijft. (Vb. virusscanner)<label></div>
<div class='radio'><label><input type='radio' name='o051462368551' value='4'>Geen van de antwoorden kloppen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462368551' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btno051462368551' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['o051462368551'] ) &&  $_POST['o051462368551'] == "3" && isset( $_POST['btno051462368551'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['o051462368551'] ) && $_POST['o051462368551'] !== "3" && isset( $_POST['btno051462368551'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462368551'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
