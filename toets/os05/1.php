<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os05<hr>
Vraag 1<h4> Wat is de beste manier om een vastgelopen programma af te sluiten?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os051462364965' value='1'>De stekker eruit trekken<label></div>
<div class='radio'><label><input type='radio' name='os051462364965' value='2'>Via `taakbeheer` het programma proberen te stoppen<label></div>
<div class='radio'><label><input type='radio' name='os051462364965' value='3'>Afmelden<label></div>
<div class='radio'><label><input type='radio' name='os051462364965' value='4'>Computer afsluiten<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462364965' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos051462364965' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os051462364965'] ) &&  $_POST['os051462364965'] == "2" && isset( $_POST['btnos051462364965'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['os051462364965'] ) && $_POST['os051462364965'] !== "2" && isset( $_POST['btnos051462364965'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462364965'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
