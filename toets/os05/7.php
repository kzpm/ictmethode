<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os05<hr>
Vraag 7<h4> Wat is een 'harde reset' van je computer of tablet?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os051462368998' value='1'>De stroom naar je computer onderbreken<label></div>
<div class='radio'><label><input type='radio' name='os051462368998' value='2'>De computer uitzetten via 'Afmelden'<label></div>
<div class='radio'><label><input type='radio' name='os051462368998' value='3'>Het beeldscherm van de computer uitdoen<label></div>
<div class='radio'><label><input type='radio' name='os051462368998' value='4'>Taakbeheer<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462368998' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos051462368998' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os051462368998'] ) &&  $_POST['os051462368998'] == "1" && isset( $_POST['btnos051462368998'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['os051462368998'] ) && $_POST['os051462368998'] !== "1" && isset( $_POST['btnos051462368998'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462368998'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
