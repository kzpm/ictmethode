<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr01<hr>
Vraag 2<h4> Hoe kan het dat de inhoud van een variabele een tijdje bewaard kan worden door de computer?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr011478851791' value='1'>Doordat de computer erg snel is<label></div>
<div class='radio'><label><input type='radio' name='pr011478851791' value='2'>De waarde van de variabele wordt tijdelijk opgeslagen in het geheugen<label></div>
<div class='radio'><label><input type='radio' name='pr011478851791' value='3'>Dat kan alleen maar als je een usb-stick bij je hebt<label></div>
<div class='radio'><label><input type='radio' name='pr011478851791' value='4'>Een computer moet een internet verbinding hebben om een variabele te onthouden<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478851791' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr011478851791' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr011478851791'] ) &&  $_POST['pr011478851791'] == "2" && isset( $_POST['btnpr011478851791'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['pr011478851791'] ) && $_POST['pr011478851791'] !== "2" && isset( $_POST['btnpr011478851791'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478851791'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
