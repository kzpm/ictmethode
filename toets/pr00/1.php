<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr01<hr>
Vraag 1<h4> Wat is een variabele?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr011478612637' value='1'>Een soort computer<label></div>
<div class='radio'><label><input type='radio' name='pr011478612637' value='2'>Geheugen<label></div>
<div class='radio'><label><input type='radio' name='pr011478612637' value='3'>Een stukje geheugen waar je tijdelijk informatie kunt opslaan<label></div>
<div class='radio'><label><input type='radio' name='pr011478612637' value='4'>Informatie, die niet meer gewist kan worden<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478612637' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr011478612637' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr011478612637'] ) &&  $_POST['pr011478612637'] == "3" && isset( $_POST['btnpr011478612637'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['pr011478612637'] ) && $_POST['pr011478612637'] !== "3" && isset( $_POST['btnpr011478612637'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478612637'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
