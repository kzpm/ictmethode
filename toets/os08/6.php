<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os08<hr>
Vraag 6<h4> Wat kan een oorzaak zijn dat je niet van Wifi via je router gebruik kunt maken?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os081462399780' value='1'>De beveiligingssleutel (wachtwoord) klopt niet<label></div>
<div class='radio'><label><input type='radio' name='os081462399780' value='2'>De beveiligingssleutel is juist<label></div>
<div class='radio'><label><input type='radio' name='os081462399780' value='3'>Je hebt draadloos internet op je computer ingeschakeld<label></div>
<div class='radio'><label><input type='radio' name='os081462399780' value='4'>Je hebt een uniek adres in het Wifi netwerk gekregen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462399780' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos081462399780' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os081462399780'] ) &&  $_POST['os081462399780'] == "1" && isset( $_POST['btnos081462399780'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['os081462399780'] ) && $_POST['os081462399780'] !== "1" && isset( $_POST['btnos081462399780'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462399780'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
