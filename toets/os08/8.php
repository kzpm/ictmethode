<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os08<hr>
Vraag 8<h4> Welke software kan er voor zorgen dat jij niet op internet kunt?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os081462399992' value='1'>De internet browser, het programma waarmee je webpagina's bekijkt<label></div>
<div class='radio'><label><input type='radio' name='os081462399992' value='2'>Mail<label></div>
<div class='radio'><label><input type='radio' name='os081462399992' value='3'>Malware<label></div>
<div class='radio'><label><input type='radio' name='os081462399992' value='4'>Geen van de antwoorden is goed<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462399992' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos081462399992' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os081462399992'] ) &&  $_POST['os081462399992'] == "3" && isset( $_POST['btnos081462399992'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['os081462399992'] ) && $_POST['os081462399992'] !== "3" && isset( $_POST['btnos081462399992'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462399992'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
