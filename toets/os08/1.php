<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os08";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os08<hr>
Vraag 1<h4> De stroom is uitgevallen. Is er nog internet?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os081462396931' value='1'>Nee het is helemaal niet meer mogelijk om te internetten<label></div>
<div class='radio'><label><input type='radio' name='os081462396931' value='2'>Misschien doet het 3G of 4G netwerk op je telefoon het nog wel<label></div>
<div class='radio'><label><input type='radio' name='os081462396931' value='3'>Ja, de meeste routers schakelen over op een accu<label></div>
<div class='radio'><label><input type='radio' name='os081462396931' value='4'>Geen van de antwoorden is 
goed<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462396931' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos081462396931' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os081462396931'] ) &&  $_POST['os081462396931'] == "2" && isset( $_POST['btnos081462396931'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['os081462396931'] ) && $_POST['os081462396931'] !== "2" && isset( $_POST['btnos081462396931'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462396931'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
