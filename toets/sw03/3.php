<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw03<hr>
Vraag 3<h4> Wat is een 'extensie'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw031477908359' value='1'>Dit betekent hetzelfde als 'bestandsnaam'<label></div>
<div class='radio'><label><input type='radio' name='sw031477908359' value='2'>Het is een extra programma wat je kunt installeren<label></div>
<div class='radio'><label><input type='radio' name='sw031477908359' value='3'>Het is het laatste stukje van een bestandsnaam, achter de laatste punt<label></div>
<div class='radio'><label><input type='radio' name='sw031477908359' value='4'>Het is een bestandsnaam waar geen punten in staan<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477908359' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw031477908359' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw031477908359'] ) &&  $_POST['sw031477908359'] == "3" && isset( $_POST['btnsw031477908359'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['sw031477908359'] ) && $_POST['sw031477908359'] !== "3" && isset( $_POST['btnsw031477908359'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477908359'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
