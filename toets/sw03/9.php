<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw03<hr>
Vraag 9<h4> Hoe kun je er achter komen waar een programma zich op je computer heeft geinstalleerd?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw031477909717' value='1'>Na installatie krijg je een mail waar precies in wordt gemeld, waar de geinstalleerde bestanden allemaal staan<label></div>
<div class='radio'><label><input type='radio' name='sw031477909717' value='2'>Als je geluk hebt, wordt er een 'log' aangemaakt, die je later kunt gebruiken om te zien waar alles staat<label></div>
<div class='radio'><label><input type='radio' name='sw031477909717' value='3'>Dat kun je niet weten<label></div>
<div class='radio'><label><input type='radio' name='sw031477909717' value='4'>Je moet contact opnemen met de fabrikant<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477909717' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw031477909717' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw031477909717'] ) &&  $_POST['sw031477909717'] == "2" && isset( $_POST['btnsw031477909717'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['sw031477909717'] ) && $_POST['sw031477909717'] !== "2" && isset( $_POST['btnsw031477909717'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477909717'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
