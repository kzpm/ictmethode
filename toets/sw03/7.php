<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw03<hr>
Vraag 7<h4> Tijdens het installeren van een app, zie je vaak dat de app gebruik wil maken van allerlei voorzieningen op je telefoon. Welke zijn dat?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw031477909326' value='1'>De spelletjes die je eerder installeerde<label></div>
<div class='radio'><label><input type='radio' name='sw031477909326' value='2'>Je documenten<label></div>
<div class='radio'><label><input type='radio' name='sw031477909326' value='3'>Je gegevens opslag, je camera, je contacten, je netwerkverbinding<label></div>
<div class='radio'><label><input type='radio' name='sw031477909326' value='4'>Je downloads en je documenten<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477909326' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw031477909326' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw031477909326'] ) &&  $_POST['sw031477909326'] == "3" && isset( $_POST['btnsw031477909326'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['sw031477909326'] ) && $_POST['sw031477909326'] !== "3" && isset( $_POST['btnsw031477909326'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477909326'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
