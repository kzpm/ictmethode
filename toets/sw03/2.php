<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw03<hr>
Vraag 2<h4> Wat is een 'gecomprimeerd' bestand?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw031477823846' value='1'>Een verzameling van 1 of meer bestanden tot een bestand, waar overbodige informatie uitgehaald is om de omvang van het bestand te beperken.<label></div>
<div class='radio'><label><input type='radio' name='sw031477823846' value='2'>Een gecomprimeerd bestand is altijd kleiner dan het originele bestand<label></div>
<div class='radio'><label><input type='radio' name='sw031477823846' value='3'>Met gecomprimeerd bestand wordt bedoeld: de omvang van het programma, nadat het geinstalleerd is<label></div>
<div class='radio'><label><input type='radio' name='sw031477823846' value='4'>Een gecomprimeerd bestand is een zeer schadelijk bestand voor je computer<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477823846' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw031477823846' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw031477823846'] ) &&  $_POST['sw031477823846'] == "1" && isset( $_POST['btnsw031477823846'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['sw031477823846'] ) && $_POST['sw031477823846'] !== "1" && isset( $_POST['btnsw031477823846'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477823846'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
