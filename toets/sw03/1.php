<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw03<hr>
Vraag 1<h4> Wat wordt bedoeld met het 'installeren van software'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw031477823432' value='1'>Door software te installeren verwijder je ongewenste software van je systeem<label></div>
<div class='radio'><label><input type='radio' name='sw031477823432' value='2'>Installeren betekent: de verkenner openen<label></div>
<div class='radio'><label><input type='radio' name='sw031477823432' value='3'>Het installeren van software gebeurt meestal op de achtergrond, je hoeft niks zelf te doen<label></div>
<div class='radio'><label><input type='radio' name='sw031477823432' value='4'>Software installeren betekent dat je een of meer apps of programma's aan je computer toevoegt<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477823432' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw031477823432' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw031477823432'] ) &&  $_POST['sw031477823432'] == "4" && isset( $_POST['btnsw031477823432'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['sw031477823432'] ) && $_POST['sw031477823432'] !== "4" && isset( $_POST['btnsw031477823432'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477823432'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
