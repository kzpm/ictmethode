<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at01<hr>
Vraag 9<h4> Is het mogelijk om niet gevolgd te kunnen worden op internet?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at011476971072' value='1'>Ja het kan wel, maar je moet dan heel veel zelf uitzoeken<label></div>
<div class='radio'><label><input type='radio' name='at011476971072' value='2'>Het is ontzettend moeilijk, je ontkomt er bijna niet aan.<label></div>
<div class='radio'><label><input type='radio' name='at011476971072' value='3'>Beide antwoorden zijn juist<label></div>
<div class='radio'><label><input type='radio' name='at011476971072' value='4'>Alle antwoorden zijn onjuist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1476971072' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat011476971072' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at011476971072'] ) &&  $_POST['at011476971072'] == "3" && isset( $_POST['btnat011476971072'] ) 
){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['at011476971072'] ) && $_POST['at011476971072'] !== "3" && isset( $_POST['btnat011476971072'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1476971072'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
