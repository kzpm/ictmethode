<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at01<hr>
Vraag 7<h4> Wat is een 'dataset'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at011476970508' value='1'>Een verzameling van gegevens over een 
bepaald onderwerp<label></div>
<div class='radio'><label><input type='radio' name='at011476970508' value='2'>Een apparaat waarmee je gegevens of data kunt controleren<label></div>
<div class='radio'><label><input type='radio' name='at011476970508' value='3'>Een app voor je tablet of smartphone<label></div>
<div class='radio'><label><input type='radio' name='at011476970508' value='4'>Alle antwoorden zijn fout<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1476970508' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat011476970508' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at011476970508'] ) &&  $_POST['at011476970508'] == "1" && isset( $_POST['btnat011476970508'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['at011476970508'] ) && $_POST['at011476970508'] !== "1" && isset( $_POST['btnat011476970508'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1476970508'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
