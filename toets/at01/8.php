<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at01";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at01<hr>
Vraag 8<h4> Wat voor gegevens kunnen er in een dataset staan?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at011476970826' value='1'>In een dataset kunnen geen gegevens staan<label></div>
<div class='radio'><label><input type='radio' name='at011476970826' value='2'>Gegevens over je zoekacties op internet<label></div>
<div class='radio'><label><input type='radio' name='at011476970826' value='3'>Gegevens over o.a. je zoekacties, hobbies, sportclub, je vrienden en je studieresultaten<label></div>
<div class='radio'><label><input type='radio' name='at011476970826' value='4'>Een dataset bestaat een uit een heleboel verzamelingen van gegevens<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1476970826' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat011476970826' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at011476970826'] ) &&  $_POST['at011476970826'] == "3" && isset( $_POST['btnat011476970826'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['at011476970826'] ) && $_POST['at011476970826'] !== "3" && isset( $_POST['btnat011476970826'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1476970826'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
