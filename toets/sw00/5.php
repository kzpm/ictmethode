<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw00";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw00<hr>
Vraag 5<h4> Wanneer je broncode schrijft gebruik je een programmeertaal. In welke spreektaal zijn deze programmeertalen gebouwd?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw001477484059' value='1'>Nederlands<label></div>
<div class='radio'><label><input type='radio' name='sw001477484059' value='2'>Engels<label></div>
<div class='radio'><label><input type='radio' name='sw001477484059' value='3'>Duits<label></div>
<div class='radio'><label><input type='radio' name='sw001477484059' value='4'>Spaans<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477484059' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw001477484059' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw001477484059'] ) &&  $_POST['sw001477484059'] == "2" && isset( $_POST['btnsw001477484059'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['sw001477484059'] ) && $_POST['sw001477484059'] !== "2" && isset( $_POST['btnsw001477484059'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477484059'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
