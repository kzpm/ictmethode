<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw00";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw00<hr>
Vraag 1<h4> Wat wordt bedoeld met 'broncode'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw001477483406' value='1'>Een code die je nodig hebt om je ergens aan te melden<label></div>
<div class='radio'><label><input type='radio' name='sw001477483406' value='2'>Een soort sleutel<label></div>
<div class='radio'><label><input type='radio' name='sw001477483406' value='3'>Dat zijn de bestanden waarmee een computerprogramma gebouwd wordt<label></div>
<div class='radio'><label><input type='radio' name='sw001477483406' value='4'>Dat zijn de codes, die je nodig hebt om een bestand te lezen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477483406' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw001477483406' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw001477483406'] ) &&  $_POST['sw001477483406'] == "3" && isset( $_POST['btnsw001477483406'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['sw001477483406'] ) && $_POST['sw001477483406'] !== "3" && isset( $_POST['btnsw001477483406'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477483406'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
