<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="sw00";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>sw00<hr>
Vraag 9<h4> Is Microsoft Word een 'platform-onafhankelijk' programma?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='sw001477484618' value='1'>Nee, want je kunt het alleen op computers met een Windows of Apple besturingssysteem gebruiken<label></div>
<div class='radio'><label><input type='radio' name='sw001477484618' value='2'>Ja, je kunt Word op Windows, Apple en Linux gebruiken<label></div>
<div class='radio'><label><input type='radio' name='sw001477484618' value='3'>Ja. Je moet voor Microsoft Word betalen, dus is het platform onafhankelijk<label></div>
<div class='radio'><label><input type='radio' name='sw001477484618' value='4'>Nee, want je kunt de broncode niet controleren<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477484618' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnsw001477484618' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['sw001477484618'] ) &&  $_POST['sw001477484618'] == "1" && isset( $_POST['btnsw001477484618'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['sw001477484618'] ) && $_POST['sw001477484618'] !== "1" && isset( $_POST['btnsw001477484618'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477484618'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
