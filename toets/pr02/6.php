<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 6<h4> Hoe kun je er voor zorgen dat je programma alleen maar werkt wanneer de waarde van de variablele 's' 4 is?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021478765020' value='1'>if s == 4<label></div>
<div class='radio'><label><input type='radio' name='pr021478765020' value='2'>if s < 4<label></div>
<div class='radio'><label><input type='radio' name='pr021478765020' value='3'>if s > 4<label></div>
<div class='radio'><label><input type='radio' name='pr021478765020' value='4'>if s == vier<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478765020' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021478765020' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021478765020'] ) &&  $_POST['pr021478765020'] == "1" && isset( $_POST['btnpr021478765020'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['pr021478765020'] ) && $_POST['pr021478765020'] !== "1" && isset( $_POST['btnpr021478765020'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478765020'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
