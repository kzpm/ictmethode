<?php
session_start();


$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";
$certdir="../../certs/";
require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if
require_once $incdir.'header.inc';

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsEvaluate.class.php';
$myEval = new clsEvaluate();
$eval=$myEval->fCreateEvaluation( $st_code, $cursuscode );

echo '<div class="container">'.$cursuscode.'<br>

Resultaten: <div class="jumbotron">
<table class ="table-striped" width ="55%"><head><tr><td><u>Vraag</u> </td><td><u>Aantal pogingen</u></td><td><u>Score (%)</u></td></tr>';
for ( $i=0;$i < count( $eval ); $i++ ){

        		echo strtolower( $eval[$i] );

}//end for

echo "</table></div></div>";

echo "<div class='container'><hr>
<form name='eval' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<input type='submit' class='btn btn-warning black-text black-text' name='btnCancel' value='Ga terug'>
</form>
<hr>";

if ( isset( $_POST['btnCancel'] ) ){

                        echo '<script>location.replace("https://ictmethode.nl/studyoverview.php" );</script>';
}else{


	echo '</div>';

	require_once $srcdir.'clsTestCert.class.php';
	$myTestCert = new clsTestCert();
	if ( $myTestCert->fTestCert($st_code, $cursuscode ) ){

		echo '<div class="alert alert-success">Bekijk je <a href="'.$certdir.$cursuscode.'-'.$st_code.'_cert.pdf" target="_blank">certificaat</a></div>';

		require_once $srcdir.'clsCreateCert.class.php';
		$myCreateCert = new clsCreateCert();

		//ob_start();			
		if ( $myCreateCert->fCreateCert( $_SESSION['leerjaar'], $st_code, $cursuscode ) ){


		}//end if
		//ob_end_flush();

	}else{

		echo '<div class="alert alert-warning">Je hebt helaas nog geen certificaat behaald voor deze module. Maak de toets <a href="1.php">nog een keer</a>...</div>';

	}//end if
}//end if



