<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 10<h4> Wat betekent `if x <> 0`</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021478765575' value='1'>Als de waarde van x ongelijk is aan 0<label></div>
<div class='radio'><label><input type='radio' name='pr021478765575' value='2'>Als de waarde van x groter is dan 0<label></div>
<div class='radio'><label><input type='radio' name='pr021478765575' value='3'>Als de waarde van x kleiner is dan 0<label></div>
<div class='radio'><label><input type='radio' name='pr021478765575' value='4'>Als x gelijk is aan 0<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478765575' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021478765575' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021478765575'] ) &&  $_POST['pr021478765575'] == "1" && isset( $_POST['btnpr021478765575'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,1 );
echo '<script>location.replace("11.php");</script>';

}//end if

if (  isset( $_POST['pr021478765575'] ) && $_POST['pr021478765575'] !== "1" && isset( $_POST['btnpr021478765575'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,10,0 );
echo '<script>location.replace("11.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478765575'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
