<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 7<h4> if p > 7 Wat gebeurt hier?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021478765226' value='1'>Er staat 'Als de variabele p gelijk aan 7 is'<label></div>
<div class='radio'><label><input type='radio' name='pr021478765226' value='2'>Er staat: 'Als de waarde van de variabele p groter is dan 
7'<label></div>
<div class='radio'><label><input type='radio' name='pr021478765226' value='3'>Er staat: 'Als de waarde van de variabele p groter of gelijk is aan 
7'<label></div>
<div class='radio'><label><input type='radio' name='pr021478765226' value='4'>Er staat: 'Als de waarde van p minder is dan 7'<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478765226' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021478765226' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021478765226'] ) &&  $_POST['pr021478765226'] == "2" && isset( $_POST['btnpr021478765226'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['pr021478765226'] ) && $_POST['pr021478765226'] !== "2" && isset( $_POST['btnpr021478765226'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478765226'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
