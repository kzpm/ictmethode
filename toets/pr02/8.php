<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 8<h4> Wat betekent: `if p > x`?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021478765415' value='1'>Als de waarde van de variabele p groter is dan 0<label></div>
<div class='radio'><label><input type='radio' name='pr021478765415' value='2'>Als de waarde van de variabele x groter is dan de waarde van p<label></div>
<div class='radio'><label><input type='radio' name='pr021478765415' value='3'>Als 3 groter is dan 0<label></div>
<div class='radio'><label><input type='radio' name='pr021478765415' value='4'>Als de waarde van de variabele p groter is dan de waarde van x<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478765415' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021478765415' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021478765415'] ) &&  $_POST['pr021478765415'] == "4" && isset( $_POST['btnpr021478765415'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['pr021478765415'] ) && $_POST['pr021478765415'] !== "4" && isset( $_POST['btnpr021478765415'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478765415'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
