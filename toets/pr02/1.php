<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="pr02";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>pr02<hr>
Vraag 1<h4> Waar gebruik je `als ... dan` voor?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='pr021478763469' value='1'>Om te kijken of iets waar is<label></div>
<div class='radio'><label><input type='radio' name='pr021478763469' value='2'>Om te testen welke waarde een variabele heeft<label></div>
<div class='radio'><label><input type='radio' name='pr021478763469' value='3'>Om iets goed of fout te rekenen<label></div>
<div class='radio'><label><input type='radio' name='pr021478763469' value='4'>Met `als .. dan` test je de waarde '0'<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1478763469' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnpr021478763469' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['pr021478763469'] ) &&  $_POST['pr021478763469'] == "2" && isset( $_POST['btnpr021478763469'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['pr021478763469'] ) && $_POST['pr021478763469'] !== "2" && isset( $_POST['btnpr021478763469'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1478763469'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
