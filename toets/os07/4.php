<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os07<hr>
Vraag 4<h4> Wat is een 'trojan'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os071476785643' value='1'>Een programma wat zonder je toestemming wordt geinstalleerd en toegang geeft tot je computer<label></div>
<div class='radio'><label><input type='radio' name='os071476785643' value='2'>Een Grieks paard wat gebruikt werd om computers te vernielen<label></div>
<div class='radio'><label><input type='radio' name='os071476785643' value='3'>Alle programma's, die niet door jou worden gebouwd<label></div>
<div class='radio'><label><input type='radio' name='os071476785643' value='4'>Een trojan is een programma wat probeert om je bankrekening leeg te halen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1476785643' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos071476785643' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os071476785643'] ) &&  $_POST['os071476785643'] == "1" && isset( $_POST['btnos071476785643'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['os071476785643'] ) && $_POST['os071476785643'] !== "1" && isset( $_POST['btnos071476785643'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1476785643'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
