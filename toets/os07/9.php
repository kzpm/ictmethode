<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os07<hr>
Vraag 9<h4> Kun je het merken wanneer je computer gehackt is?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os071476786663' value='1'>Ja je merkt direct dat je computer veel trager is<label></div>
<div class='radio'><label><input type='radio' name='os071476786663' value='2'>Nee, je weet nooit of je gehackt bent<label></div>
<div class='radio'><label><input type='radio' name='os071476786663' value='3'>Soms kun je het wel merken, maar vaak niet<label></div>
<div class='radio'><label><input type='radio' name='os071476786663' value='4'>Computers kunnen niet gehackt worden als er een goed virusprogramma draait<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1476786663' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos071476786663' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os071476786663'] ) &&  $_POST['os071476786663'] == "3" && isset( $_POST['btnos071476786663'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['os071476786663'] ) && $_POST['os071476786663'] !== "3" && isset( $_POST['btnos071476786663'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1476786663'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
