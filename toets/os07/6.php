<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os07";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os07<hr>
Vraag 6<h4> Welke groep hackers kunnen alle middelen inzetten om toegang tot computernetwerken te krijgen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os071476786086' value='1'>Amateur hackers<label></div>
<div class='radio'><label><input type='radio' name='os071476786086' value='2'>Overheden<label></div>
<div class='radio'><label><input type='radio' name='os071476786086' value='3'>Professionele hackers<label></div>
<div class='radio'><label><input type='radio' name='os071476786086' value='4'>Alle antwoorden zijn goed<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1476786086' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos071476786086' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os071476786086'] ) &&  $_POST['os071476786086'] == "2" && isset( $_POST['btnos071476786086'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,1 );
echo '<script>location.replace("7.php");</script>';

}//end if

if (  isset( $_POST['os071476786086'] ) && $_POST['os071476786086'] !== "2" && isset( $_POST['btnos071476786086'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,6,0 );
echo '<script>location.replace("7.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1476786086'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
