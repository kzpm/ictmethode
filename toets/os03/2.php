<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];         
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace("../../logout.php" )</script>';

}//end if
$st_code =$_SESSION['studentnr'];
$cursuscode ="os03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os03<hr>
Vraag 2<h4> Wat betekent `zippen` of `comprimeren`?</h4>Gebruik een <a href ='https://nl.wiktionary.org/wiki/comprimeren' target = '_blank'>zoekmachine!</a><br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST' autocomplete=off>
<div class='radio'><label><input type='radio' name='os031453711652' value='1'>Bestanden uitpakken<label></div>
<div class='radio'><label><input type='radio' name='os031453711652' value='2'>Kleine bestanden groter maken<label></div>
<div class='radio'><label><input type='radio' name='os031453711652' value='3'>Grote bestanden kleiner maken, door het samen te persen<label></div>
<div class='radio'><label><input type='radio' name='os031453711652' value='4'>Proberen om verwijderde bestanden weer terug te halen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1453711652' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos031453711652' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os031453711652'] ) && $_POST['os031453711652'] == "3" && isset( $_POST['btnos031453711652'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
$nextpage=4;
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['os031453711652'] ) && $_POST['os031453711652'] !== "3" && isset( $_POST['btnos031453711652'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
$nextpage=4;
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1453711652'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if
?>
