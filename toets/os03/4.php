<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os03<hr>
Vraag 4<h4> Wat is een goede zoekstrategie, wanneer je een bestand kwjt bent?</h4>Kies het beste antwoord!<br>Je mag 
een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os031462226672' value='1'>Computer uitzetten<label></div>
<div class='radio'><label><input type='radio' name='os031462226672' value='2'>Zoeken op bestandsnaam, als je die nog weet<label></div>
<div class='radio'><label><input type='radio' name='os031462226672' value='3'>De tijd van je computer terugzetten<label></div>
<div class='radio'><label><input type='radio' name='os031462226672' value='4'>Bestanden, die verwijderd zijn, komen vanzelf weer terug<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462226672' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos031462226672' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os031462226672'] ) &&  $_POST['os031462226672'] == "2" && isset( $_POST['btnos031462226672'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,1 );
echo '<script>location.replace("5.php");</script>';

}//end if

if (  isset( $_POST['os031462226672'] ) && $_POST['os031462226672'] !== "2" && isset( $_POST['btnos031462226672'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,4,0 );
echo '<script>location.replace("5.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462226672'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
