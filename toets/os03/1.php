<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"].'/';         
$incdir=$rdir."inc/";
$srcdir=$rdir."src/";

require_once $rdir.'src/clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace("../../logout.php" )</script>';

}//end if
$st_code =$_SESSION['studentnr'];
$cursuscode ="os03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os03<hr>
Vraag 1<h4> Op welke kolom klik je om bestanden op datum te sorteren?</h4><br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST' autocomplete=off>
<div class='radio'><label><input type='radio' name='os031453711151' value='1'>Op de kolom 'Gewijzigd'<label></div>
<div class='radio'><label><input type='radio' name='os031453711151' value='2'>Op de kolom 'Type'<label></div>
<div class='radio'><label><input type='radio' name='os031453711151' value='3'>Op de kolom 'Grootte'<label></div>
<div class='radio'><label><input type='radio' name='os031453711151' value='4'>Op de kolom 'Naam'<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1453711151' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos031453711151' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os031453711151'] ) && $_POST['os031453711151'] == "1" && isset( $_POST['btnos031453711151'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,1 );
$nextpage=3;
echo '<script>location.replace("2.php");</script>';

}//end if

if (  isset( $_POST['os031453711151'] ) && $_POST['os031453711151'] !== "1" && isset( $_POST['btnos031453711151'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,1,0 );
$nextpage=3;
echo '<script>location.replace("2.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1453711151'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if
?>
