<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="os03";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>os03<hr>
Vraag 7<h4> Wat is een 'bestandsextensie'?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='os031462227351' value='1'>Een deel van je computer<label></div>
<div class='radio'><label><input type='radio' name='os031462227351' value='2'>Het is een tekstverwerker voor je tablet<label></div>
<div class='radio'><label><input type='radio' name='os031462227351' value='3'>Het is een soort zoekactie<label></div>
<div class='radio'><label><input type='radio' name='os031462227351' value='4'>Het is het laatste gedeelte van een bestandsnaam achter de punt. (Vb. mijnverslag.docx)<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1462227351' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnos031462227351' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['os031462227351'] ) &&  $_POST['os031462227351'] == "4" && isset( $_POST['btnos031462227351'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if (  isset( $_POST['os031462227351'] ) && $_POST['os031462227351'] !== "4" && isset( $_POST['btnos031462227351'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1462227351'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
