<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at06<hr>
Vraag 3<h4> Waarom zijn sociale media zo populair?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at061477212360' value='1'>Je kunt zelf bepalen wie je toevoegt<label></div>
<div class='radio'><label><input type='radio' name='at061477212360' value='2'>Je kunt zelf bepalen welke berichten je stuurt<label></div>
<div class='radio'><label><input type='radio' name='at061477212360' value='3'>Je privacy is goed beschermd bij social-media diensten<label></div>
<div class='radio'><label><input type='radio' name='at061477212360' value='4'>Mensen vinden het prettig wanneer ze veel vrienden hebben en positieve reacties krijgen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477212360' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat061477212360' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at061477212360'] ) &&  $_POST['at061477212360'] == "4" && isset( $_POST['btnat061477212360'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if (  isset( $_POST['at061477212360'] ) && $_POST['at061477212360'] !== "4" && isset( $_POST['btnat061477212360'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477212360'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
