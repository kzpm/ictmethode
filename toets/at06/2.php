<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at06";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at06<hr>
Vraag 2<h4> Waarom kunnen selfies 'bijwerkingen' hebben?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at061477212041' value='1'>Sommige mensen willen niet zomaar gefotografeerd worden<label></div>
<div class='radio'><label><input type='radio' name='at061477212041' value='2'>Zo'n selfie wordt altijd gedeeld<label></div>
<div class='radio'><label><input type='radio' name='at061477212041' value='3'>Je kunt selfies 'photoshoppen'<label></div>
<div class='radio'><label><input type='radio' name='at061477212041' value='4'>Er wordt bedoeld dat je altijd leuke reacties krijgt op selfies<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477212041' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat061477212041' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at061477212041'] ) &&  $_POST['at061477212041'] == "1" && isset( $_POST['btnat061477212041'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['at061477212041'] ) && $_POST['at061477212041'] !== "1" && isset( $_POST['btnat061477212041'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477212041'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
