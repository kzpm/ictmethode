<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at05<hr>
Vraag 9<h4> Kun je reclame altijd herkennen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at051477152352' value='1'>Ja dat is altijd goed te zien<label></div>
<div class='radio'><label><input type='radio' name='at051477152352' value='2'>Ja, maar dan moet je wel je 'ad-blocker' uitzetten<label></div>
<div class='radio'><label><input type='radio' name='at051477152352' value='3'>Nee het is erg moeilijk om te zien wat echt waar is en wat een advertentie is<label></div>
<div class='radio'><label><input type='radio' name='at051477152352' value='4'>Wanneer je een gesponsord artikel ziet, is het soms moeilijk om vast te stellen of het al dan niet reclame is<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477152352' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat051477152352' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at051477152352'] ) &&  $_POST['at051477152352'] == "4" && isset( $_POST['btnat051477152352'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['at051477152352'] ) && $_POST['at051477152352'] !== "4" && isset( $_POST['btnat051477152352'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477152352'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
