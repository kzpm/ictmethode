<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at05";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at05<hr>
Vraag 2<h4> Wat wordt er bedoeld met een 'product' in een advertentie?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at051477148782' value='1'>Een product is iets wat je vast zou kunnen houden en iets wat je kunt gebruiken.<label></div>
<div class='radio'><label><input type='radio' name='at051477148782' value='2'>Een product is het zelfde als een 'dienst'<label></div>
<div class='radio'><label><input type='radio' name='at051477148782' value='3'>Wanneer het in een advertentie om een product gaat, bedoelt men meestal: 'de uitkomst van een keersom'<label></div>
<div class='radio'><label><input type='radio' name='at051477148782' value='4'>Geen van de antwoorden is juist<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477148782' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat051477148782' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at051477148782'] ) &&  $_POST['at051477148782'] == "1" && isset( $_POST['btnat051477148782'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,1 );
echo '<script>location.replace("3.php");</script>';

}//end if

if (  isset( $_POST['at051477148782'] ) && $_POST['at051477148782'] !== "1" && isset( $_POST['btnat051477148782'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,2,0 );
echo '<script>location.replace("3.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477148782'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
