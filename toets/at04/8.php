<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at04<hr>
Vraag 8<h4> Welke licentie staat je toe om foto's vrij te gebruiken?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at041477391935' value='1'>Gelabeld voor hergebruik<label></div>
<div class='radio'><label><input type='radio' name='at041477391935' value='2'>Gelabeld voor hergebruik inclusief aanpassing<label></div>
<div class='radio'><label><input type='radio' name='at041477391935' value='3'>Gelabeld voor niet-commercieel hergebruik<label></div>
<div class='radio'><label><input type='radio' name='at041477391935' value='4'>Alle antwoorden kloppen, maar je mag de afbeelding bij antwoord 3 niet bijvoorbeeld voor een webshop gebruiken<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477391935' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat041477391935' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at041477391935'] ) &&  $_POST['at041477391935'] == "4" && isset( $_POST['btnat041477391935'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,1 );
echo '<script>location.replace("9.php");</script>';

}//end if

if (  isset( $_POST['at041477391935'] ) && $_POST['at041477391935'] !== "4" && isset( $_POST['btnat041477391935'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,8,0 );
echo '<script>location.replace("9.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477391935'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
