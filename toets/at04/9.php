<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at04<hr>
Vraag 9<h4> Waarom is het belangrijk om ervoor te zorgen dat je eigen wachtwoord niet wordt opgeslagen als je van een openbare computer gebruik maakt?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at041477392110' value='1'>Wanneer je je wachtwoord opslaat, kunnen andere gebruikers ook in jouw account komen<label></div>
<div class='radio'><label><input type='radio' name='at041477392110' value='2'>Wanneer je je wachtwoord opslaat, kan niemand anders meer gebruik maken van de computer<label></div>
<div class='radio'><label><input type='radio' name='at041477392110' value='3'>Wanneer je je wachtwoord opslaat, kan iedereen je wachtwoord lezen<label></div>
<div class='radio'><label><input type='radio' name='at041477392110' value='4'>Alle antwoorden zijn goed<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477392110' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat041477392110' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at041477392110'] ) &&  $_POST['at041477392110'] == "1" && isset( $_POST['btnat041477392110'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,1 );
echo '<script>location.replace("10.php");</script>';

}//end if

if (  isset( $_POST['at041477392110'] ) && $_POST['at041477392110'] !== "1" && isset( $_POST['btnat041477392110'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,9,0 );
echo '<script>location.replace("10.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477392110'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
