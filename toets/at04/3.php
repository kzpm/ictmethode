<?php
session_start();
$rdir=$_SERVER["DOCUMENT_ROOT"];         
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( $rdir."logout.php" )</script>';

                }//end if


require_once $incdir.'header.inc';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="at04";

echo "<div class='container'>at04<hr>
Vraag 3<h4> Hoe kun je voorkomen dat je computer door anderen wordt gebruikt als je net even weg bent?</h4>(Er zijn meerdere antwoorden mogelijk!)<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qcheck' action ='' method= 'POST'>
<div class='checkbox'><label><input type='checkbox' name='at0414771601821' value='2'>De computer vergrendelen<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0414771601822' value='3'>De monitor uitzetten<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0414771601823' value='4'>De smartphone of tablet met een  'patroon' beveiligen<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0414771601824' value='5'>Een briefje ophangen met de tekst 'Niet aankomen!'<label></div>
<!--<button type='submit' name='btn21477160182' value='1'>Ok</button>-->
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477160182' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn21477160182' value='Ok'>
</form>
<hr></div>";
$array1=array();
  foreach( $_POST as $name => $value) {
        		if ( $value !='Ok' )
        			$array1[]= $value;
   	 	 }//end foreach

$array2=explode( ",",'2,4');
//$res1= array_product( $array1 );
//$res2= array_product( $array2 );
//echo "Array1 ".array_product( $array1 ).'<br>';
//echo "Array2 ".array_product( $array2 ).'<br>';
//echo "Product totaal ".$res1*$res2;
if ( array_product( $array1 )== array_product( $array2) && isset( $_POST["btn21477160182"] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,1 );
echo '<script>location.replace("4.php");</script>';

}//end if

if ( array_product( $array1 )!== array_product( $array2) && isset( $_POST["btn21477160182"] ) ){
        		
$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,3,0 );
echo '<script>location.replace("4.php");</script>';


}//end if
if ( isset( $_POST['btnCancel1477160182'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

