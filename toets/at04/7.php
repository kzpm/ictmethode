<?php
session_start();
$rdir=$_SERVER["DOCUMENT_ROOT"];         
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

                require_once $srcdir.'clsChkSession.class.php';
                $mySession = new clsChkSession();
                if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

                        echo '<script>location.replace( $rdir."logout.php" )</script>';

                }//end if


require_once $incdir.'header.inc';
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
require_once $srcdir.'clsWriteProgress.class.php';
$st_code =$_SESSION['studentnr'];
$cursuscode ="at04";

echo "<div class='container'>at04<hr>
Vraag 7<h4> Waarom is het mogelijk om bestanden te 'verbergen'?</h4>(Er zijn meerdere antwoorden mogelijk!)<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qcheck' action ='' method= 'POST'>
<div class='checkbox'><label><input type='checkbox' name='at0414773894731' value='2'>Sommige bestanden zijn erg belangrijk voor de werking van een programma. Door ze te verbergen, kunnen ze niet zo gauw per ongeluk worden verwijderd<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0414773894732' value='3'>Zo lijkt het net of je veel meer ruimte op je computer hebt<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0414773894733' value='4'>Het is een soort spelletje, net als verstoppertje spelen. Wanneer je alle verborgen bestanden hebt gevonden krijg je een punt<label></div>
<div class='checkbox'><label><input type='checkbox' name='at0414773894734' value='5'>Door 'onbelangrijke' bestanden voor je te verbergen, heb je veel meer overzicht<label></div>
<!--<button type='submit' name='btn21477389473' value='1'>Ok</button>-->
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477389473' value='Ga terug'>
                <input type='submit' class='btn btn-success' name='btn21477389473' value='Ok'>
</form>
<hr></div>";
$array1=array();
  foreach( $_POST as $name => $value) {
        		if ( $value !='Ok' )
        			$array1[]= $value;
   	 	 }//end foreach

$array2=explode( ",",'2,5');
//$res1= array_product( $array1 );
//$res2= array_product( $array2 );
//echo "Array1 ".array_product( $array1 ).'<br>';
//echo "Array2 ".array_product( $array2 ).'<br>';
//echo "Product totaal ".$res1*$res2;
if ( array_product( $array1 )== array_product( $array2) && isset( $_POST["btn21477389473"] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,1 );
echo '<script>location.replace("8.php");</script>';

}//end if

if ( array_product( $array1 )!== array_product( $array2) && isset( $_POST["btn21477389473"] ) ){
        		
$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,7,0 );
echo '<script>location.replace("8.php");</script>';


}//end if
if ( isset( $_POST['btnCancel1477389473'] ) ){

echo '<script>location.replace("../../studyoverview.php" );</script>';

}//end if

?>

