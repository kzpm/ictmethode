<?php
session_start();

$rdir=$_SERVER["DOCUMENT_ROOT"];
$srcdir=$rdir."/src/";
$incdir=$rdir."/inc/";

require_once $srcdir.'clsChkSession.class.php';
$mySession = new clsChkSession();
if ( !$mySession->fChkSession( $_SESSION['leerjaar'] ) ){

        echo '<script>location.replace(."/logout.php" )</script>';

}//end if

$st_code =$_SESSION['studentnr'];
$cursuscode ="at04";
require_once $incdir.'error_reporting.inc';
require_once $srcdir.'clsQuestionProgress.class.php';
$myQuestionProgress = new clsQuestionProgress();
$myQuestionProgress->fCreateQuestionProgress( $st_code, $cursuscode );
require_once $incdir.'header.inc';
require_once $srcdir.'clsWriteProgress.class.php';

echo "<div class='container'>at04<hr>
Vraag 5<h4> Wanneer heeft het zin om je printopdrachten te beveiligen?</h4>Kies het beste antwoord!<br>Je mag een zoekmachine gebruiken!<br><br>
<form name='qradio' action =".$_SERVER['PHP_SELF']." method= 'POST'>
<div class='radio'><label><input type='radio' name='at041477388698' value='1'>Wanneer de afdruk in kleur is<label></div>
<div class='radio'><label><input type='radio' name='at041477388698' value='2'>Wanneer de printer in een andere ruimte staat<label></div>
<div class='radio'><label><input type='radio' name='at041477388698' value='3'>Wanneer je afdruk in zwart-wit is<label></div>
<div class='radio'><label><input type='radio' name='at041477388698' value='4'>Alle antwoorden kloppen<label></div>
<input type='submit' class='btn btn-warning black-text' name='btnCancel1477388698' value='Ga terug'>
<input type='submit' class='btn btn-success' name='btnat041477388698' value='Ok'>
</form>
 <hr></div>";


if (  isset( $_POST['at041477388698'] ) &&  $_POST['at041477388698'] == "2" && isset( $_POST['btnat041477388698'] ) ){

$myProgress = new clsWriteProgress();
$tablename = 'q'.$st_code.$cursuscode;
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,1 );
echo '<script>location.replace("6.php");</script>';

}//end if

if (  isset( $_POST['at041477388698'] ) && $_POST['at041477388698'] !== "2" && isset( $_POST['btnat041477388698'] ) ){

$tablename = 'q'.$st_code.$cursuscode;
$myProgress = new clsWriteProgress();
$myProgress->fSetProgress( $tablename, $_SESSION["leerjaar"],$st_code,$cursuscode,5,0 );
echo '<script>location.replace("6.php");</script>';


}//end if

if ( isset( $_POST['btnCancel1477388698'] ) ){

echo '<script>location.replace( "../../studyoverview.php" );</script>';

}//end if
?>
