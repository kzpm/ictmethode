= Help computer is vastgelopen
:toc: right

----
Module: os05
----


== Voorkennis

- Begrippen "Vastlopen" "Bevriezen" met betrekking tot gebruik computers

== Wat ga je leren?

- Wat een 'vastgelopen' of 'bevroren' systeem betekent
- Onderzoeken wat er aan de hand is
- Oplossingsstrategie
- Data veilig stellen
- Gebruik maken van 'tussentijds opslaan'
- Systeemonderhoud

=== Gebeurt het vaker?

Wanneer een computer, telefoon, laptop of tablet vastloopt, reageert het besturingssysteem niet meer op je opdrachten. 
Je hebt het vast al wel eens meegemaakt...
Dat een computer vastloopt is heel lastig. 
Je kunt niet doorwerken en je kunt het werk waarmee je bezig bent verliezen!

Het liefst wil je dat natuurlijk voorkomen.
In deze les leer je hoe je dat kunt doen en hoe je weer verder kunt werken.

=== Hoe weet je dat een computer is vastgelopen?

- Je computer reageert nauwelijks of helemaal niet meer op opdrachten. De muiswijzer blijft staan of verdwijnt helemaal. Je kunt niks meer intypen.
- Wanneer je de 'NumLock' toets indrukt zie je dat het ledje.footnoteref:[disclaimer, Een klein lampje, http://1.bp.blogspot.com/-7aoHF2inzcg/Tyb8mEb3JQI/AAAAAAAAAdo/JqjCMYPFTYo/s1600/HiperKeyboard_LEDsLit.jpg] niet meer reageert. Het blijft 'aan' of 'uit' staan.

.Programma reageert niet meer.footnoteref:[disclaimer,Blogspot,http://plzcdn.com/ZillaIMG/0aa2327e03a3307fc1cd79d0c2ae1056.jpg]
image::http://1.bp.blogspot.com/-7aoHF2inzcg/Tyb8mEb3JQI/AAAAAAAAAdo/JqjCMYPFTYo/s1600/HiperKeyboard_LEDsLit.jpg[Leds, 200,200]
 

=== Gebeurt het met een bepaald programma?

Het kan zijn dat een programma op je computer het probleem veroorzaakt. Tijdens gebruik, nemen sommige programma's heel veel geheugenruimte in beslag.
Hoe meer geheugen er in gebruik is, hoe minder er openstaat voor andere programma's.
Typische ruimtevreters in Windows zijn de Internet Explorer, de Verkenner en de Office programma's. 
De grootste geheugenvreters in een tablet of smartphone zijn in de regel fotobewerkingsprogramma, camera gebruik en het gebruik van een internetbrowser voor het kijken 
van films, bijv. YouTube.

Wanneer je weet met welk programma het gebeurt, kun je ervoor zorgen, dat alle andere programma's zoveel mogelijk afgesloten worden. 

image::http://plzcdn.com/ZillaIMG/0aa2327e03a3307fc1cd79d0c2ae1056.jpg[300,300]

=== Staan er veel programma's open?

Veel kleine programma's maken toch weer een grote. Wanneer je pc traag is en dreigt vast te lopen, sluit dan zoveel mogelijk proramma's af. Sla je werk natuurlijk op!

=== Taakbeheer

Via het tabblad 'Processen' in het programma 'Taakbeheer' kun je in Windows zien welke taken op een gegeven moment actief zijn. 
Probeer via de zoekmachine uit te zoeken of deze taken van belang zijn. Wanneer ze niets lijken toe te voegen, kun je de taak beeindigen via de knop 'Proces beeindigen'. 

NOTE:Sla je werk wel eerst op.

.Taakbeheer.footnoteref:[disclaimer, Taakbeheer, https://www.windowscorner.nl/windowsxp/wp-content/uploads/2002/03/taakbeheer.jpg]
image::https://www.windowscorner.nl/windowsxp/wp-content/uploads/2002/03/taakbeheer.jpg[Taakbeheer, 300, 300]

=== Afmelden

Wanneer je computer erg traag wordt en dreigt vast te lopen, kun je ook proberen om via 'Afmelden' en daarna weer aanmelden, programma's, die op de 'achtergrond' 
blijven lopen, af te sluiten. Meestal krijg je tijdens deze actie ook wel de vraag of je gewijzigde inhoud wil bewaren.

=== Overbodige software en systeemservices verwijderen

Een andere oorzaak van vastlopende systemen is het gedrag van bepaalde software of services. 

Haal programma's, die je niet nodig hebt van je pc! 
Denk maar zo: elk programma op je computer kan je apparaat weer een stukje trager en onveiliger maken. Wanneer het programma niet op je pc staat, kan het ook niet als 
'backdoor' (achterdeurtje) gebruikt worden en neemt het ook geen ruimte in beslag!

In Windows kun je via het `Controlepaneel` software de-installeren. 
Om software te de-installeren, kun je deze methode in Windows het beste gebruiken, omdat alle software, die te maken heeft met het programma waarschijnlijk ook wordt 
verwijderd. 
Waarschijnlijk, omdat het moeilijk te controleren is.

.Programma's verwijderen.footnoteref:[disclaimer, Programma's verwijderen,http://www.freeuninstallguide.com/images/Windows_7_add-remove-programs_list.gif]
image::http://www.freeuninstallguide.com/images/Windows_7_add-remove-programs_list.gif[Programma's verwijderen, 300, 300]

=== Defragmenteren en optimaliseren

Een 'fragment' is een deeltje van een groter geheel. 
Stel je voor dat een harddisk een soort ladenkast is. En stel je voor dat je nog een verzameling boeken hebt. Deze boeken kun je allemaal nog in de kast kwijt, maar niet meer in dezelde lade. Je moet dus meer lades gebruiken, om je boeken allemaal op te bergen.

Zo gaat het ook met een nieuw programma. Je installeert een programma, maar de harddisk heeft niet genoeg plaats om alle onderdelen van het programma netjes bij elkaar te zetten. Het programma wordt in deeltjes (fragmenten) over de schijf verdeelt.

Wanneer je het programma wil gaan gebruiken, moet de hardeschijf alle deeltjes van dat programma bij elkaar zoeken. Dat kost tijd. Daardoor wordt je computer langzamer. 
Zo nu en dan defragmenteren of optimaliseren, kan dus helpen voorkomen, dat je computer vastloopt.

.Defragmenteren.footnoteref:[disclaimer, defragmenteren = de deeltjes bij elkaar zoeken, https://www.schoonepc.nl/i/win8/defragmenteren.png]
image::https://www.schoonepc.nl/i/win8/defragmenteren.png[Defragmenteren of optimaliseren,https://www.schoonepc.nl/i/win8/defragmenteren.png, 300,300]


=== Gegevens verwijderen, backup maken

Controleer voordat je een programma weghaalt, of er ook extra gegevens verdwijnen, die je met het programma hebt gemaakt. Wanneer je bijvoorbeeld een programma wegdoet, waarmee je foto's of documenten hebt gemaakt, kan het lastig zijn om ze met een ander programma te openen.
Je kunt het beste een backup maken van de gegevens, die je met een programma hebt gemaakt. Het programma is in de regel wel weer te vinden, maar je gegens natuurlijk niet..
Denk bij gegevens aan foto's, muziek, films, belangrijke documenten, email adressen, telefoonnummers, accountinstellingen, enz.

=== Harde reset

Wanneer echt niets meer werkt rest je alleen nog een 'harde reset'. Hiermee start je de computer opnieuw op.
Je kunt op twee manieren zo'n harde reset uitvoeren:

1. De aan/uit knop net zo lang ingedrukt houden, totdat de computer uitgaat. Na 5 seconden kun je de computer wel weer opnieuw aanzetten.
2. De stroomkabel uit het stopcontact halen. Wacht 5 seconden en plug de stroomkabel weer in. Start daarna de computer weer.

Bij tablets en smartphones kun je in zo'n geval het beste de accu in het apparaat loshalen en kort daarna weer terugplaatsen.

.Accu verwijderen.footnoteref:[disclaimer, Accu verwijderen, http://blog.mytrendyphone.nl/wp-content/uploads/2015/03/s6-accu.jpg]
image::http://blog.mytrendyphone.nl/wp-content/uploads/2015/03/s6-accu.jpg[200,200]

== link:../../studyoverview.php[Terug]
