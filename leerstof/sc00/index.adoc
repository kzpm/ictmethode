= Account aanmaken `Scratch` 
:toc: right

----
Module sc00
----

== Voorkennis
- Je hebt met GameStudio gewerkt.

== Wat ga je leren?
- Je leert hoe je een account aan moet maken in Scratch.
	
== Scratch, wat is het?

Met Scratch, kun je leren programmeren. 
Je leert hoe je een computer duidelijke instructies moet geven. 
Pas dan doet de computer wat jij wilt!


== Game Studio gedaan?

Heb je GameStudio nog niet gedaan? Dan is het handig om de lessen als nog te volgen, want dan weet je hoe voorwerpen in een spel zich "gedragen":

link:../gs100/index.html[Jouw game^]

link:../gs101/index.html[Veranderen^]

link:../gs200/index.html[Spel maken^]

link:../gs201/index.html[Opdracht 1: Een spel maken^]

link:../gs202/index.html[Opdracht 2: Voorwerpen veranderen^]

link:../gs203/index.html[Opdracht 3: Je eigen spel!^]


== Account aanmaken Scratch 


Stap 1:
Surf naar de link:https://scratch.mit.edu/[website^] van Scratch. 

Stap 2:
Klik op "Word Scratcher" of op "Join Scratch" ("Join" betekent hetzelfde als 
`meedoen`)

image::http://imgur.com/MyYaDWhl.png[300,300]


Stap 3:

Vul bij "username" je gebruikersnaam. Tip is om niet je eigen naam te 
gebruiken. Wanneer je Jan de Vries heet kun je het beste J de Vries als gebruikersnaam nemen.
Wanneer de gebruikersnaam al bestaat kun je je naam aanpassen naar J de Vries01 of iets dergelijks.
Denk erom dat punten en komma's in de gebruikersnaam niet geldig zijn!

Bedenk een goed wachtwoord met hoofdletters, kleine letters en cijfers 
van minimaal 8 tekens.
Vul het daarna nogmaals in.

Klik op "Next"

image::http://imgur.com/GBnfQHNl.png[350,350]

Vul je geboortemaand en jaar in. Je geslacht (Male = mannelijk, Female = 
vrouwelijk). Je mag bij geslacht ook "onbekend" invullen.


Vul bij het land "Netherlands" (= Nederland) in.



image::http://imgur.com/DqCYXFzl.png[350,350]


Klik op "Next"

Vul nu het volgende mailadres in:
ict.eshoek@primah.org

Je moet dit adres invullen, omdat het anders moeilijk wordt, om je werk 
in te leveren!


image::http://imgur.com/NjzfjZll.png[350,350]


Je account is nu klaar. Je ICT juf of meester, moet er nu voor zorgen 
dat je account geactiveerd wordt.
Daarna kun je er echt mee gaan werken!
	
== link:../../studyoverview.php[Terug]


