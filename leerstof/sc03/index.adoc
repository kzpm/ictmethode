= Opdracht 2 
:toc: right

----
Module sc03
----

== Voorkennis
- Je hebt met GameStudio gewerkt.
- Je hebt al een account in Scratch
- Je hebt opdracht 1 succesvol afgerond

== Wat ga je leren?
- Je leert nog meer over hoe Scratch werkt
- Je leert hoe je voorwerpen laat "bewegen"
- Je leert hoe je een gebeurtenis (in het Engels:"event") kunt laten herhalen
- Je leert hoe je een voorwerp kunt 'bedienen'.

== Maak een nieuw project in Scratch

- Ga naar de link:https://scratch.mit.edu/[website van Scratch^]
- Meld je aan via "Sign in" met je gebruikersnaam en wachtwoord

image::http://imgur.com/aBp16Fql.png[Ga naar "My Class"]


Je gaat nu zelf een project maken.

Klik linksboven op het woord "Create" (Dat betekent "Maak")

Je komt nu direct in de Scratch-editor. Dat is goed!


== De opdracht

Je moet de sprite van de kat verwijderen.


Verwijder eerst de sprite van de kat.
Klik met de rechtermuisknop op de sprite, en kies "delete".

image::http://imgur.com/HhaCRoel.png[300,300]

Daarna plaats je een nieuwe sprite uit de bibliotheek van Scratch.

Kies uit de bibliotheek de sprite "basketbal":

image::http://imgur.com/ggnTGTIl.png[300,300]


Om de bal te laten bewegen, stuiteren, moet je gebruikmaken van de opdrachten-bibliotheek. Deze vind je onder het tabblad "scripts":

image::http://imgur.com/o7OQlmpl.png[350,350]

Klik op "Motion" en kies het blauwe blokje "Go to x:<getal> y:<getal>"

Ziet er zo uit:

image::http://imgur.com/14lHzoMl.png[250,250]

Sleep dit blokje naar je editor werkblad en dupliceer het. Dit doe door met de rechtermuisknop op het blokje te klikken en dan voor "duplicate" te kiezen.
Je hebt dan twee dezelfde blokjes.

Ga nu in je opdrachten-bibliotheek naar "Control":

image::http://imgur.com/2CggAnhl.png[250,250]

Sleep het blokje met het opschrift "repeat <getal>" naar je editor werkblad. Repaet betekent "herhalen".

Sleep ook het blokje met het opschrift "Wait <getal> seconds" naar je werkblad en dupliceer het ook twee keer.

Als alles goed is heb je nu dit op je werkblad staan:

image::http://imgur.com/jKRJENFl.png[350,350]

Zorg er nu voor dat de basketbal 10x gaat stuiteren en daarna stopt. 
Aan jou de taak om de juiste getallen in de witte vakjes in te vullen!
Vergeet ook net om je project een naam te geven. Dit kun je zo doen:

image::http://imgur.com/wo4lVWsl.png[200,300]

== Inleveren en beoordeling

Wanneer je klaar bent met de opdracht, klik je bovenin de editor op de "Share" knop. Vergeet dit niet, anders wordt je project niet bewaard en kun je het ook niet 
delen!

image::http://imgur.com/7T03DOOl.png[200,200]

Je project wordt nu opgeslagen en gedeeld.

Klik link:opsturen.html[hier^] om je project op te sturen naar  je ICT juf/meester


*Beoordeling:*
Wanneer je je precies aan de opdracht hebt gehouden, komt er in je ICT leerlijnen bij je certificaat "Ja" te staan.
 


== link:../../studyoverview.php[Terug]


