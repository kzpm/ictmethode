= Hoe krijg ik bepaalde berichten over mij van het internet af?
:toc: right

----
Module at09
----

== Voorkennis
- Je weet hoe je een bericht plaatst. (Vb Facebook, SnapChat, link:http://www.woorden.org/woord/Fora[Fora^])


== Wat ga je leren?

Je leert in deze les :

- Hoe je informatie, over jezelf van internet af kunt krijgen.

=== Hoe kan ik erachter komen wat er over mij bekend is? Zoek jezelf!

Geef als zoekactie eens je voor- en achternaam. Wat kom je allemaal tegen over jezelf?
Als je iets gevonden hebt, vind je het dan prettig, dat iedereen dat kan weten?

.Zoek jezelf footnoteref:[disclaimer, Zoek jezelf, http://www.mijnonlineidentiteit.nl/personen-zoeken-op-het-internet/]
image::http://www.mijnonlineidentiteit.nl/wp-content/uploads/Personen-zoeken-op-het-internet-620x320.jpg[400,400]

=== Welke informatie kun je laten verwijderen?

Informatie:

- Die over jou door anderen zijn geplaatst en waarvoor je geen toestemming hebt gegeven
- Die link:http://www.woorden.org/woord/kwetsen[kwetsend^] of link:http://www.woorden.org/woord/beledigen[beledigend^] is
- Foto's waar jij op afgebeeld bent, maar waar je geen toestemming tot plaatsing voor hebt gegeven.(Vb. naaktfoto's)
- `Nametags` in foto's. Dit zijn foto's waar jij op staat met naamvenstertje om je hoofd. 

.Informatie verwijderen footnoteref:[disclaimer, Mijn online identiteit, http://www.mijnonlineidentiteit.nl/wp-content/uploads/Informatie-verwijderen-van-het-internet-600x500-1.png]
image::http://www.mijnonlineidentiteit.nl/wp-content/uploads/Informatie-verwijderen-van-het-internet-600x500-1.png[400,400]

=== Gegevens verwijderen via redacties/webbeheerders

Wanneer het niet lukt om informatie over jou weg te krijgen, neem dan contact op met de redactie of de webmaster van 
de website.
Bij Facebook kun je bijvoorbeeld geposte berichten wel uit je eigen tijdlijn wegkrijgen, maar niet uit die van je vrienden en 
kennissen! Pas dus op met wat je schrijft!

=== Gegevens verwijderen uit zoekresultaten zoekmachines

Wanneer er informatie over je in de zoekresultaten verschijnt, moet je contact opnemen met het bedrijf waar je het zoekresultaat 
tegenkwam.
Zo werkt het bij link:https://support.google.com/websearch/troubleshooter/3111061?hl=nl[Google]

=== Informatie verwijderen van een website

Neem contact op met de beheerder van de website. Wanneer dat niet lukt, kun je het bedrijf waar de website wordt 
link:http://www.woorden.org/woord/hosting[gehost] vragen.

Het hosting bedrijf vind je door de domeinnaam (Vb. www.google.com) in te typen in de link:https://whois.icann.org/en[zoekbox^] van 
deze website.

=== Informatie verwijderen van een link:http://www.woorden.org/woord/forum[forum]

Soms is het mogelijk om een bericht zelf te verwijderen van een forum, maar meestal is dat niet het geval.
Wanneer je het bericht niet zelf kunt verwijderen, moet je contact opnemen met de beheerder van het forum.
Wanneer er geen forumbeheerder is, kun je contact opnemen met de webmaster van het bedrijf wat het forum gebruikt.

=== Informatie verwijderen van social media

Elke social media denst, heeft een eigen stappenplan, waarmee je berichten kunt verwijderen.
Hou er rekening mee dat bij sommige diensten het helemaal niet mogelijk is om iets te laten verwijderen.
Je kunt dan alleen nog proberen met beheerders van zoekmachines in contact te komen. Misschien kunnen ze je bericht wegfilteren. 
Mogelijk moet je daar geld voor betalen..

=== Hoe zorg ik ervoor dat verwijderde gegevens niet opnieuw gepost worden?

Als het gelukt is om een bericht uit de zoekresultaten te halen, kan het best zijn dat enkele weken later je bericht gewoon opnieuw
gevonden kan worden.
Je moet dan weer contacty opnemen met de zoekmachine-beheerder...

=== Voorkomen is beter dan genezen.

Je ziet dat het erg lastig is om berichten of artikelen echt van internet af te krijgen. Pas dus gewoon op met wat je beweert en 
schrijft op internet.
Over 50 jaar is je bericht misschien nog steeds vindbaar...

.Voorkomen...footnoteref:[disclaimer, ChristenUnie, https://leusden.christenunie.nl/l/library/download/D_a_GvloSOPXlk0G2aag9FvyVb3EVxokuZv/tegel+voorkomen+is+beter+dan+genezen.jpg?color=ffffff&scaleType=5&width=168&height=168&ext=.jpg]
image::https://leusden.christenunie.nl/l/library/download/D_a_GvloSOPXlk0G2aag9FvyVb3EVxokuZv/tegel+voorkomen+is+beter+dan+genezen.jpg?color=ffffff&scaleType=5&width=168&height=168&ext=.jpg[300,300]

=== Identiteitsfraude

Post alsjeblieft geen adresgegevens, rekeningnummers, kopieen van paspoorten of identiteitskaarten over internet. Sla deze 
informatie ook niet op in de cloud.

Voor kwaadwillenden is het heel eenvoudig om deze gegevens te gebruiken om op jouw naam aankopen te doen of andere misdaden te 
plegen. En jij draait er dan voor op...

.Identiteitsfraude footnoteref:[disclaimer, Rijksoverheid, https://www.rijksoverheid.nl/binaries/large/content/gallery/rijksoverheid/content-afbeeldingen/ministeries/bzk/2015/idfraude/idfraude-kopie-paspoort-op-mobiel.jpg]
image::https://www.rijksoverheid.nl/binaries/large/content/gallery/rijksoverheid/content-afbeeldingen/ministeries/bzk/2015/idfraude/idfraude-kopie-paspoort-op-mobiel.jpg[300,300]

== link:../../studyoverview.php[Terug]


