= Scratch 
:toc: right

----
Module sc01
----

== Voorkennis
- Je hebt met GameStudio gewerkt.
- Je hebt al een account in Scratch

== Wat ga je leren?
- Je leert hoe Scratch werkt
	
== Scratch, wat is het?

Met Scratch, kun je leren programmeren. Je leert hoe je een computer duidelijke instructies moet geven. Pas dan doet de computer wat jij wilt!

== Blokjes

In Scratch programmeer je met 'blokjes'. Je kunt de blokjes onder elkaar en naast elkaar zetten. Als je alles goed doet, gaat je programma echt 
werken! Hoe leuk is dat!

.Het 'Scratch' logo footnoteref:[disclaimer, Wikimedia.com, https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Scratchcat.svg/180px-Scratchcat.svg.png] 
image::https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Scratchcat.svg/180px-Scratchcat.svg.png[300,300]


== Voordat je aan het werk gaat

Natuurlijk wil direct aan de slag. 
Maar ho... even geduld nog! Wanneer je wilt begrijpen hoe je dingen in Scratch voor elkaar krijgt is het misschien 
handig om de "Scratch show" te bekijken.

Surf eerst naar de link:https://scratch.mit.edu[website^] van Scratch en log in.

image::http://imgur.com/E1JY1Cql.png[350,350]

Klik in het hoofdmenu op "Help" en dan op "Get started with Scratch". Probeer de demo ("Try the step-by-step intro")

In het Engels wordt verteld, dat je een "bewegingsblokje" (= move) op het werkblad moet slepen.
Wanneer je op het blokje klikt, zie je dat de kat gaat lopen.

Ga naar de volgende dia.

Je moet nu een drumgeluid uit de geluidsbibliotheek onder het eerste blokje slepen. Je weet vast al dat geluid in het Engels "sound" betekent!

Probeer alle stappen van de uitleg te doorlopen, dan kun je al aardig op weg met Scratch.


== Aan het werk met Scratch


Wanneer je je eerste project in Scratch gaat maken, krijg je dit scherm in beeld:

image::http://imgur.com/OszC9bil.png[1200,1200]

Dit is je `editor`. Wanneer je gaat programmeren, wordt de omgeving waarin je je code gaat schrijven een `editor` genoemd.

Ga er maar lekker even mee spelen.


	
== link:../../studyoverview.php[Terug]


