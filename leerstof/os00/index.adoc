
= Woordenboek
:toc: right

----
Module os00
----

== Voorkennis

Geen voorkennis nodig

== Wat ga je leren?

Je leert in deze les de betekenis van een aantal begrippen, die horen bij het gebruik van computers.

== Lezen

=== Desktop/Laptops/Netbooks/Servers/Tablets/Smartphones. 

*Desktops*-
Computers met een grote kast en een los beeldscherm, worden 'desktops' genoemd. Je moet achter een bureau zitten om er mee te kunnen werken.
Desktops worden niet veel meer verkocht. 
Mensen die veel computerspellen spelen ('gamers'), maken nog wel gebruik van desktops. Het woord desktop komt uit het Engels. 'Desk' betekent bureau.

.Desktop.footnoteref:[disclaimer, Mindsetcomputerrepairs.com.]
image::http://www.mindsetcomputerrepairs.com/graphics/HP-desktop-computer-repair.jpg[Desktop, 300,500, float="right",align="center"]

*Laptops* -
Het woord 'lap' is Engels. Het kan 'ronde' of 'schoot' betekenen. Bij het woord laptop heb je dan al snel door dat het om een 'schootcomputer' gaat. Het toetsenbord en het beeldscherm vormen een geheel.

.Netbook.footnoteref:[disclaimer, Informationonelectronics.com]
image::https://upload.wikimedia.org/wikipedia/commons/e/e5/ASUS_Eee_White_Alt.jpg[Netbook, 200,300,float="right"]

*Netbooks* -
Een kleinere uitvoering van een laptop.

*Servers* -
Dit zijn computers die steeds iets voor je regelen. 
Bijvoorbeeld: opslagruimte voor je bestanden, je mail ontvangen en versturen, je berichten doorsturen, je videogesprekken doorsturen, jouw surfgegevens opslaan. 
Meestal staan die servers in 'data-centers'. Dit zijn grote hallen, waarin duizenden servers worden beheerd.

.Gang met server-racks.footnoteref:[disclaimer, chessbase.com]
image::http://en.chessbase.com/portals/4/files/news/2008/computers03.jpg[Server, 300,500, float="right"]

*Tablets* -
Dit is een minicomputer, die bediend kan worden met je vinger via een touchscreen. Een toetsenbord aansluiten is niet meer nodig. 

.Tablet.footnoteref:[disclaimer,Dlcdimgns.asus.com]
image::https://dlcdnimgs.asus.com/20160129_cosmo/cosmo/websites/global/products/ljpuYUTL7HW5HMen/img/accessory/smart-cover-blue.png[Tablet, 200, 400, float="right"]


*Smartphone* -
 Een smartphone gebruikt dezelfde techniek als een tablet. Met een smartphone kun je bellen, met een tablet niet. Een smartphone is wat kleiner dan een tablet.

.Smartphone.footnoteref:[disclaimer, i.telegraph.co.uk]
image::http://i.telegraph.co.uk/multimedia/archive/03419/honor-7_3419528b.jpg[Smartphone, 200,100, float="left"]

=== Geheugen

Computers maken veel gebruik van geheugen. In het geheugen kan informatie opgeslagen worden, die de computer even later weer nodig heeft.
Er zijn twee soorten geheugens: *intern* geheugen en *extern* geheugen.
Intern geheugen bewaart informatie tijdelijk. Wanneer de computer uitgeschakeld wordt, worden de gegevens in het geheugen gewist.
Extern geheugen kan informatie veel langer bewaren. Ook wanneer de computer uitgezet wordt, blijven de gegevens dus bewaard.

Een voorbeeld van intern geheugen zijn de geheugenkaarten, die in je computer zitten.
Een voorbeeld van extern geheugen, is een SD kaart een USB stick of een harddisk.

.Intern geheugen.footnoteref:[disclaimer, ramght.com]
image::http://www.ramght.com/upfile/product/1gb-ram-memory-ddr-sdram_3.jpg[Intern geheugen,200,400, float="right"]

.Extern geheugen.footnoteref:[disclaimer,cdn3.androidworld.nl]
image::http://cdn3.androidworld.nl/160bd2ebf6/media/Promo%20Mini%20USB%20OTG_big.jpg[Extern geheugen, 100,100 float="right"]


=== Hardware

Dit zijn alle onderdelen van de computer, die je aan zou kunnen raken. *Probeer dit overigens niet uit wanneer de computer aangesloten is op netstroom! Haal de stekker 
er altijd uit, wanneer je aan je computer wil gaan sleutelen!*

.Hardware.footnoteref:[disclaimer, 1.bp.blogspot.com]
image::http://1.bp.blogspot.com/-P5RWMuEOjmk/TlGytBW9sQI/AAAAAAAAAE8/3XpleFl8J5o/s1600/Graphic1.jpg[Hardware, 350,350, float="right"]


=== SD kaarten

Je weet nu waarschijnlijk al dat een SD kaart extern geheugen is. Dat betekent dus, dat de gegevens op het kaartje blijven staan. Ook al staat de computer uit.
Er wordt veel gebruik gemaakt van SD kaarten in mobiele apparaten, dus in smartphones en tablets. Ook de populaire Raspberry Pi werkt geheel vanaf een SD kaart.

.SD kaarten.footnoteref:[disclaimer, http://vignette3.wikia.nocookie.net/egamia/images/2/2f/Sd-cards.jpg/revision/latest?cb=20140907232248]
image::http://vignette3.wikia.nocookie.net/egamia/images/2/2f/Sd-cards.jpg/revision/latest?cb=20140907232248[SD cards, 150, 150, float="right"]


=== Bits en Bytes

In vrijwel de hele wereld wordt het 'decimale' stelsel gebruikt, wanneer we gaan rekenen. 

==== Decimaal
'Deci' is een latijns woord wat '10' betekent. Het decimale stelsel bestaat uit 10 getallen: 0,1,2,3,4,5,6,7,8 en 9.

==== Binair footnoteref:[disclaimer,Beter rekenen,https://www.beterrekenen.nl/website/index.php?pag=259]
Computers werken met het binaire stelsel. 'Bi' is ook Latijns en betekent 'twee'. 
In het binaire stelsel gebruik je twee getallen: 0 en 1.
Neem een kijkje op link:https://www.beterrekenen.nl/website/index.php?pag=259[Beter rekenen^] om meer te weten te 
komen over binaire getallen.

==== Kilobyte, megabyte en meer

In de computerwereld zie je vaak termen als kilobyte, megabyte, gigabyte en terabyte, bijvoorbeeld voor de 
opslagcapaciteit van een harddisk. Er is weleens een misverstand over de betekenis van die termen.

    1 kB (kilobyte) = 1000 bytes
    1 MB (megabyte) = 1000 kB = 1.000.000 bytes
    1 GB (gigabyte) = 1000 MB = 1.000.000.000 bytes
    1 TB (terabyte) = 1000 GB = 1.000.000.000.000 bytes

Sinds 1998 geldt de afspraak dat de stappen van 1024 worden aangeduid met kibibyte, mebibyte, gibibyte en tebibyte:

    1 KiB (kibibyte) = 1024 bytes
    1 MiB (mebibyte) = 1024 KiB = 1.048.576 bytes
    1 GiB (gibibyte) = 1024 MiB = 1.073.741.824 bytes
    1 TiB (tebibyte) = 1024 GiB = 1.099.511.627.776 bytes

==== Bit = schakelaar
Een *bit* kun je goed met een lichtschakelaar vergelijken. 
Hij kan uit -of aanstaan. Wanneer de bit 'uit' staat, heeft het de waarde '0'. Wanneer de bit aanstaat, heeft het de waarde '1'.

Zo kun je heel veel schakelaartjes naast elkaar zetten. Hoe meer schakelaartjes je naast elkaar zet, hoe meer opdrachten de computer aankan.

==== Bytes
Een *byte* is een groepje van 8 schakelaars (bits). Als ze allemaal uitstaan, dan heeft een byte de waarde 0 0 0 0 0 0 0 0.
Ini ons decimale stelsel betekent dat gewoon 0.
Weet je nu hoe een byte er uit ziet als alle schakelaars aan staan.footnoteref:[disclaimer, Natuurlijk: 1 1 1 1 1 1 1 1]?
Weet je ook de decimale waarde ervan?

=== Processor

.Processor.footnoteref:[disclaimer, http://techreport.com/r.x/athlon64-x2-3800/chip-top2.jpg]
image::http://techreport.com/r.x/athlon64-x2-3800/chip-top2.jpg[Processor, 150,150, float="right"]

De processor zorgt ervoor dat alle schakelaartjes bediend worden. 

=== Gegevens of 'Data'

Data is een ander woord voor gegevens. Wat zijn gegevens dan? 
Gegevens zijn stukjes informatie. 
Voorbeeld van gegevens: bestanden, afbeeldingen, tekst, muziek, video. 
Wanneer je veel gegevens hebt van een bepaald onderwerp, dan noemen we dat een 'gegevensverzameling'.
Computers zijn erg goed in het verzamelen, analyseren en opslaan van gegevens. 

.Op deze disk kun je 1 teraByte aan informatie opslaan!.footnoteref:[disclaimer,cdn2.expertreviews.co.uk]
image::http://cdn2.expertreviews.co.uk/sites/expertreviews/files/5/59//seagate_laptop_sshd.jpg?itok=RDD3g9G_[Harddisk,250,250,float="right"]

=== Videokaart en monitor

Om beeld op je monitor te krijgen, heeft je computer een videokaart nodig. 
Deze videokaart zorgt voor het achtergrondlicht in je monitor, de juiste beeldverhouding, de juiste beeldscherpte en de kleuren.

Er zijn heel wat soorten en maten monitors te koop.
Op de afbeelding kun je zien welke afmetingen monitors hebben.


.Beeldverhouding.footnoteref:[disclaimer, http://www.equasys.de/tl_files/equasys/content/equasysAspectRatio.png]
image:http://www.equasys.de/tl_files/equasys/content/equasysAspectRatio.png[Aspect ratio,400,400,float="leftt"]



=== Randapparatuur
.Randapparatuur.footnoteref:[disclaimer,Ormedia.nl Bron: https://ormedia.nl/media/catalog/category/ormedia-categorie-randapparatuur.jpg]
image:https://ormedia.nl/media/catalog/category/ormedia-categorie-randapparatuur.jpg[Randapparatuur,250,250,float="left"]

Behalve de processor (CPU) en het geheugen, is er vaak extra apparatuur nodig om de computer opdrachten uit te laten voeren. Alle apparaten, die je daarvoor kunt 
gebruiken noem je 'randapparatuur'.
Opslagmedia, zoals USB sticks, harddisks of dvd recorders, horen dus bij randapparatuur.
De meeste hardware valt dus onder randapparaten.




== link:../../studyoverview.php[Terug]


