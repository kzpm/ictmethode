= Help ik word gepest
:toc: right

----
Module at08
----

== Voorkennis
- Je weet wat link:http://www.woorden.org/woord/cyberpesten[(cyber)pesten^] is
- Je kent je eigen rol
- Je kent link:http://www.schoolenveiligheid.nl/po-vo/kennisbank/online-pesten/[gedragsregels^]

== Wat ga je leren?

Je leert in deze les 



=== Leg contact met je vertrouwenspersoon!

=== Wel of niet reageren

=== Gebeurtenissen bijhouden

=== Speurwerk

=== Bewijsmateriaal verzamelen

=== Rol van ouders

=== Rol van school

=== Aangifte doen

=== Praktijk gevalstudie



== link:../../studyoverview.php[Terug]


