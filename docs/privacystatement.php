<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/inc/header.inc';

echo '
<div class="container">
<h1>Privacy statement</h1>

<h4>ICTmethode is een Massive open online cursussysteem (MOOC).
<br>
<br>
De methode is gericht op het primair onderwijs in Nederland.
<br>
<br>
Van meet af aan, is de cursus opgezet met het oogmerk op `data-minimalisatie`.
Dit houdt in dat er alleen een gebruikersnaam en wachtwoord wordt gevraagd om de cursus te kunnen starten. 
Immers alleen deze gegevens zijn strikt noodzakelijk om voortgang bij te houden. 
<br>
<br>
De gebruikersnaam is dus nodig om voortgang van de cursus bij te houden. In combinatie met een wachtwoord, wat door de applicatie gegenereerd wordt, zijn deze gegevens uniek voor een persoon.
<br>
<br>
Gegevens in de database zijn echter niet te herleiden tot personen of organisaties, tenzij de gebruiker zelf kiest om een herleidbare naam te gebruiken.
Tijdens het registratie proces, worden toekomstige gebruikers er echter op gewezen geen persoonlijk herleidbare informatie te gebruiken.
<br>
<br>
De broncode en de layout van de database is <a href="https://github.com/kzpm/leerlijnen_ict_wiki">publiekelijk toegankelijk en verifieerbaar</a>
</h4>
</div>
';

?>
