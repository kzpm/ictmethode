<?php

class clsMyShortCode{

	protected $resultCode;
	protected $arr= 'abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789=';

	public function fResultCode( $voorNaam, $leerjaar ){


		if ( !empty( $voorNaam )  ){

			$this->resultCode = $this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))];

			if ( !$this->fCheckIfExists( $this->resultCode ) ) {
		

				if ( $this->fStoreCode( $this->resultCode, $voorNaam, $leerjaar )){


					return $this->resultCode;

				}//end if

		
			}else{

				return FALSE;

			}//end if

		}//end if


	}//end f

	protected function fStoreCode( $resultCode, $voorNaam, $leerjaar ){

		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();

		require_once 'clsStudentNrManager.class.php';
		$myMax =new clsStudentNrManager();

		if ( $myConnect->fConnect() ){
			
			$sql = sprintf("INSERT INTO `user` values( %d, %d,'%s','%s', '%s' )", 
			mysqli_real_escape_string( $myConnect::$cn, 0 ),
			mysqli_real_escape_string( $myConnect::$cn, $myMax->fGetMaxStudentNr()+1 ),
			mysqli_real_escape_string( $myConnect::$cn, $voorNaam ),
			mysqli_real_escape_string( $myConnect::$cn, $this->resultCode ),
			mysqli_real_escape_string( $myConnect::$cn, $leerjaar )
			);

			$result = mysqli_query( $myConnect::$cn, $sql );
			if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

				return TRUE;

			}else{

				return FALSE;

			}//end  if

		}//end if


	}//end f


	protected function fCheckIfExists( $Code ){

		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();

		if ( $myConnect->fConnect() ){
        
                                $sql = sprintf("select code from user where code = '%s'", mysqli_real_escape_string( $myConnect::$cn, $Code ) );
                                $result = mysqli_query( $myConnect::$cn, $sql );

                                if ( mysqli_num_rows( $result ) > 0 ){


					$this->resultCode = $this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))].$this->arr[rand(0, strLen( $this->arr ))];
					return TRUE;

                                }else{

					return FALSE;

				}//end  if

                }//end if


	}//end f




}//end class
?>
