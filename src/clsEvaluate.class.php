<?php


class clsEvaluate{

	public function fCreateEvaluation( $nr_student, $cursus_code){

		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();
		if ( $myConnect->fConnect() ){

		
			$sql =sprintf("select question, try, ok from q%s%s where nrstudent=%s and cursus_code='%s' order by question ASC", mysqli_real_escape_string( $myConnect::$cn, $nr_student ), mysqli_real_escape_string( $myConnect::$cn, $cursus_code ), mysqli_real_escape_string( $myConnect::$cn, $nr_student ), mysqli_real_escape_string( $myConnect::$cn, $cursus_code )  );
//echo $sql;
			$result = mysqli_query( $myConnect::$cn, $sql );
			while ( $eval = mysqli_fetch_assoc( $result ) ){
				
					

					$score = 100*( $eval['ok']/$eval['try'] );
					$score = number_format((float)$score, 2, '.', '');
				        $eval_items[] = '<tr><td>'.$eval["question"].'</td><td>'.$eval["try"].'</td><td>'.$score.'</td></tr>' ;


			}//end while
	

			return $eval_items;


		}//end if
		mysqli_free_result( $result );
                mysqli_close( $myConnect::$cn );

	}//end f

}//end class
