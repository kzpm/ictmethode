<?php

class clsHasRecords{


	public function fHasRecords( $tableName ){


		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();

		if ( $myConnect->fConnect() ){


			$sql = sprintf("select * from %s ",mysqli_real_escape_string( $myConnect::$cn, $tableName ) );
//echo $sql;
			$result = mysqli_query( $myConnect::$cn, $sql );
			
			if ( mysqli_num_rows( $result ) > 0 ){
				return FALSE;

			}else{

				return TRUE;

			}//end if

			mysqli_free_result( $result );

		}//end f

		mysqli_close( $myConnect::$cn );

	}//end f

}//end class


?>
