<?php


class clsCreateCert{



	public function fCreateCert( $leerjaar, $nr_student, $cursus_code ){

		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();
		if ( $myConnect->fConnect() ){


			$sql =sprintf("select SUM(ok) from q%s%s where nrstudent=%s and cursus_code='%s' order by question ASC", 
			mysqli_real_escape_string( $myConnect::$cn, $nr_student ), 
			mysqli_real_escape_string( $myConnect::$cn, $cursus_code ), 
			mysqli_real_escape_string( $myConnect::$cn, $nr_student ), 
			mysqli_real_escape_string( $myConnect::$cn, $cursus_code )  );
//echo $sql;
			$result = mysqli_query( $myConnect::$cn, $sql );
			$cert = mysqli_fetch_row( $result );
//echo "Stap 0";

			if ( $cert[0] == 10 ){

//echo "Stap 1";
				if ( $this->fCert2PDF( $leerjaar, $nr_student, $cursus_code ) ){
//echo "Stap 2";
					$sql=sprintf( "update user%s%s set certificaat='1' where cursus_code='%s'", 
					mysqli_real_escape_string( $myConnect::$cn, $leerjaar ), 
					mysqli_real_escape_string( $myConnect::$cn, $nr_student ), 
					mysqli_real_escape_string( $myConnect::$cn, $cursus_code ) );
//echo $sql;
					$result=mysqli_query( $myConnect::$cn, $sql );
					if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

						return TRUE;

					}else{

						return FALSE;

					}//end if

				}else{

					return "<div class='wrong'>Kon geen certificaat maken..</div>";

				}//end if

			}else{

				return "<div class='wrong'>Het is niet gelukt een certificaat te maken..</div>";

			}//end if


		//print_r( $eval_items );
		return $eval_items;

		}//end if
		mysqli_free_result( $result );
                mysqli_close( $myConnect::$cn );

	}//end f




	public function fCert2PDF( $leerjaar, $nr_student, $cursus_code ){
	

		$rdir=$_SERVER["DOCUMENT_ROOT"]."/certs/";
		$studentName=$this->fGetStudentName( $nr_student );
		//echo $studentName;
		$cursus_naam=$this->fGetCourseName( $leerjaar, $nr_student, $cursus_code );

		require_once '../../inc/fpdf/fpdf.php';
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetFont( 'Arial','B',20 );

		$pdf->Cell( 0,10,'Certificaat', 1,1,"C" );
		$pdf->SetFont( 'Arial','B',16 );
		$pdf->Cell( 0,10,'Basisvaardigheden ICT Basisonderwijs', 0,1,"C" );
		$pdf->Ln( 20 );

		$pdf->SetFont('Arial','B',30);
		$pdf->Cell( 0,10,$studentName, 0,1,"C" );
		$pdf->Ln( 20 );

		$pdf->SetFont( 'Arial','B',16 );
		$pdf->Cell( 80,10,"Module" );
		$pdf->Cell( 0,10, $cursus_naam,0,1,"L" );
		$pdf->Cell( 80,10,"Module code" );
		$pdf->Cell( 0,10, $cursus_code,0,1,"L" );
		$pdf->Cell( 80,10,"Leerjaar" );
		$pdf->Cell( 0,10,$leerjaar,0,1,"L" );
		$pdf->Cell( 80,10,"Gemiddelde score " );
		$pdf->Cell( 0,10, $this->fCalcScore( $nr_student, $cursus_code ),0,1,"L" );
		$pdf->Cell( 80,10,"Datum" );
		$pdf->Cell( 0,10,date("d-m-Y"),0,1,"L" );
		$fileName=$rdir.$cursus_code.'-'.$nr_student.'_cert.pdf';
		//echo $fileName;
		$pdf->Output( $fileName, 'F' );
		
		if ( file_exists(  $fileName ) ){

			return TRUE;

		}else{

			return FALSE;

		}//end if

	}//end f


	public function fCalcScore( $nr_student, $cursus_code ){


		require_once 'clsDbConnect.class.php';
                $myConnect = new clsDbConnect();
		
		// let's print the international format for the en_US locale
		setlocale(LC_MONETARY, 'nl_NL');
                
		if ( $myConnect->fConnect() ){

			$sql=sprintf( "select sum(ok)/sum(try) from q%s%s", \
			mysqli_real_escape_string( $myConnect::$cn, $nr_student  ), \
			mysqli_real_escape_string( $myConnect::$cn, $cursus_code ) );
			$result=mysqli_query( $myConnect::$cn, $sql );  
                        $score=mysqli_fetch_row( $result );

                        //return abs( $score[0] *10 );
			return number_format(  $score[0] *10, 2 );

                }//end if
                mysqli_free_result( $result );
                mysqli_close( $myConnect::$cn );
	

	}//end f


	public function fGetStudentName( $nr_student ){


		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();

		if ( $myConnect->fConnect() ){
	
			$sql=sprintf( "select voornaam from user where nrstudent=%s", mysqli_real_escape_string( $myConnect::$cn, $nr_student ) );
//echo $sql;
			$result=mysqli_query( $myConnect::$cn, $sql );	
			$studentName=mysqli_fetch_row( $result );
			//$student=studentName[0];

			return ucfirst( $studentName[0] );

		}//end if
		mysqli_free_result( $result );
		mysqli_close( $myConnect::$cn );

	}//end f


public function fAliasCursusCode( $cursus_code ){


	if ( !empty( $cursus_code ) ){

		switch ( $cursus_code ){

			case "os00":
        			return "0-8-besturingssysteem00";
        		break;

		}//end switch


	}// end if


}//end f

	public function fGetCourseName( $leerjaar, $nr_student, $cursus_code ){

		require_once 'clsDbConnect.class.php';
                $myConnect = new clsDbConnect();

                if ( $myConnect->fConnect() ){
        
                        $sql=sprintf( "select cursus_naam from  user%s%s where cursus_code='%s'", mysqli_real_escape_string( $myConnect::$cn, $leerjaar ), mysqli_real_escape_string( $myConnect::$cn, $nr_student ), mysqli_real_escape_string( $myConnect::$cn, $cursus_code ) );
//echo $sql;
                        $result=mysqli_query( $myConnect::$cn, $sql );  
                        $courseName=mysqli_fetch_row( $result );
                        //$student=studentName[0];

                        return ucfirst( $courseName[0] );

                }//end if
                mysqli_free_result( $result );
                mysqli_close( $myConnect::$cn );




	}//end f

}//end class


//$myCert = new clsCreateCert();
//$myCert->fCreateCert( 5, 12, "os00" );


?>
