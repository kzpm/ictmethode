<?php


class clsUserConnect{

	public function fUserConnect( $leerjaar, $user_code ){

		require_once 'src/clsGetStudentNr.class.php';
		$myStudentNr= new clsGetStudentNr();
		
		$nr_student=$myStudentNr->fGetStudentNr( $user_code );
		require_once 'src/clsDbConnect.class.php';
		$myConnect = new clsDbConnect();
		if ( $myConnect->fConnect() ){
		
			$sql =sprintf("create table if not exists user%s%s (idp int(11) not null auto_increment,cursus_nr int(11), leerjaar char(1), nrstudent int(11),cursus_code char(50),cursus_naam varchar(155),lezen varchar(100),toets varchar(100), certificaat int(5), primary key (idp) );",mysqli_real_escape_string( $myConnect::$cn, $leerjaar ), mysqli_real_escape_string( $myConnect::$cn, $nr_student[0] ) );
//echo $sql;
			if ( mysqli_query( $myConnect::$cn, $sql ) ){


				return TRUE;
	
  		        }else{

				return FALSE;

		   	}//end if



		}//end if

	}//end f

}//end class
