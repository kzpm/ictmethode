<?php

class clsDbConnect{
        static public $db;
        static public $cn;

        public function __construct(){
                self::fConnect();
        }//end f

        /*fConnect is de database connector. resp. host, username, ww, database*/
        public static function fConnect(){

                if(self::$cn = mysqli_connect('localhost','dbuser','dbpass','dbname')){

                        if(self::$db = mysqli_select_db(self::$cn, 'dbname')){

                                return TRUE;

                        }else{

                                return FALSE;

                        }//end if

                }else{

                        return FALSE;

                }//end if

        }//end f


}//end class

