<?php
/*
MariaDB [itv]> describe jts810;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| idp         | int(11)      | NO   | PRI | NULL    | auto_increment |
| cursus_nr   | int(11)      | YES  |     | NULL    |                |
| leerjaar    | char(1)      | YES  |     | NULL    |                |
| nrstudent   | int(11)      | YES  |     | NULL    |                |
| cursus_code | char(50)      | YES  |     | NULL    |                |
| cursus_naam | varchar(155) | YES  |     | NULL    |                |
| lezen       | varchar(100) | YES  |     | NULL    |                |
| oefening    | varchar(100) | YES  |     | NULL    |                |
| toets       | varchar(100) | YES  |     | NULL    |                |
| certificaat | char(1)      | YES  |     | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
10 rows in set (0.01 sec)
//(1".$prefix.$i.",%s,%s,'".$i."-8-besturingssysteem".$prefix.$i."', '".$os_array[$i]."', 

*/
class clsInsertCourses{

		public function fInsertCourses_OS( $tableName, $leerjaar, $nrstudent){
			
			$NR_COURSES =10;//Aanpassen als er een module bijkomt of afgaat
			$log="";
			require_once 'clsDbConnect.class.php';
			$myConnect = new clsDbConnect();
			$os_array=array( "Woordenboek","Wat is een `besturingssysteem`?","Bestandsbewerking", "Zoeken naar bestanden", "Zoeken en vervangen van tekst", 
"Help: computer is vastgelopen!", "Help: Heeft mijn apparaat een virus?","Help: mijn computer is gehackt!","Help: ik heb geen internet!","Sneltoetsen");

			if ( $myConnect-> fConnect() ){

				switch( $leerjaar ){
				case 8:
					for ( $i=0;$i < $NR_COURSES;$i++ ){

                				if ( $i < $NR_COURSES ){

							$prefix=0;
						}else{

							$prefix="";

						}//end if

						$sql = sprintf( "insert into %s (cursus_nr,leerjaar,nrstudent,cursus_code,cursus_naam,lezen,toets,certificaat) VALUES
						(1".$prefix.$i.",%s,%s,'os".$prefix.$i."', '".$os_array[$i]."', 
						'http://ictmethode.nl/leerstof/os".$prefix.$i."/index.html',
						'http://ictmethode.nl/toets/os".$prefix.$i."/1.php',0)",
						mysqli_real_escape_string ( $myConnect::$cn, $tableName ),
						mysqli_real_escape_string ( $myConnect::$cn, $leerjaar ),
						mysqli_real_escape_string ( $myConnect::$cn, $nrstudent ) );
//echo $sql;
						$result=mysqli_query( $myConnect::$cn, $sql );
						if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

                                        		$log.="TRUE";

                                		}else{

                                        		$log.="FALSE";
				

						}//end if
						

        				}//end for
//					mysqli_free_result( $result );
       	                       		mysqli_close( $myConnect::$cn );

					if ( strPos( "FALSE", $log ) ){
						
						return FALSE;


					}else{

						return TRUE;
							

					}//end if
				break;


				}//end switch

			}//end if

		}//end f


		public function fInsertCourses_AT( $tableName, $leerjaar, $nrstudent){
			
			$NR_COURSES = 9;
			$log="";
			require_once 'clsDbConnect.class.php';
			$myConnect = new clsDbConnect();
			$at_array=array( "Woordenboek","Big Data","Jouw profiel op internet", "Wachtwoorden", "Jouw omgang met computers","Advertenties op internet","Help: is dit echt of niet?","Help: ben ik gehackt?","Hoe krijg ik negatieve berichten over mij van het internet af?");

			if ( $myConnect-> fConnect() ){

				switch( $leerjaar ){
				case 8:
					for ( $i=0;$i < $NR_COURSES;$i++ ){

                				if ( $i < $NR_COURSES ){

							$prefix=0;
						}else{

							$prefix="";

						}//end if

						$sql = sprintf( "insert into %s (cursus_nr,leerjaar,nrstudent,cursus_code,cursus_naam,lezen,toets,certificaat) VALUES
						(2".$prefix.$i.",%s,%s,'at".$prefix.$i."', '".$at_array[$i]."', 
						'http://ictmethode.nl/leerstof/at".$prefix.$i."/index.html',
						'http://ictmethode.nl/toets/at".$prefix.$i."/1.php',0)",
						mysqli_real_escape_string ( $myConnect::$cn, $tableName ),
						mysqli_real_escape_string ( $myConnect::$cn, $leerjaar ),
						mysqli_real_escape_string ( $myConnect::$cn, $nrstudent ) );
//echo $sql;
						$result=mysqli_query( $myConnect::$cn, $sql );

						if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

                                        		$log.="TRUE";

                                		}else{

                                        		$log.="FALSE";
				

						}//end if


        				}//end for
//					mysqli_free_result( $result );
                               		mysqli_close( $myConnect::$cn );

					if ( strPos( "FALSE", $log ) ){
						
						return FALSE;


					}else{

						return TRUE;
							

					}//end if
				break;


				}//end switch

			}//end if

		}//end f

		public function fInsertCourses_SW( $tableName, $leerjaar, $nrstudent){
			
			$NR_COURSES = 10;
			$log="";
			require_once 'clsDbConnect.class.php';
			$myConnect = new clsDbConnect();
			$sw_array=array( "Woordenboek","Beroep en software","Downloaden van software", "Installeren van software", "Verwijderen van software","Configureren van software","Gedrag van software","Software in de cloud","Apps","Software storingen");

			if ( $myConnect-> fConnect() ){

				switch( $leerjaar ){
				case 8:
					for ( $i=0;$i < $NR_COURSES;$i++ ){

                				if ( $i < $NR_COURSES ){

							$prefix=0;
						}else{

							$prefix="";

						}//end if

						$sql = sprintf( "insert into %s (cursus_nr,leerjaar,nrstudent,cursus_code,cursus_naam,lezen,toets,certificaat) VALUES
						(2".$prefix.$i.",%s,%s,'sw".$prefix.$i."', '".$sw_array[$i]."', 
						'http://ictmethode.nl/leerstof/sw".$prefix.$i."/index.html',
						'http://ictmethode.nl/toets/sw".$prefix.$i."/1.php',0)",
						mysqli_real_escape_string ( $myConnect::$cn, $tableName ),
						mysqli_real_escape_string ( $myConnect::$cn, $leerjaar ),
						mysqli_real_escape_string ( $myConnect::$cn, $nrstudent ) );
//echo $sql;
						$result=mysqli_query( $myConnect::$cn, $sql );

						if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

                                        		$log.="TRUE";

                                		}else{

                                        		$log.="FALSE";
				

						}//end if


        				}//end for
//					mysqli_free_result( $result );
                               		mysqli_close( $myConnect::$cn );

					if ( strPos( "FALSE", $log ) ){
						
						return FALSE;


					}else{

						return TRUE;
							

					}//end if
				break;


				}//end switch

			}//end if

		}//end f
		public function fInsertCourses_PR( $tableName, $leerjaar, $nrstudent){
			
			$NR_COURSES = 5;
			$log="";
			require_once 'clsDbConnect.class.php';
			$myConnect = new clsDbConnect();
			$sw_array=array( "Onthouden","Bewerkingen","Als..dan","Aanmelden in `Scratch`","Scratch opdracht");

			if ( $myConnect-> fConnect() ){

				switch( $leerjaar ){
				case 8:
					for ( $i=0;$i < $NR_COURSES;$i++ ){

                				if ( $i < $NR_COURSES ){

							$prefix=0;
						}else{

							$prefix="";

						}//end if

						$sql = sprintf( "insert into %s (cursus_nr,leerjaar,nrstudent,cursus_code,cursus_naam,lezen,toets,certificaat) VALUES
						(2".$prefix.$i.",%s,%s,'pr".$prefix.$i."', '".$sw_array[$i]."', 
						'http://ictmethode.nl/leerstof/pr".$prefix.$i."/index.html',
						'http://ictmethode.nl/toets/pr".$prefix.$i."/1.php',0)",
						mysqli_real_escape_string ( $myConnect::$cn, $tableName ),
						mysqli_real_escape_string ( $myConnect::$cn, $leerjaar ),
						mysqli_real_escape_string ( $myConnect::$cn, $nrstudent ) );
//echo $sql;
						$result=mysqli_query( $myConnect::$cn, $sql );

						if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

                                        		$log.="TRUE";

                                		}else{

                                        		$log.="FALSE";
				

						}//end if


        				}//end for
//					mysqli_free_result( $result );
                               		mysqli_close( $myConnect::$cn );

					if ( strPos( "FALSE", $log ) ){
						
						return FALSE;


					}else{

						return TRUE;
							

					}//end if
				break;


				}//end switch

			}//end if

		}//end f
		public function fInsertCourses_SC( $tableName, $leerjaar, $nrstudent){
			
			$NR_COURSES = 4;
			$log="";
			require_once 'clsDbConnect.class.php';
			$myConnect = new clsDbConnect();
			$sw_array=array( "Aanmelden in Scratch","Werken met Scratch","Opdracht1", "Opdracht2");

			if ( $myConnect-> fConnect() ){

				switch( $leerjaar ){
				case 7:
					for ( $i=0;$i < $NR_COURSES;$i++ ){

                				if ( $i < $NR_COURSES ){

							$prefix=0;
						}else{

							$prefix="";

						}//end if

						$sql = sprintf( "insert into %s (cursus_nr,leerjaar,nrstudent,cursus_code,cursus_naam,lezen,toets,certificaat) VALUES
						(1".$prefix.$i.",%s,%s,'sc".$prefix.$i."', '".$sw_array[$i]."', 
						'http://ictmethode.nl/leerstof/sc".$prefix.$i."/index.html',
						'http://ictmethode.nl/toets/sc".$prefix.$i."/1.php',0)",
						mysqli_real_escape_string ( $myConnect::$cn, $tableName ),
						mysqli_real_escape_string ( $myConnect::$cn, $leerjaar ),
						mysqli_real_escape_string ( $myConnect::$cn, $nrstudent ) );
//echo $sql;
						$result=mysqli_query( $myConnect::$cn, $sql );

						if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

                                        		$log.="TRUE";

                                		}else{

                                        		$log.="FALSE";
				

						}//end if


        				}//end for
//					mysqli_free_result( $result );
                               		mysqli_close( $myConnect::$cn );

					if ( strPos( "FALSE", $log ) ){
						
						return FALSE;


					}else{

						return TRUE;
							

					}//end if
				break;


				}//end switch

			}//end if

		}//end f
		public function fInsertCourses_GS2( $tableName, $leerjaar, $nrstudent){
			
			$NR_COURSES = 4;
			$log="";
			require_once 'clsDbConnect.class.php';
			$myConnect = new clsDbConnect();
			$sw_array=array( "Mijn GameStudio","Opdracht 1","Opdracht 2", "Opdracht 3");

			if ( $myConnect-> fConnect() ){

				switch( $leerjaar ){
				case 6:
					for ( $i=0;$i < $NR_COURSES;$i++ ){

                				if ( $i < $NR_COURSES ){

							$prefix=0;
						}else{

							$prefix="";

						}//end if

						$sql = sprintf( "insert into %s (cursus_nr,leerjaar,nrstudent,cursus_code,cursus_naam,lezen,toets,certificaat) VALUES
						(1".$prefix.$i.",%s,%s,'gs2".$prefix.$i."', '".$sw_array[$i]."', 
						'http://ictmethode.nl/leerstof/gs2".$prefix.$i."/index.html',
						'http://ictmethode.nl/toets/gs2".$prefix.$i."/1.php',0)",
						mysqli_real_escape_string ( $myConnect::$cn, $tableName ),
						mysqli_real_escape_string ( $myConnect::$cn, $leerjaar ),
						mysqli_real_escape_string ( $myConnect::$cn, $nrstudent ) );
//echo $sql;
						$result=mysqli_query( $myConnect::$cn, $sql );

						if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

                                        		$log.="TRUE";

                                		}else{

                                        		$log.="FALSE";
				

						}//end if


        				}//end for
//					mysqli_free_result( $result );
                               		mysqli_close( $myConnect::$cn );

					if ( strPos( "FALSE", $log ) ){
						
						return FALSE;


					}else{

						return TRUE;
							

					}//end if
				break;


				}//end switch

			}//end if

		}//end f
		public function fInsertCourses_GS1( $tableName, $leerjaar, $nrstudent){
			
			$NR_COURSES = 2;
			$log="";
			require_once 'clsDbConnect.class.php';
			$myConnect = new clsDbConnect();
			$sw_array=array( "Game Studio: hoe werkt een spel?","Game studio: spel veranderen");

			if ( $myConnect-> fConnect() ){

				switch( $leerjaar ){
				case 5:
					for ( $i=0;$i < $NR_COURSES;$i++ ){

                				if ( $i < $NR_COURSES ){

							$prefix=0;
						}else{

							$prefix="";

						}//end if

						$sql = sprintf( "insert into %s (cursus_nr,leerjaar,nrstudent,cursus_code,cursus_naam,lezen,toets,certificaat) VALUES
						(1".$prefix.$i.",%s,%s,'gs1".$prefix.$i."', '".$sw_array[$i]."', 
						'http://ictmethode.nl/leerstof/gs1".$prefix.$i."/index.html',
						'http://ictmethode.nl/toets/gs1".$prefix.$i."/1.php',0)",
						mysqli_real_escape_string ( $myConnect::$cn, $tableName ),
						mysqli_real_escape_string ( $myConnect::$cn, $leerjaar ),
						mysqli_real_escape_string ( $myConnect::$cn, $nrstudent ) );
//echo $sql;
						$result=mysqli_query( $myConnect::$cn, $sql );

						if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

                                        		$log.="TRUE";

                                		}else{

                                        		$log.="FALSE";
				

						}//end if


        				}//end for
//					mysqli_free_result( $result );
                               		mysqli_close( $myConnect::$cn );

					if ( strPos( "FALSE", $log ) ){
						
						return FALSE;


					}else{

						return TRUE;
							

					}//end if
				break;


				}//end switch

			}//end if

		}//end f


}//end class


?>
