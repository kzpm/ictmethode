<?php


class clsQuestionProgress{

	public function fCreateQuestionProgress( $nr_student, $cursus_code){

		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();
		if ( $myConnect->fConnect() ){
		
			$sql =sprintf("create table if not exists q%s%s (idq int(11) not null auto_increment, leerjaar char(1), nrstudent int(11),cursus_code char(50),question int(5), try int(5) default 0, ok int(1), primary key (idq) );", mysqli_real_escape_string( $myConnect::$cn, $nr_student ), mysqli_real_escape_string( $myConnect::$cn, $cursus_code ) );
//echo $sql;
			if ( mysqli_query( $myConnect::$cn, $sql ) ){


				return TRUE;
	
  		        }else{

				return FALSE;

		   	}//end if



		}//end if

	}//end f

}//end class
