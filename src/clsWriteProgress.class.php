<?php

class clsWriteProgress{

	public function fSetProgress( $tableName, $leerjaar, $st_code, $cursuscode, $question, $ok  ){
	
		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();

		if ( $myConnect->fConnect() ){
			
			if ( !$this->fCheckIfExists( $tableName, $question )  ){


				$sql = sprintf( "insert into %s ( leerjaar, nrstudent, cursus_code, question, try, ok) values (%s,%s,'%s',%s,try+1,%s)", mysqli_real_escape_string( $myConnect::$cn, $tableName ), mysqli_real_escape_string( $myConnect::$cn, $leerjaar ),mysqli_real_escape_string( $myConnect::$cn, $st_code ), mysqli_real_escape_string( $myConnect::$cn, $cursuscode ), mysqli_real_escape_string( $myConnect::$cn, $question ), mysqli_real_escape_string( $myConnect::$cn, $ok ) );
//echo $sql;
				$result = mysqli_query( $myConnect::$cn, $sql );
				if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

					return TRUE;

				}//end if



			}//end if
			if ( $this->fCheckIfExists( $tableName, $question ) ){

				if ( $ok == 0 ){

					$try=1;

				}else{

					$try=0;

				}//end if

				$sql = sprintf("update %s set try=try+%s, ok=%s where question= %s", mysqli_real_escape_string( $myConnect::$cn, $tableName ),mysqli_real_escape_string( $myConnect::$cn, $try ), mysqli_real_escape_string( $myConnect::$cn, $ok ), mysqli_real_escape_string( $myConnect::$cn, $question ));
//echo $sql;
				$result = mysqli_query( $myConnect::$cn, $sql );
				if ( mysqli_affected_rows( $myConnect::$cn ) > 0 ){

					return TRUE;

				}else{

					return FALSE;

				}//end if
				mysqli_free_result( $result );


			}//end if

		}//end if
		mysqli_close( $myConnect::$cn );

	}//end f

	public function fCheckIfExists( $tableName, $question ){

		require_once 'clsDbConnect.class.php';
                $myConnect = new clsDbConnect();

                if ( $myConnect->fConnect() ){
			
			$sql=sprintf("select * from %s where question = %s", mysqli_real_escape_string( $myConnect::$cn, $tableName ), mysqli_real_escape_string( $myConnect::$cn, $question ));
//echo $sql;
			$result = mysqli_query( $myConnect::$cn, $sql );
			$row_count = mysqli_num_rows( $result );
			if ( $row_count > 0 ){


				return TRUE;
			}else{


				return FALSE;

			}//end if
			mysqli_free_result( $result );

		}//end if
		mysqli_close( $myConnect::$cn );



	}//end f

}//end class
