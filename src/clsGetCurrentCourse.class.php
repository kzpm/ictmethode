<?php

class clsGetCurrentCourse{

	public function fGetCurrentCourse( $tableName, $leerjaar, $nr_student ){

		$still_open=array();
		$current_courses=array();
		require_once 'clsDbConnect.class.php';
		$myConnect = new clsDbConnect();
		if ( $myConnect->fConnect() ){

			$sql = sprintf("SELECT * FROM %s where nrstudent=%s LIMIT 12",mysqli_real_escape_string( $myConnect::$cn, $tableName ),mysqli_real_escape_string( $myConnect::$cn,$nr_student ));
//echo $sql;
	  	        $result = mysqli_query( $myConnect::$cn, $sql );

			if ( $result ){

				// iterate through the results
				while ( $current_courses = mysqli_fetch_assoc( $result ) ){

					if ( $current_courses['certificaat']  == 1 ){
					
						$cert='<a href= "certs/'.$current_courses['cursus_code'].'-'.$nr_student.'_cert.pdf">Ja</a>';

					}else{

						$cert = 'Nee';

					}//end if

  					$still_open[] = '<tr><td>'.$current_courses['cursus_code']. '</td><td>'. $current_courses['cursus_naam'].'</td><td><a href="'.
					$current_courses['lezen'].'">Lezen</a></td><td><a href="'.$current_courses['toets'].'">Toets</a></td><td>'.$cert.'</tr>' ;

				}//

				mysqli_free_result( $result );
				mysqli_close( $myConnect::$cn );
                        	return $still_open;

                	}//end if


		}//end if

	}//end f


}//end class

