<?php


class nwCert{

public function fCert2PDF( $leerjaar, $nr_student, $cursus_code ){

		$_SERVER['DOCUMENT_ROOT'] = dirname( dirname(__FILE__) );
                $rdir = $_SERVER['DOCUMENT_ROOT'];
		$certsdir = $rdir."/certs/";

		require_once 'clsCreateCert.class.php';
		$myCert = new clsCreateCert();
		$myCert->fGetStudentName( $nr_student );
                $studentName=$myCert->fGetStudentName( $nr_student );
                echo $studentName;
                $cursus_naam=$myCert->fGetCourseName( $leerjaar, $nr_student, $cursus_code );

                require_once $rdir.'/inc/fpdf/fpdf.php';
                $pdf = new FPDF();
                $pdf->AddPage();
                $pdf->SetFont( 'Arial','B',20 );

                $pdf->Cell( 0,10,'Certificaat', 1,1,"C" );
                $pdf->SetFont( 'Arial','B',16 );
                $pdf->Cell( 0,10,'Basisvaardigheden ICT Basisonderwijs', 0,1,"C" );
                $pdf->Ln( 20 );

                $pdf->SetFont('Arial','B',30);
                $pdf->Cell( 0,10,$studentName, 0,1,"C" );
                $pdf->Ln( 20 );

                $pdf->SetFont( 'Arial','B',16 );
                $pdf->Cell( 80,10,"Module" );
                $pdf->Cell( 0,10, $cursus_naam,0,1,"L" );
                $pdf->Cell( 80,10,"Module code" );
                $pdf->Cell( 0,10, $cursus_code,0,1,"L" );
                $pdf->Cell( 80,10,"Leerjaar" );
                $pdf->Cell( 0,10,$leerjaar,0,1,"L" );
                $pdf->Cell( 80,10,"Datum" );
                $pdf->Cell( 0,10,date("d-m-Y"),0,1,"L" );
                $fileName=$certsdir.$cursus_code.'-'.$nr_student.'_cert.pdf';
                echo $fileName;
                $pdf->Output( $fileName, 'F' );

                if ( file_exists(  $fileName ) ){

                        return TRUE;

                }else{

                        return FALSE;

                }//end if
        }//end f

}//end class
// echo $_SERVER["DOCUMENT_ROOT"];
// $myCert=new nwCert();
// $myCert->fCert2PDF( 5, 412, "gs100" ) 

?>
