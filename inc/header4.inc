<!---
Created using Bootstrap responsive: https://www.w3schools.com/bootstrap
-->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:locale" content="nl_NL" />
<meta property="og:type" content="article" />
<meta property="og:title" content="IT vaardigheden P.O." />
<meta property="og:description" content="Cursusplatform voor vaardigheden IT primair onderwijs" />
<meta property="og:url" content="https://ictmethode.nl" />
<meta property="og:site_name" content="Vaardigheidstraining IT primair onderwijs" />
<meta name="twitter:description" content="Gratis een volledige vaardigheidstraining IT voor het primair onderwijs, met certificering!"/>
<meta name="twitter:title" content="It vaardigheden PO"/>
<meta name="twitter:site" content="@ITvaardig"/>
<meta name="twitter:domain" content="ictmethode.nl"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Work+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Rubik:400,700' rel='stylesheet' type='text/css'>
   <!-- <link rel="icon" href="https://ictmethode.nl/open-source.png" type="image/png" sizes="12x13"> -->
<script>
	setTimeout(function() {
		 $(".wrong").fadeOut("fast");
	}, 1000);
	setTimeout(function() {
		 $(".correct").fadeOut("fast");
	}, 1000);
  </script>

<title>ICT vaardigheden PO</title>
<style>

	body{
		//background-image:url("https://www.vector-eps.com/wp-content/gallery/digital-backgrounds-stock-images/thumbs/thumbs_digital-backgrounds-stock-images4.jpg");
		background: #333 url();
		//z-index: -1; position:relative;
	}

	.wrapper{
		min-height:100%;
		height:auto !important;
		height:100%;
		margin:0 auto -165px;
	}
	.footer, .push{
		height: 165px;
		text-align:center;
		font-size:1.75em;
		font-family: 'Work Sans', sans-serif;
		color:#fff;
		padding-top:100px;
		background-color:#fff;
	}

	h1{
		margin-left:auto;		
		margin-right:auto;		
		font-family: 'Rubik', sans-serif;
		background-position:right;
		background-repeat: no-repeat;
		/*opacity: 0.8;*/
		color:lightblue;
		
	}

	h2, h4, h3{
		font-family: 'Rubik', sans-serif;
		color:white;

	}
	h6{
		font-family: 'Work Sans', sans-serif;
		color:black;

	}

	p{
		font-family: 'Work Sans', sans-serif;
		color:white;


	}

	table * {
		color:white;
		padding:10px;
    		background-color:transparent;
		opacity: 0.9;
    		filter: alpha(opacity=50); /* For IE8 and earlier */
	}
	input{
		color:white;
	}
	.wrong{

		font-size:26px;color:white;text-align:center;border-color: width:20%; background-color:#d00;
	}
	.correct{

		font-size:26px;color:white;text-align:center;border-color: width:20%; background-color:green;
	}


	menu, 
	.menu li,
	.menu a {
    		margin: 0;
    		padding: 0;
    		border: none;
    		outline: none;
	}

	.menu {

		margin-top:0px;
    		height: 40px;
    		width: 100%;

    		background:  #4c4e5a;
    		background: -webkit-linear-gradient(top, #4c4e5a 0%,#2c2d33 100%);
    		background: -moz-linear-gradient(top, #4c4e5a 0%,#2c2d33 100%);
    		background: -o-linear-gradient(top, #4c4e5a 0%,#2c2d33 100%);
    		background: -ms-linear-gradient(top, #4c4e5a 0%,#2c2d33 100%);
    		background: linear-gradient(top, #4c4e5a 0%,#2c2d33 100%);

    		-webkit-border-radius: 5px;
    		-moz-border-radius: 5px;
    		border-radius: 5px;
	}

	.menu li {
    		position: relative;
    		list-style: none;
    		float: left;
    		display: block;
    		height: 40px;
	}

	.menu li a {
    		display: block;
    		padding: 0 14px; #14px
    		margin: 4px 0;
    		line-height: 28px;
    		text-decoration: none;

    		border-left: 1px solid #393942;
    		border-right: 1px solid #4f5058;

    		font-family: Helvetica, Arial, sans-serif;
    		font-weight: bold;
    		font-weight: normal;
    		font-size: 13px;

    		color: #f3f3f3;
    		text-shadow: 1px 1px 1px rgba(0,0,0,.6);

    		-webkit-transition: color .2s ease-in-out;
    		-moz-transition: color .2s ease-in-out;
    		-o-transition: color .2s ease-in-out;
    		-ms-transition: color .2s ease-in-out;
    		transition: color .2s ease-in-out;
	}

	.menu li:first-child a { border-left: none; }
	.menu li:last-child a{ border-right: none; }

	.menu li:hover > a { color: #8fde62; }

	.menu ul {
    		position: absolute;
    		top: 40px;
    		left: 0;

    		opacity: 0;
    		background: #1f2024;

    		-webkit-border-radius: 0 0 5px 5px;
    		-moz-border-radius: 0 0 5px 5px;
    		border-radius: 0 0 5px 5px;

    		-webkit-transition: opacity .25s ease .1s;
    		-moz-transition: opacity .25s ease .1s;
    		-o-transition: opacity .25s ease .1s;
    		-ms-transition: opacity .25s ease .1s;
    		transition: opacity .25s ease .1s;
	}

	.menu li:hover > ul { opacity: 1; }

	.menu ul li {
    		height: 0;
    		overflow: hidden;
    		padding: 0;

    		-webkit-transition: height .25s ease .1s;
    		-moz-transition: height .25s ease .1s;
    		-o-transition: height .25s ease .1s;
    		-ms-transition: height .25s ease .1s;
    		transition: height .25s ease .1s;
	}

	.menu li:hover > ul li {
    		height: 36px;
    		overflow: visible;
    		padding: 0;
	}

	.menu ul li a {
    		width: 380px;
    		padding: 1px 0 4px 40px;
    		margin: 0;

    		border: none;
    		border-bottom: 1px solid #353539;	
	}

	.menu ul li:last-child a { border: none; }

	#.menu a.documents { background: url("qqlq_ps_ico.gif") no-repeat 0px center; }
	#.menu a.messages { background: url("qqlq_ps_ico.gif") no-repeat 0px center; }
	#.menu a.signout { background: url("qqlq_ps_ico.gif") no-repeat 0px center; }

</style>

<!--- <script src="//f1-eu.readspeaker.com/script/5/ReadSpeaker.js?pids=embhl" type="text/javascript"></script>-->

  </head>

<body>
<div class="container">

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<h3 class =""><img class="img-rounded" src="https://blog.careerhub.mu/wp-content/uploads/2017/05/PRESS-4TH-INDUSTRIAL-REVOLUTION-MAY-2017-7.png" width="100">
	    Welkom bij de cursus ICT vaardigheden!
	</h3>      


 	<div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                       <span class="sr-only">Toggle navigation</span>
                       <span class="icon-bar"></span>
                       <span class="icon-bar"></span>
                       <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="#" >ICTmethode, gratis online ICT onderwijs!</a>

          </div>
 	  <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
			<li class="active"><a href="index.php">Home</a></li>
                        <li><a href="contact.php">Contactformulier</a></li>

                        <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Groepen<b class="caret"></b></a>
                                <ul class="dropdown-menu">
  					<li><a href="/gr12/begrippenlijst.html" target="_blank">Groep 1 en 2</a></li>
  					<li><a href="/cb3.pdf" target="_blank">Groep 3</a></li>
  					<li><a href="/cb4.pdf" target="_blank">Groep 4</a></li>

                                </ul>
                        </li>
		
  			<li class="dropdown"><a href="#"  class="dropdown-toggle" data-toggle="dropdown">Lesidee<b class="caret"></b></a>
                                <ul class="dropdown-menu">
  					<li><a href ="https://www.codekinderen.nl/leerling/unplugged/sandwich-robot/index.html" target="_blank">Boterham met hagelslag</a></li>
  					<li><a href="https://www.codekinderen.nl/leerling/unplugged/robot-taal/index.html" target="_blank">Programmeren zonder computer</a></li>
  					<li><a href="https://www.codekinderen.nl/leerling/unplugged/binair-tellen/index.html">Computer rekenen</a></li>
				</ul>
  			</li>

  			<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Wat is er al gemaakt?<b class="caret"></b></a>
                                <ul class="dropdown-menu">
					<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=571226" target="_blank">Kom bij de megamunt, door B.W.</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=571196" target="_blank">Van steen naar hout, door B.W.</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=571173" target="_blank">Kistjes verschuiven, door Je naam komt hier</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=571189" target="_blank">Muurtje tien, door B.W.</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=572814" target="_blank">Safari men, door P.L.</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=572469" target="_blank">ersion=, door L.A.</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=573522" target="_blank">Muurtje tien door L.A.</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=574519" target="_blank">Achter de muur door Test</a> </li>
        				<li><a href="https://gamestudio.hetklokhuis.nl/player/home?ID=574984" target="_blank">Onder water game door Denise</a> </li>
				</ul>
  			</li>

                </ul>
          </div>
      </div>
</div>

<!--
<div id="readspeaker-button1" class="rs_skip rsbtn rs_preserve">
    <a rel="nofollow" class="rsbtn_play" accesskey="L" title="Laat de tekst voorlezen met ReadSpeaker" href="//app-eu.readspeaker.com/cgi-bin/rsent?customerid=5&lang=nl_nl&amp;voice=Ilse&readid=div&url=http%3A%2F%2Fictmethode.nl%2F">
        <span class="rsbtn_left rsimg rspart"><span 
class="rsbtn_text"><span>Lees voor</span></span></span>
        <span class="rsbtn_right rsimg rsplay rspart"></span>
    </a>
</div>
-->


<br>
<br>
<br>
<br>
<br>
<br>

